import React, {useEffect, setRef, useState} from 'react';

import AppNavigator from './src/navigation/AppNavigator';
import { useDispatch } from "react-redux";
import {updateStatusAuth, updateUser} from './src/reducers/User';
import {updateSettings} from './src/reducers/General';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-toast-message';
import {StatusBar, PermissionsAndroid, Platform, View, Button, 
      TouchableOpacity, Text, Image, StyleSheet, SafeAreaView, Dimensions} from 'react-native';
import {UserAPI} from './src/api/user';
import I18n from './src/i18n/i18n';
import Update from './src/scenes/Update';
import Preloader from './src/components/Preloader';
import Modal from 'react-native-modal';


let deviceWidth = Dimensions.get('window').width;

/*const ModaldReview = (props) => {

  return(
      <Modal isVisible={props.isVisible}>
      <SafeAreaView style={{ flex: 1 }}>
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
              <View style={[styles.modalContainer]}>
                        <View style={styles.modalHeader}>
                              <Text style={styles.modalTitle}></Text>
                              <View>
                                  <TouchableOpacity onPress={() => props.close()} style={styles.close}>
                                      <Image style={styles.closeIcon} source={require('./src/source/img/close.png')}/>
                                  </TouchableOpacity>
                              </View>
                        </View>
                         <Image source={require('./src/source/img/rating.png')} style={styles.ratingImg}/>
                         <TouchableOpacity onPress={() => props.close()}  style={styles.bt}>
                           <Text style={styles.review}>Оставить отзыв</Text>
                         </TouchableOpacity>
                         <TouchableOpacity onPress={() => props.close()}  style={styles.bt}>
                           <Text style={styles.cancle}>Оставить позже</Text>
                         </TouchableOpacity>
                         <TouchableOpacity onPress={() => props.close()}  style={styles.bt}>
                           <Text style={styles.cancle}>Больше не показывать</Text>
                         </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
      </Modal>
  )
}*/

const App = () => {
  const dispatch = useDispatch();
  
  let [version, setVersion] = useState(true);
  let [load, setLoad] = useState(true);
  let [skip, setSkip] = useState(false);
  let [lang, setLang] = useState(false);
  let [modalRating, setModalRating] = useState(false);

  useEffect( () => {
    checkVersion();
    getStartData();
    if (Platform.OS === 'android') {
      requestLocationPermission();
    }
    setLoad(true);
  }, []);


  const getStartData = async () => {
    let user = await AsyncStorage.getItem('@user');
    let settings = await AsyncStorage.getItem('@settings');
    let subdomain = await AsyncStorage.getItem('@subdomain');

    console.log('getStartData')


    settings = settings ? JSON.parse(settings) : {};

    settings = {
      ...settings,
      subdomain
    };
    
    if(settings){
      dispatch(updateSettings(settings));
    }
      
    user = JSON.parse(user);


    if(user && user.accessToken){
      console.log('accessToken')

      UserAPI.getUser()
        .then(res => {
          console.log(res, 'res');
          dispatch(updateUser(res));
          setLoad(false);
        })
        .catch(err => {
          console.log(err, 'err');
          setLoad(false);
        })
      dispatch(updateStatusAuth(true));
    } else {
      console.log('no')
      setLoad(false);
      dispatch(updateStatusAuth('auth'));
    }

  
  }

  const checkVersion = async () => {
    let version = await AsyncStorage.getItem('@version');

    let language = await AsyncStorage.getItem('@language');
    language = language ? JSON.parse(language) : {name: 'English', id: 'en'};
    setLang(language);

    let new_version = '1.0';

    if(!version && new_version !== version){
      version = new_version;
      await AsyncStorage.setItem('version', new_version);
    }

    let platform = Platform.OS;
   UserAPI.getVersion()
    .then(res => {
      console.log(res, 'getVersion');

        if(res[platform].version === version){
          setVersion(true);
        } else {
          setVersion(res[platform]);
        }
        setLoad(false);

    })
    .catch(err => {
        console.log(err, 'err');
        setLoad(false);
    });
  }

  const requestLocationPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          'title': I18n.t('attention'),
          'message': I18n.t('errorGeo')
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      
      } else {
      
      }
    } catch (err) {
     
    }
  }

  

  return(
    <View style={{flex: 1}}>
    <StatusBar barStyle="dark-content" />
    {load || version === null ? <Preloader/> : version === true || skip ? <AppNavigator/> :
       <Update version={version}
              skip={() => setSkip(true)}
              lang={lang}
       />}
   {/** <AppNavigator/> */}
     {/** <ModaldReview isVisible={modalRating} close={() => setModalRating(false)}/> */}
    <Toast ref={(ref) => Toast.setRef(ref)} />
    </View>
  )
}

const styles = StyleSheet.create({
  modalContainer: {
    width: deviceWidth-42,
    padding: 16,
    borderRadius: 8,
    backgroundColor: '#fff',
    maxWidth: 450,
    alignItems: 'center'
  },  
  close: {
    position: 'absolute',
    top: 0,
    right: 0,
    padding: 2,
},
closeIcon: {
    width: 20, 
    height: 20
},
modalTitle: {
    color: '#7E8086',
    fontSize: 22,
    width: '100%',
    textAlign: 'center'
},
modalHeader: {
    flexDirection: 'row',
},
ratingImg:{
  width: 240,
  height: 150,
  resizeMode: 'contain',
  //marginBottom: 15
},
review: {
  fontSize: 16,
  textTransform: 'uppercase',
  fontWeight: '600',
},
cancle: {
  fontSize: 16,
  textTransform: 'uppercase',
  fontWeight: '600',
  color: '#7E8086'
},
bt: {
  paddingVertical: 8
}
});


export default App;
