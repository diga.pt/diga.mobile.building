import I18n from 'react-native-i18n';
import en from './locales/en';
import ru from './locales/ru';
import pt from './locales/pt';
import es from './locales/es';

import AsyncStorage from '@react-native-community/async-storage';


I18n.fallbacks = true;

I18n.locale =  'en';

let getLanguage = async () => {
  let language = await AsyncStorage.getItem('@language');
  language = language ? JSON.parse(language) : {name: 'English', id: 'en'};
  I18n.locale =  language.id;
}

getLanguage();


I18n.translations = {
  en,
  ru,
  pt,
  es
};

export default I18n;