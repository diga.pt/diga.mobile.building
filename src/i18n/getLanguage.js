
import AsyncStorage from '@react-native-community/async-storage';

export const getLanguage = async () => {
    let language = await AsyncStorage.getItem('@language');
    language = language ? JSON.parse(language) : {name: 'English', id: 'en'};
    return language;
  }