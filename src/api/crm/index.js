import API from '../utils/APIConnector';
import config from '../utils/config';

const Request = new API();
const TOKEN = config.token;

export class CrmAPI {

  static getContact = async (props) => await Request.GET(`api/contacts/${props.id}`, {  }, true, 'subdomain');
  
  static getEventTypes = async (props) => await Request.GET(`api/event_types`, {  }, true, 'subdomain');

  static getEvents = async (props) => await Request.GET(`api/events`, {...props}, true, 'subdomain');

  static getEvent = async (props) => await Request.GET(`api/events/${props.id}`, {}, true, 'subdomain');

  static addEvents = async (props) => await Request.POST(`api/events`, {...props}, true, 'subdomain');

  static updateEvent = async (props) => await Request.PATCH(`api/events/${props.id}`, {...props.data}, true, 'subdomain');

  static getUsers = async (props) => await Request.GET(`api/users`, {...props}, true, 'subdomain');

  static getUser = async (props) => await Request.GET(`api/users/${props.id}`, {}, true, 'subdomain');

  static getFunnel = async (props) => await Request.GET(`api/service_states/services`, {}, true, 'subdomain');

  static addСontacts = async (props) => await Request.POST(`api/contacts`, {...props}, true, 'subdomain');

  static updateСontact = async (props) => await Request.PATCH(`api/contacts/${props.id}`, {...props.data}, true, 'subdomain');

  static getServiceTypes = async (props) => await Request.GET(`api/event_types`, {  }, true, 'subdomain');

  static getAllUsers = async (props) => await Request.GET(`api/users/all`, {  }, true, 'subdomain');

  static getServiceStates = async (props) => await Request.GET(`api/service_states`, {  }, true, 'subdomain');

  static getServicePriorities = async (props) => await Request.GET(`api/service_priorities`, {  }, true, 'subdomain');

  static addServices = async (props) => await Request.POST(`api/services`, {...props}, true, 'subdomain');

  static addHistory = async (props) => await Request.POST(`api/clients/${props.id}/history`, {message: props.message}, true, 'subdomain');
  
  static finish = async (props) => await Request.POST(`api/calendar/${props.id}/finish`, {}, true, 'subdomain');

  static estimates = async (props) => await Request.GET('api/estimates', { ...props }, true, 'subdomain');

  static services = async (props) => await Request.GET('api/services', { ...props }, true, 'subdomain');

  static addUser = async (props) => await Request.POST('api/users', { ...props }, true, 'subdomain');

  static getBranch = async () => await Request.GET('api/branches', {}, true, 'subdomain');

  static getHistory = async (props) => await Request.GET(`api/contacts/history/${props.id}`, {...props.data}, true, 'subdomain');

  static getMessage = async (props) => await Request.GET(`api/history/${props.id}/message`, {}, true, 'subdomain');

  static getSites = async (props) => await Request.GET(`api/sites?fields=id,domain,token`, {}, true, 'subdomain');

  static getLinkToPDF = async (props) => await Request.GET(`api/estimates/get_link/${props.id}`, {}, true, 'subdomain');

  static getInvoice = async (props) => await Request.GET(`api/expences`, {...props}, true, 'subdomain');

};
