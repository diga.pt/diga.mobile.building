import API from '../utils/APIConnector';
import config from '../utils/config';

const Request = new API();
const TOKEN = config.token;

export class SettingsAPI {

  static contacts = async (props) => await Request.GET('api/contacts', { ...props }, true, 'subdomain');

  static estimates = async (props) => await Request.GET('api/estimates', { ...props }, true, 'subdomain');

  static services = async (props) => await Request.GET('api/services', { ...props }, true, 'subdomain');

  static addUser = async (props) => await Request.POST('api/users', { ...props }, true, 'subdomain');

  static getBranch = async () => await Request.GET('api/branches', {}, true, 'subdomain');

  static getLogs = async () => await Request.GET('api/timetracker/logs', {}, true, 'subdomain');


};
