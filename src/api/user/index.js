import API from '../utils/APIConnector';
import config from '../utils/config';

const Request = new API();
const TOKEN = config.token;

export class UserAPI {

  static auth = async (props) => await Request.POST('login', { ...props }, false, 'subdomain');

  static authWithPin = async (props) => await Request.POST('api/login_with_pin', { ...props }, false, 'subdomain');
  
  static signUp = async (props) => await Request.POST('external_trial', { ...props }, false, 'new');

  static getSubdomain = async (props) => await Request.GET(`api/auth0/${props.data}/roles`, {}, false, 'new');

  static checkSubdomain = async (props) => await Request.GET('client/v4/zones/21a77b2f4123019138385c92cca28101/dns_records', { ...props }, true, 'checkSubdomain');

  static getUser = async () => await Request.GET('api/me', {}, true, 'subdomain');

  static forgotPassword = async (props) => await Request.POST('password/email', { ...props }, false, 'forgot');

  static getAdditionalUser = async (props) => await Request.GET('api/me', {...props}, true, 'my');

  static checkPin = async (props) => await Request.GET(`api/user/pin/${props.pin}`, {}, true, 'subdomain');

  static getVersion = async (props) => await Request.GET('api/settings/app_settings', { }, false, 'new');


};
