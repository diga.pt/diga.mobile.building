import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import config from '../utils/config';

const SERVER_URL = config.serverURL;
const TOKEN = config.token;

const APIMethods = {
  PUT: 'PUT',
  GET: 'GET',
  POST: 'POST',
  DELETE: 'DELETE',
  PATCH: 'PATCH'
};

const request = async (
  route,
  method,
  params,
  token = true,
  type = null,
) => {
  console.log({
    route,
  method,
  params,
  token, 
  type
  })
  

  let user = await AsyncStorage.getItem('@user');

  let subdomain = await AsyncStorage.getItem('@subdomain');

  let SERVER = SERVER_URL;

  let access_token  = token ? type === 'checkSubdomain' ? 
          'x0N1AHTVxwEyh5tLgx2lRXaIeMRBQ7s0YixX-mwF' : JSON.parse(user).accessToken : '';

  if(type){
    let domain = 'diga.pt';
      if(subdomain === 'besterp' || params.subdomain === 'besterp'){
        domain = 'rkesa.pt';
      }
    SERVER = type === 'subdomain' || type === 'my' ? 
    `https://${subdomain}.${domain}/` : 
    type === 'checkSubdomain' ? 
    'https://api.cloudflare.com/' : 
    type === 'new' ? 'https://pec.pt/' : type === 'forgot' 
    ? `https://${params.subdomain}.${domain}/` : '';
  }

  if(type === 'forgot'){
    params = {email: params.email}
  } else if (type === 'my') {
    access_token = params.token;
    params = {};
  }

  const query = new URLSearchParams(params).toString();
  let url = APIMethods[method] !== 'GET' ? `${SERVER}${route}` : `${SERVER}${route}?${query}`;
  const headers = token ? {'Content-Type': type === 'file' ? 'multipart/form-data' : 'application/json',
  'Authorization': 'Bearer '+access_token} : {'Content-Type': 'application/json'};

  let body = {
    url,
    headers,
    method: APIMethods[method],
  }

  if(APIMethods[method] !== 'GET'){
    // body.url = `${SERVER_URL}${route}`;
    console.log('!!!params >>> ', query);
    body['data'] = params;
  }

  console.log('====================================');
  console.log('url', url);
  console.log('body', body);


  return axios(body)
  .then((response) => {
    // console.log('====================================');
    return response.data;
  })
  .catch((error) => {
    console.log('====================================');
    console.log('axios.error', url, error);
    return { status: 'error' };
  });
}

class API {
  url = '';

  constructor(url = '') {
    this.url = url;
  }

  GET = (internal = '', params, token, type) => request(this.url + internal, APIMethods.GET, params, token, type);

  POST = (internal = '', params, token, type) => request(this.url + internal, APIMethods.POST, params, token, type);

  PATCH = (internal = '', params, token, type) => request(this.url + internal, APIMethods.PATCH, params, token, type);

  PUT = (internal = '', params, token) => request(this.url + internal, APIMethods.PUT, params, token);

  DELETE = (internal = '', params, token) => request(this.url + internal, APIMethods.DELETE, params, token);
}

export default API;
