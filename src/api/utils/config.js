const DOMAIN = 'https://scooter.if.ua/api/';

const CONFIGS = {
  DEV: {
    serverURL: DOMAIN,
  },
};

export default CONFIGS.DEV;
