import React, {useEffect, useState} from 'react';
import {View, ActivityIndicator, Text, StyleSheet} from 'react-native';

const Preloader = (props) => {
    return(
        <View style={styles.container}>
                <ActivityIndicator size="large" color="#fff"/>
        </View>
    )
}

const styles = StyleSheet.create({
    content: {
        backgroundColor: 'rgba(30, 30, 30, 0.75)',
        padding: 22,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection:'column',
        borderRadius: 14,
        width:173, 
        height:173,
        borderColor: 'rgba(30, 30, 30, 0.75)',
      },
      contentTitle: {
        fontSize: 12,
        fontWeight: '500', 
        color: 'white',
        textAlign:'center',
        top: 10,
      },
      container: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 9999,
        backgroundColor: 'rgba(30, 30, 30, 0.65)',
      }
})

export default Preloader;