import React, { useState, useEffect } from 'react';

import {Text, StyleSheet, TouchableOpacity, TextInput, 
        View, Dimensions, ScrollView, SafeAreaView, Image} from 'react-native';

import Modal from 'react-native-modal';

let deviceWidth = Dimensions.get('window').width;


const ModalSearch = (props) => {
    return(
        <Modal isVisible={props.isVisible}>
                    <SafeAreaView style={{ flex: 1 }}>
                      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <View style={[styles.modalContainer]}>
                            <View style={styles.modalHeader}>
                                <Text style={styles.modalTitle}>{props.title}</Text>
                                <View>
                                    <TouchableOpacity onPress={() => props.close()} style={styles.close}>
                                        <Image style={styles.closeIcon} source={require('../source/img/close.png')}/>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={[styles.inputWrapper, {paddingTop: 16}]}>
                                <View style={styles.inputSearch}>
                                    <TextInput
                                        placeholder={'Search'}
                                        style={styles.input}
                                        placeholderTextColor={'#7E8086'}
                                        onChangeText={(e) => props.search(e)}
                                    />
                                </View>
                            </View>
                            {props.activeInput === props.type && props.data.length > 0 && <View style={styles.listContainer}>
                        <ScrollView keyboardShouldPersistTaps='always' nestedScrollEnabled = {true} style={styles.list}>
                            {props.data.map((it, index) => {
                                return(
                                    <TouchableOpacity onPress={() => {props.choose(it.id, it.label);}} 
                                        key={index} style={styles.bt}
                                    >
                                        <Text style={styles.btText}>{it.label}</Text>
                                    </TouchableOpacity>
                                )
                            })}
                        </ScrollView>
                    </View>}
                        </View>
                    </View>
                    </SafeAreaView>
        </Modal>
    )
}

export const Autocomplete = (props) => {

    const [value, setValue] = useState('');
    const [isVisible, setIsVisible] = useState(false);

    useEffect(() => {
        setValue(props.defaultValue);
    }, [])

    const search = (e) => {
        setValue(e);
        props.search(e, props.type);
        if(!props.activeInput){
            props.setFocus(props.type);
        }
    };

    const choose = (id, label) => {
        props.input.onChange(id);
        setValue(label);
        props.setFocus(null);
        setIsVisible(false);
    };

    return(
        <>
        <ModalSearch    isVisible={isVisible}
                        title={props.label}
                        close={() => setIsVisible(false)}
                        activeInput={props.activeInput}
                        type={props.type}
                        data={props.data}
                        setFocus={props.setFocus}
                        value={value}
                        search={search}
                        choose={choose}
                    />
    <View style={[styles.inputWrapper, props.index && {zIndex: props.index}]}>
    {props.label && <Text style={styles.label}>{props.label}</Text>}
      <View style={[styles.inputContainer, props.index && {zIndex: props.index}]}>
            <TouchableOpacity style={styles.btInput} 
                                onPress={() =>  {
                                            props.setFocus(props.type);
                                            setIsVisible(true)
                                        }}></TouchableOpacity>
        <TextInput
          placeholder={props.placeholder}
          style={styles.input}
          placeholderTextColor={'#7E8086'}
          onFocus={() => {
              props.setFocus(props.type);
          }}
          onBlur={() => {
            props.setFocus(null);
          }}
          onChangeText={(e) => search(e)}
          value={value}
          editable={false}
        />
      </View>
      {props.meta.touched && (props.meta.error && <Text style={styles.error}>{props.meta.error}</Text>)}
      </View>
      </>
    )
}

const styles = StyleSheet.create({
    input: {
        flex: 1,
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 50,
        paddingHorizontal: 16,
        fontSize: 15
    },
    label: {
        paddingHorizontal: 16,
        //marginTop: 15,
        fontWeight: '500',
        fontSize: 14,
        color: '#4D5860',
        paddingBottom: 8,
        width: '100%'
    },
    inputWrapper: {
      width: '100%',
      position: 'relative',
      zIndex: 1,
      alignItems: 'center',
      marginBottom: 15
    },
    inputContainer: {
        width: '100%',
        height: 50,
        backgroundColor: '#dedede',
        flexDirection: 'row',
        alignItems: 'center',
        fontWeight: '500',
        backgroundColor: '#fff',
        borderRadius: 50,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        position: 'relative',
    },
    list: {
        //width: deviceWidth-34,
       // borderBottomLeftRadius: 10,
        //borderBottomRightRadius: 10,
        //zIndex: 9999
    },
    listContainer: {
        //maxHeight: 310,
        //backgroundColor: '#fff',
        //position: 'absolute',
        //top: 38,
       /* zIndex: 8888,
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        paddingBottom: 10,
        paddingTop: 38,
        zIndex: 8888*/
        width: '100%',
        height: 250
    },
    bt: {
        width: '100%',
        paddingHorizontal: 16,
        paddingVertical: 12,
        borderBottomColor: '#e9ecef',
        borderBottomWidth: 1
    },
    btText: {
        color: '#7E8086',
        fontSize: 16
    },
    listBt: {
        paddingHorizontal: 16,
        paddingVertical: 16
    },
    modalContainer: {
        width: deviceWidth-42,
        padding: 16,
        borderRadius: 8,
        backgroundColor: '#fff',
        maxWidth: 450,
    },
    close: {
        position: 'absolute',
        top: 0,
        right: 0,
        padding: 2,
    },
    closeIcon: {
        width: 20, 
        height: 20
    },
    modalTitle: {
        color: '#7E8086',
        fontSize: 18,
        width: '100%',
        textAlign: 'center'
    },
    modalHeader: {
        flexDirection: 'row',
    },
    btInput: {
        flex: 1,
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        zIndex: 99999,
        width: '100%',
        height: 50,
        borderRadius: 50
    },
    inputSearch: {
        borderBottomColor: '#e9ecef',
        borderBottomWidth: 1,
        height: 44,
        width: '100%'
    },
    error: {
        color: '#F44336',
        fontSize: 12,
        paddingTop: 5,
        paddingHorizontal: 16,
        width: '100%'
    },
});

