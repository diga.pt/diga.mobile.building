import React from 'react';

import {Text, StyleSheet, TouchableOpacity, View, Image} from 'react-native';

const icon = {
    menu: require('../source/img/menu.png'),
    plus: require('../source/img/plusActive.png'),
    back: require('../source/img/back.png'),
    check: require('../source/img/check-symbol.png'),
    addUser: require('../source/img/add-user.png'),
    edit: require('../source/img/edit.png')
}

const Header = (props) => {
    return(
        <View style={styles.header}>
           {props.leftIcon && <TouchableOpacity style={[styles.bt, {left: 16}]} onPress={() => props.leftAction()}>
                <Image style={styles.icon} source={icon[props.leftIcon]}/>
            </TouchableOpacity> } 
            <Text style={styles.title}>{props.title}</Text>
            {props.rightIcon || props.rightText  ? <TouchableOpacity style={[styles.bt, {right: 16}]} onPress={() => props.rightAction()}>
                {props.rightIcon  ? <Image style={[styles.icon, props.sizeIcon && {width: props.sizeIcon, height: props.sizeIcon}]} 
                    source={icon[props.rightIcon]}/> : <Text style={styles.rightText}>{props.rightText}</Text>}
            </TouchableOpacity> : null } 
        </View>
    )
}

const styles = StyleSheet.create({
    header: {
        width: '100%',
        height: 55,
        position: 'relative',
        alignItems: 'center',
        justifyContent: 'center',
       // backgroundColor: '#fff',
        flexDirection: 'row',
    },
    title: {
        fontSize: 18,
        fontWeight: '500',
        color: '#4d4d4f',
        paddingHorizontal: 48
    },
    icon: {
        width: 20,
        height: 20,
        resizeMode: 'contain'
    },
    bt: {
        position: 'absolute',
        padding: 10
    },
    rightText: {
        color: '#42bec8',
        fontSize: 14,
        textAlign: 'center',
        maxWidth: 110
    }
});

export default Header;