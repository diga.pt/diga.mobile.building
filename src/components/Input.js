import React from 'react';

import {View, Text, StyleSheet, TextInput} from 'react-native';

const Input = (props) => {
    return(
        <View style={{}}>
            {!props.hideLable && <Text style={styles.label}>{props.label}</Text>}
            <View style={styles.inputContainer}>
                <TextInput
                    placeholder={props.placeholder ? props.placeholder : props.label}
                    style={styles.input}
                    placeholderTextColor={'#7E8086'}
                    editable={props.editable}
                    value={props.value}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    inputContainer: {
        width: '100%',
        height: 50,
        backgroundColor: '#dedede',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        position: 'relative',
        fontWeight: '500',
        backgroundColor: '#fff',
        borderRadius: 50,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    input: {
        flex: 1,
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 50,
        paddingHorizontal: 16,
        fontSize: 15
    },
    label: {
        paddingHorizontal: 16,
        marginTop: 15,
        fontWeight: '500',
        fontSize: 14,
        color: '#4D5860',
    },
});

export default Input;