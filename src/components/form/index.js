
import React, { useState } from 'react';
import { StyleSheet, View, Text, TextInput, Image, TouchableOpacity, Dimensions } from 'react-native';
let deviceWidth = Dimensions.get('window').width;
import RNPickerSelect from 'react-native-picker-select';


export const renderInput = ({ input: { onChange, meta, ...restInput }, ...props }) => {
    return (
      <View style={styles.inputWrapper}>
        {props.label && <Text style={styles.label}>{props.label}</Text>}
      <View style={[styles.inputContainer, props.shadow && styles.shadow]}>
        <TextInput
          onChangeText={onChange}
          placeholder={props.placeholder}
          {...restInput}
          keyboardType={props.type}
          style={styles.input}
          placeholderTextColor={'#7E8086'}
          autoCapitalize="none"
          autoCorrect={false} 
        />
        {props.subdomain && <Text style={styles.subdomain}>*.diga.pt</Text>}
      </View>
      {props.meta.touched && (props.meta.error && <Text style={styles.error}>{props.meta.error}</Text>)}
      </View>
    )
  }

  export const renderPasswordInput = ({ input: { onChange, meta, ...restInput }, ...props }) => {
    const [secureTextEntry, setSecureTextEntry] = useState(true);

    return (
      <View style={styles.inputWrapper}>
        {props.label && <Text style={styles.label}>{props.label}</Text>}
      <View style={[styles.inputContainer, props.shadow && styles.shadow]}>
        <TextInput
          onChangeText={onChange}
          placeholder={props.placeholder}
          {...restInput}
          keyboardType={props.type}
          style={styles.input}
          placeholderTextColor={'#7E8086'}
          secureTextEntry={secureTextEntry}
          autoCapitalize="none"
          autoCorrect={false} 
        />
        <TouchableOpacity style={styles.showPassword}
                                        onPress={() => {
                                            setSecureTextEntry(secureTextEntry ? false : true)
                                        }}
                                  activeOpacity={1}>
                            <Image style={styles.showPasswordIcon} source={secureTextEntry ? require('../../source/img/eye.png') : require('../../source/img/hideEye.png')}/>
        </TouchableOpacity>
      </View>
      {props.meta.touched && (props.meta.error && <Text style={styles.error}>{props.meta.error}</Text>)}
      </View>
    )
  }

  export const renderSelect = (props) => {
    return (
      <View style={styles.inputWrapper}>
        {props.label && <Text style={styles.label}>{props.label}</Text>}
      <View style={[styles.inputContainer, {justifyContent: 'center'}]}>
        <RNPickerSelect
                    onValueChange={(value) => props.input.onChange(value)}
                    //value={restInput.value}
                    itemKey={props.input.value}
                    items={props.items}  
                    style={props.shadow ? pickerSelectStylesShadow : pickerSelectStyles}
                    placeholder={{label: props.placeholder}}
                />
      </View>
      {props.meta.touched && props.meta.error && <Text style={styles.error}>{props.meta.error}</Text>}
      </View>
    )
  }
  


  const styles = StyleSheet.create({
    input: {
      flex: 1,
      height: 50,
      backgroundColor: '#fff',
      borderRadius: 50,
      paddingHorizontal: 16,
      fontSize: 15
  },
  inputWrapper: {
    width: '100%',
    marginBottom: 15,
    zIndex: 1,
  },
  inputContainer: {
      width: '100%',
      height: 50,
      backgroundColor: '#fff',
      borderRadius: 50,
      flexDirection: 'row',
      alignItems: 'center',
      position: 'relative',
      fontWeight: '500',
      
  },
    error: {
        color: '#F44336',
        fontSize: 12,
        paddingTop: 5,
        paddingHorizontal: 16,
    },
    subdomain: {
      paddingRight: 16,
      paddingLeft: 8,
      fontSize: 15,
      fontWeight: '500',
      color: '#42bec8'
  },
  shadow: {
    shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
  },
  label: {
    paddingHorizontal: 16,
    //marginTop: 15,
    fontWeight: '500',
    fontSize: 14,
    color: '#4D5860',
    paddingBottom: 8,

},
showPasswordIcon: {
  width: 24,
  height: 24,
  resizeMode: 'contain',
  opacity: .7
},
showPassword: {
  position: 'absolute',
  right: 16
}
  })

  const pickerSelectStylesShadow = StyleSheet.create({
    inputIOS: {
        backgroundColor: '#fff',
        borderRadius: 50,
        paddingHorizontal: 16,
        fontSize: 15,
        height: 50,
        width: deviceWidth-32,
        color: '#000',
        borderRadius: 50,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    inputAndroid: {
        backgroundColor: '#fff',
        borderRadius: 50,
        paddingHorizontal: 16,
        fontSize: 15,
        height: 50,
        width: deviceWidth-48,
        color: '#000',
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    placeholder: {
        color: '#7E8086',
        fontSize: 15,
    },

  });

  const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        //backgroundColor: '#fff',
        borderRadius: 50,
        paddingHorizontal: 16,
        fontSize: 15,
        height: 50,
        width: deviceWidth-32,
        color: '#000',
        borderRadius: 50,
    },
    inputAndroid: {
        //backgroundColor: '#fff',
        borderRadius: 50,
        paddingHorizontal: 16,
        fontSize: 15,
        height: 50,
        width: deviceWidth-48,
        color: '#000',
    },
    placeholder: {
        color: '#7E8086',
        fontSize: 15,
    }
  });