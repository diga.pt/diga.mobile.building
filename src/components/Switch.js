import React from 'react';
import { Switch } from 'react-native-switch';
import {View, Text, StyleSheet} from 'react-native';

const CustomSwitch = (props) => {
    return(
        <View style={styles.btContainer}>
                <Text style={styles.btText}>{props.title}</Text>
                <Switch
                    value={props.data ? props.data : false}
                    onValueChange={(val) => {
                        props.action(val);
                    }}
                    disabled={false}
                    activeText={'On'}
                    inActiveText={'Off'}
                    circleSize={16}
                    barHeight={24}
                    circleBorderWidth={0}
                    backgroundActive={'#b5b5b557'}
                    backgroundInactive={'#b5b5b557'}
                    circleActiveColor={'#42bec8'}
                    circleInActiveColor={'#42bec8'}
                    changeValueImmediately={true}
                    changeValueImmediately={true} 
                    innerCircleStyle={{ alignItems: "center", justifyContent: "center" }} 
                    outerCircleStyle={{}}
                    renderActiveText={false}
                    renderInActiveText={false}
                    switchLeftPx={-2}
                    switchRightPx={-2}
                    switchWidthMultiplier={3}
                    switchBorderRadius={30}
                />
            </View> 
    )
}

const styles = StyleSheet.create({
    btContainer: {
       // flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    btText: {
        fontWeight: '500',
        fontSize: 16,
        color: '#4D5860',
    },
});

export default CustomSwitch;