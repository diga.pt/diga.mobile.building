import React from 'react';

import {Text, StyleSheet, TouchableOpacity, ActivityIndicator} from 'react-native';

const CustomButton = (props) => {
    return(
        <TouchableOpacity disabled={props.load} onPress={() => props.action()} 
                        style={[styles.bt, {backgroundColor: props.load ? '#308e96' : '#42bec8'}]}>
                {props.load ?  <ActivityIndicator size="small" color="#fff"/> : <Text style={styles.btText}>{props.title}</Text>}
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    bt: {
        width: '100%',
        height: 50,
        backgroundColor: '#42bec8',
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    btText: {
        color: '#fff',
        fontSize: 18,
        fontWeight: '500'
    }
});

export default CustomButton;