import React, { useState, useEffect } from 'react';
import {
  TouchableOpacity,
  Image,
  StyleSheet,
  Dimensions,
  View,
  SafeAreaView,
  Text,
  ScrollView,
  Linking
} from 'react-native';
import {
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import AsyncStorage from '@react-native-community/async-storage';

import I18n from '../i18n/i18n';
import { useDispatch } from "react-redux";
import {updateStatusAuth} from '../reducers/User';
import Auth0 from 'react-native-auth0';

const auth0 = new Auth0({
    domain: 'diga.eu.auth0.com',
    clientId: 'In9799EXJYQi4UqZSVS6T4r49d2LDdYg'
  });

const deviceWidth = Math.round(Dimensions.get('window').width);
const deviceHeight = Math.round(Dimensions.get('window').height);

const CustomDrawerContent = ({ setCurrentUser, currentUser, progress, ...rest }) => {

  const dispatch = useDispatch();

  const logOut =  async () => {
  auth0.webAuth
   .clearSession({ scope: 'openid profile email', audience:'https://diga.pt'})
   .then(success => {
        dispatch(updateStatusAuth('auth'));
        AsyncStorage.removeItem('@user');
        AsyncStorage.removeItem('@users');
        AsyncStorage.removeItem('@reports');
   })
   .catch(error => {
       console.log('Log out cancelled');
   });
    
}

  return (
    <>

      <View style={{flex: 1,  backgroundColor:'#FFF', height: '100%',}}>
        <View  style={{ height: deviceHeight * 0.25, flexDirection:'column', alignItems:'center', justifyContent:'center'}}>
            <Image source ={require('../source/img/logo.png')} style={{width:(deviceWidth * 0.7 + 20)/2.5,  resizeMode:'contain'}}/>
        </View>
        <ScrollView>
        <DrawerItemList {...rest} />
        <DrawerItem
          label={I18n.t('about')}
          onPress={() => Linking.openURL('http://new.diga.pt/home/AboutUs')}
          icon={() => (
            <Image style={{width: 23, height: 23}} source={require('../source/img/information.png')}/>
          )}
          labelStyle={styles.label}
        />
        <DrawerItem
          label={I18n.t('contactSupport')}
          onPress={() => Linking.openURL('mailto:geral@diga.pt')}
          icon={() => (
            <Image style={{width: 23, height: 23}} source={require('../source/img/message.png')}/>
          )}
          labelStyle={styles.label}
        />
        <TouchableOpacity style={{marginTop: 25}} onPress={() => logOut()}>
            <View style={{ paddingLeft: 18, paddingVertical: 16, flexDirection:'row', alignItems:'center', justifyContent:'flex-start', borderTopColor: 'rgba(84, 84, 88, 0.45);', borderTopWidth: 1}}>
                <Image source={require('../source/img/logout.png')} style={{width: 20, height: 20}}></Image>
                <View style={{width: 32}}></View>
                <Text style={styles.label}>{I18n.t('logOut')}</Text>
            </View>
        </TouchableOpacity>
        </ScrollView>
      </View>
    </>
  );
};



export default CustomDrawerContent;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'#fff'
  },
  bg: {
    position: 'absolute',
    right: 0,
    backgroundColor:'#FFF'
  },
  profileInfo: {
    paddingTop: 15,
    paddingBottom: 30,
    paddingLeft: 18,
  },
  avatar: {
    borderRadius: 50,
    marginBottom: 10,
  },
  profileName: {
    fontSize: 14,
    color: 'rgba(0, 0, 0, 0.87)',
    marginBottom: 3,
  },
  profilePhone: {
    fontSize: 14,
    color: '#000',
  },
  label: {
      color: '#3A3A3C',
      fontSize: 16,
      fontWeight: 'normal',
  }
});
