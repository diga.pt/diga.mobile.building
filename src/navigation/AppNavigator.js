import React, {setRef} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import CustomDrawerContent from './CustomDrawerContent';
import {Image, SafeAreaView, Text} from 'react-native';
import MyTabBar from './MyTabBar';

import AuthStart from '../scenes/auth/Start';
import Login from '../scenes/auth/Login';
import SignUp from '../scenes/auth/SignUp';
import ForgotPassword from '../scenes/ForgotPassword/ForgotPassword';

import Users from '../scenes/Users';
import Settings from '../scenes/Settings';
import InOut from '../scenes/InOut';
import Logs from '../scenes/Logs';
import AddUser from '../scenes/AddUser/AddUser';
import LogInfo from '../scenes/LogInfo';
import Search from '../scenes/Search';
import Branch from '../scenes/Branch';
import Languages from '../scenes/Languages';
import WorkSchedule from '../scenes/WorkSchedule';

//

import Contacts from '../scenes/CRM/Contacts/Contacts';
import UsersList from '../scenes/CRM/Users/Users';
import Calendar from '../scenes/CRM/Calendar/Calendar';
import AddTask from '../scenes/CRM/Calendar/AddTask';
import User from '../scenes/CRM/Users/User';
import Client from '../scenes/CRM/Contacts/Client';
import Funnel from '../scenes/CRM/Funnel/Funnel';
import NewService from '../scenes/CRM/Service/NewService';
import EditClient from '../scenes/CRM/Contacts/EditClient';
import AddContact from '../scenes/CRM/Contacts/AddContact';
import Responsible from '../scenes/CRM/Calendar/Responsible';
import Task from '../scenes/CRM/Calendar/Task';
import Estimates from '../scenes/CRM/Estimates/Estimates';
import Request from '../scenes/CRM/Contacts/Request';
import Invoice from '../scenes/CRM/Contacts/Invoice';
import AddInvoice from '../scenes/CRM/Contacts/AddInvoice';
//

import Preloader from '../components/Preloader';

import { useSelector } from "react-redux";

import I18n from '../i18n/i18n';


const icons = {
  Settings: require('../source/img/settings.png'),
  Home: require('../source/img/home.png'),
  Logs: require('../source/img/error.png'),
  'In/Out': require('../source/img/check-in.png'),
  about: require('../source/img/information.png'),
  contacts: require('../source/img/message.png'),
  workSchedule: require('../source/img/calendar.png'),
  contacts: require('../source/img/contact.png'),
  userList: require('../source/img/userList.png'),
  filter: require('../source/img/filter.png'),
  calendarM: require('../source/img/calendar_m.png'),
  timeTracker: require('../source/img/stopwatch.png'),
  estimates: require('../source/img/economy-forecast.png'),
}



const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tab = createBottomTabNavigator();


const AuthNavigator = (props) => {
  return (
      <Stack.Navigator
        screenOptions={{
          gestureEnabled: false,
        }}
      >
        <Stack.Screen
          name="AuthStart"
          component={AuthStart}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="SignUp"
          component={SignUp}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="MainNaw"
          component={MainNaw}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="ForgotPassword"
          component={ForgotPassword}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
              name="Languages"
              component={Languages}
              options={{
                headerShown: false,
              }}
            />
      </Stack.Navigator>
    )
  }

 
  
  const BottomTab = () => {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
      <Tab.Navigator tabBar={props => <MyTabBar {...props} />}
      animationEnabled={true}
      initialRouteName="Home">
        <Tab.Screen name="Home" component={Users}/>
        <Tab.Screen name="In/Out" component={InOut} />
        <Tab.Screen name="Logs" component={Logs} />
        <Tab.Screen name="Settings" component={Settings}/>
      </Tab.Navigator>
      </SafeAreaView>
    );
  }

  const  MainDrawer = () => {
    return (
      <Drawer.Navigator
        drawerContent={props => <CustomDrawerContent {...props} />}
        initialRouteName="MapNavigator"
        drawerContentOptions={{
            activeTintColor: '#2FCB75',
            activeBackgroundColor: '#FFF',
            labelStyle: {
              color: '#3A3A3C',
              fontSize: 16,
              fontWeight: 'normal',
            },
            itemStyle: {
              borderRadius: 0,
              width: '100%',
              marginVertical: 0,
              marginLeft: 0,
              paddingLeft: 10,
              paddingVertical: 5,
            },
          }}
       >
        <Drawer.Screen
            name="calendar"
            component={Calendar}
            options={{
              drawerLabel: I18n.t('home'),
              drawerIcon: () => (
                  <Image style={{width: 23, height: 23}} source={icons['Home']}/>
              ),
            }}
          />
          <Drawer.Screen
            name="workSchedule"
            component={WorkSchedule}
            options={{
              drawerLabel: I18n.t('workSchedule'),
              drawerIcon: () => (
                  <Image style={{width: 23, height: 23}} source={icons['workSchedule']}/>
              ),
            }}
          />
          <Drawer.Screen
            name="contacts"
            component={Contacts}
            options={{
              drawerLabel: I18n.t('contacts'),
              drawerIcon: () => (
                  <Image style={{width: 23, height: 23}} source={icons['contacts']}/>
              ),
            }}
          />
          <Drawer.Screen
            name="estimates"
            component={Estimates}
            options={{
              drawerLabel: 'Сметы',
              drawerIcon: () => (
                  <Image style={{width: 23, height: 23}} source={icons['estimates']}/>
              ),
            }}
          />
           <Drawer.Screen
            name="funnel"
            component={Funnel}
            options={{
              drawerLabel: I18n.t('funnel'),
              drawerIcon: () => (
                  <Image style={{width: 23, height: 23}} source={icons['filter']}/>
              ),
            }}
          />
         {/**<Drawer.Screen
            name="calendar"
            component={Calendar}
            options={{
              drawerLabel: I18n.t('calendar'),
              drawerIcon: () => (
                  <Image style={{width: 23, height: 23}} source={icons['calendarM']}/>
              ),
            }}
          />*/}
          <Drawer.Screen
            name="timeTracker"
            component={BottomTab}
            options={{
              drawerLabel: I18n.t('timeTracker'),
              drawerIcon: () => (
                  <Image style={{width: 23, height: 23}} source={icons['timeTracker']}/>
              ),
            }}
          />
          <Drawer.Screen
            name="userlist"
            component={UsersList}
            options={{
              drawerLabel: I18n.t('users'),
              drawerIcon: () => (
                  <Image style={{width: 23, height: 23}} source={icons['userList']}/>
              ),
            }}
          />
          <Drawer.Screen
            name="settings"
            component={Settings}
            options={{
              drawerLabel: I18n.t('settings'),
              drawerIcon: () => (
                  <Image style={{width: 23, height: 23}} source={icons['Settings']}/>
              ),
            }}
          />
        </Drawer.Navigator>
    )}


    const MainNaw = () => {
      return(
        <Stack.Navigator
          screenOptions={{
            gestureEnabled: false,
          }}>
            <Stack.Screen
              name="MainDrawer"
              component={MainDrawer}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="Search"
              component={Search}
              options={{
                headerShown: false,
                tabBarVisible: false
              }}
            />
            <Stack.Screen
              name="LogInfo"
              component={LogInfo}
              options={{
                headerShown: false,
                tabBarVisible: false
              }}
            />
            <Stack.Screen
              name="AddUser"
              component={AddUser}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="Branch"
              component={Branch}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="Languages"
              component={Languages}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="AuthNavigator"
              component={AuthNavigator}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="AddTask"
              component={AddTask}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="Task"
              component={Task}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="User"
              component={User}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="Client"
              component={Client}
              options={{
                headerShown: false,
              }}
            />
           <Stack.Screen
              name="NewService"
              component={NewService}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="EditClient"
              component={EditClient}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="AddContact"
              component={AddContact}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="Responsible"
              component={Responsible}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="Request"
              component={Request}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="Invoice"
              component={Invoice}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="AddInvoice"
              component={AddInvoice}
              options={{
                headerShown: false,
              }}
            />
          </Stack.Navigator>
      )
    }

  const  AppNavigator =  (props) => {

    const user = useSelector(state => state.user);

      console.log('-----------');
      console.log(user.statusAuth, 'user.statusAuth');
      console.log(user, 'user');
      console.log('-----------');


    return (
      !user.statusAuth ? <Preloader/> : <NavigationContainer>
            {user.statusAuth === 'auth' ? <AuthNavigator/> : !user.statusAuth ? <Preloader/> : <MainNaw/>}
      </NavigationContainer>
    );
  }

  export default AppNavigator;