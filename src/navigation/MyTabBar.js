import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';

import I18n from '../i18n/i18n';

const icons = {
    Settings: require('../source/img/settings.png'),
    Home: require('../source/img/home.png'),
    Logs: require('../source/img/error.png'),
    'In/Out': require('../source/img/arrows.png'),
  }

  const activeIcons = {
    Settings: require('../source/img/active-settings.png'),
    Home: require('../source/img/active-home.png'),
    Logs: require('../source/img/active-error.png'),
    'In/Out': require('../source/img/active-arrows.png'),
  }

function MyTabBar({ state, descriptors, navigation }) {
  const focusedOptions = descriptors[state.routes[state.index].key].options;

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  return (
    <View style={styles.container}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityStates={isFocused ? ['selected'] : []}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={styles.tab}
            activeOpacity={1}
            key={index}
          >
            <Image style={styles.icon} source={isFocused ? activeIcons[label] : icons[label]}/>
            <Text style={[styles.label, { color: isFocused ? '#42bec8' : '#222' }]}>
              {I18n.t(label)}
            </Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: 'auto',
        flexDirection: 'row',
        backgroundColor: '#fff',
        height: 55,
    },
    tab: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    icon: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        marginBottom: 2
    },
    label: {
        fontSize: 14,
        fontWeight: '500'
    }
})


export default MyTabBar;