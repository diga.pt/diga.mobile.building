
import {UserAPI} from '../api/user';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';

const UPDATE_USER = 'UPDATE-USER';
const UPDATE_AUTHORIZED_USER = 'UPDATE-AUTHORIZED-USER';
const UPDATE_STAUS_AUTH = 'UPDATE-STAUS-AUTH';
const UPDATE_ADITIONAL_USER = 'UPDATE_ADITIONAL_USER';

  
const INITIAL_STATE = {
  statusAuth: null,
  user: null,
  authorizedUser: null,
  additionalUser: null
};

const User = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case UPDATE_USER:
      return {
        ...state,
        user: action.data
      }
    case UPDATE_AUTHORIZED_USER:
        return {
          ...state,
          authorizedUser: action.data
        }
      case UPDATE_STAUS_AUTH:
      return {
        ...state,
        statusAuth: action.data
      }
      case UPDATE_ADITIONAL_USER:
      return {
        ...state,
        additionalUser: action.data
      }
    default: return state
  }
};


export const updateStatusAuth = (data) => ({type: UPDATE_STAUS_AUTH, data});

export const updateUser = (data) => ({type: UPDATE_USER, data});

export const updateAuthorizedUser = (data) => ({type: UPDATE_AUTHORIZED_USER, data});

export const updateAdditionalUser = (data) => ({type: UPDATE_ADITIONAL_USER, data});


export default User;
