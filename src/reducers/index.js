import {combineReducers, createStore, applyMiddleware} from 'redux';
import  thunkMiddleware from 'redux-thunk';
import { reducer as form } from 'redux-form';

import User from './User';
import General from './General';

let reducers = combineReducers({
    user: User,
    general: General,
    form
});



let store = createStore(reducers, applyMiddleware(thunkMiddleware));
export default store;