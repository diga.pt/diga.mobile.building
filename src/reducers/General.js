const UPDATE_SETTINGS = 'UPDATE-SETTINGS';

  
const INITIAL_STATE = {
  settings: {}
};

const General = (state = INITIAL_STATE, action) => {
  switch (action.type) {
      case UPDATE_SETTINGS:
      return {
        ...state,
        settings: {
          ...state.settings,
          ...action.data
        }
      }
    default: return state
  }
};


export const updateSettings = (data) => ({type: UPDATE_SETTINGS, data});


export default General;
