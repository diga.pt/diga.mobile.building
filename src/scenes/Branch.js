import React, {useEffect, useState} from 'react';

import LinearGradient from 'react-native-linear-gradient';

import {View, Text, StyleSheet, SafeAreaView, ScrollView,
        Dimensions, Image, TouchableOpacity, TextInput} from 'react-native';

import Header from '../components/Header';

let deviceWidth = Dimensions.get('window').width;

import {SettingsAPI} from '../api/settings';
import AsyncStorage from '@react-native-community/async-storage';

import {updateSettings} from '../reducers/General';

import { useDispatch } from "react-redux";

import Preloader from '../components/Preloader';

import I18n from '../i18n/i18n';



const Branch = (props) => {

    const dispatch = useDispatch();

    const [list, setList] = useState([]);

    const [load, setLoad] = useState(false);


    const [activeId, setActiveId] = useState(null);

    const saveData = async () => {
        let settings = await AsyncStorage.getItem('@settings');
            settings = settings === null ? {} : JSON.parse(settings);
        if(activeId){
            let data = list.find(item => item.id === activeId);
            if(data){
                if(settings === null){ 
                    settings['branch'] = data;
                } else {
                    settings = {
                        ...settings,
                        ['branch']: data
                    }
                }
                dispatch(updateSettings(settings));
                await AsyncStorage.setItem(`@settings`, JSON.stringify(settings));
                props.navigation.navigate('Settings');
            }
        }
    }

   
    useEffect(() => {
        setLoad(true);
        SettingsAPI.getBranch()
        .then(res => {
            console.log(res, 'res');
            if(res.errcode === 0){
                let branch = res.rows.map((item) => {
                    return { 
                        name: item.name,
                        id: item.id,
                    }
                })
                setList(branch);
                setLoad(false);
            }
        })
        .catch(err => {
            console.log(err, 'err');
            setLoad(false)
        })
    }, [])

    return(
        <>
        {load && <Preloader/>}
        <SafeAreaView style={{flex: 0, backgroundColor: '#FEFEFE'}}/>
        <LinearGradient colors={['#FEFEFE', '#F9F9F9']} style={styles.container}>
            <SafeAreaView style={styles.contentContainer}>
            <Header leftIcon='back' title={I18n.t('branch')}
                    leftAction={() => props.navigation.goBack()}
                    rightAction={() => saveData()}
                    rightIcon={'check'}/>
            <View style={{flex: 1, alignItems: 'center'}}>
            <ScrollView style={{flex: 1, width: '100%'}} showsVerticalScrollIndicator={false}>
            <View style={{flex: 1, paddingHorizontal: 16, paddingBottom: 20}}>
                {list.map((item, index) => {
                    return(
                        <TouchableOpacity key={index} 
                                style={[styles.item]}
                                onPress={() => {
                                    setActiveId(activeId !== item.id ? item.id : null)
                                }}
                                activeOpacity={1}
                                >
                            <Text style={styles.itemLabel}>{item.name}</Text>
                            {activeId === item.id && <Image style={styles.icon} source={require('../source/img/check-symbol.png')}/>}
                        </TouchableOpacity>
                    )
                })}
            </View>
            </ScrollView>
            </View>
            </SafeAreaView>
        </LinearGradient>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        width: deviceWidth,
        flex: 1,
        flexDirection:'column', 
        alignItems:'center', 
        justifyContent:'center'
    },
    contentContainer: {
        flex: 1,
        width: '100%',
    },
    input: {
        flex: 1,
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 50,
        paddingHorizontal: 16,
        fontSize: 15
    },
    inputWrapper: {
      width: deviceWidth-32,
      marginBottom: 12,
      marginTop: 8,
    },
    inputContainer: {
        width: '100%',
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 50,
        flexDirection: 'row',
        alignItems: 'center',
        position: 'relative',
        fontWeight: '500',
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    warning: {
        fontSize: 16, 
        fontWeight: '500',
        color: '#7E8086',
        textAlign: 'center',
        paddingVertical: 16
    },
    item: {
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 16,
        height: 48, 
        justifyContent: 'space-between',
        backgroundColor: '#FFF',
        borderRadius: 12,
        marginTop: 10,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    itemLabel: {
        fontSize: 14, 
        fontWeight: '500',
        color: '#7E8086',
    },
    active: {
        borderWidth: 2,
        borderColor: '#42bec8'
    },
    icon: {
        width: 22,
        height: 22,
        resizeMode: 'contain'
    }
});

export default Branch;