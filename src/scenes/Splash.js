import React from 'react';

import LinearGradient from 'react-native-linear-gradient';

import {View, Text, StyleSheet, 
        Dimensions, Image} from 'react-native';

let deviceWidth = Dimensions.get('window').width;

const Splash = () => {
    return(
        <LinearGradient colors={['#ebecec', '#d8e9ed']} style={styles.container}>
            <Image source={require('../source/img/logo.png')} style={styles.logo}/>
        </LinearGradient>
    )
}

const styles = StyleSheet.create({
    container: {
        width: deviceWidth,
        flex: 1,
        flexDirection:'row', 
        alignItems:'center', 
        justifyContent:'center'
    },
    logo: {
        width: 250,
        height: 115,
        resizeMode: 'contain'
    }
});

export default Splash;