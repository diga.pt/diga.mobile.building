import React, {useEffect, useState} from 'react';

import LinearGradient from 'react-native-linear-gradient';

import {View, Text, StyleSheet, SafeAreaView, ScrollView,
        Dimensions, Image, TouchableOpacity, TextInput} from 'react-native';

import Header from '../components/Header';

let deviceWidth = Dimensions.get('window').width;

import {SettingsAPI} from '../api/settings';
import AsyncStorage from '@react-native-community/async-storage';

import {updateSettings} from '../reducers/General';

import { useDispatch } from "react-redux";

import I18n from '../i18n/i18n';

import Toast from 'react-native-toast-message';

const Search = (props) => {

    const dispatch = useDispatch();

    const [searchList, setSearchList] = useState([]);

    const [searchText, setSearchText] = useState('');

    const [activeId, setActiveId] = useState(null);

    const saveData = async () => {
        let settings = await AsyncStorage.getItem('@settings');
            settings = settings === null ? {} : JSON.parse(settings);
        if(activeId){
            let name = props.route.params.name;
                name = name.toLowerCase();
            let data = searchList.find(item => item.id === activeId);
            if(data){
                if(settings === null){ 
                    settings[name] = data;
                } else {
                    settings = {
                        ...settings,
                        [name]: data
                    }
                }
                dispatch(updateSettings(settings));
                await AsyncStorage.setItem(`@settings`, JSON.stringify(settings));
                props.navigation.navigate('Settings');
            }
        }
    }

    const search = (data) => {
        setSearchText(data);
        let name = props.route.params.name;
        let API = name === 'Service' ? SettingsAPI.services : name === 'Estimate' ? SettingsAPI.estimates : SettingsAPI.contacts;

        API({query: name === 'Estimate' ? Number(data) : data})
            .then(res => {
                console.log(res, 'res');
                if(res.status !== 'error'){
                    let list = res.rows.map((item) => {
                        return {
                            name: name === 'Estimate' ? `${item.service.estimate_number} ${item.subject}` :item.name,
                            id: item.id
                        }
                    })
                    setSearchList(list);
                    setActiveId(null);
                } else {
                    Toast.show({
                        type: 'error',
                        position: 'bottom',
                        text1: I18n.t('attention'),
                        text2: I18n.t('permission'),
                        visibilityTime: 4000,
                        autoHide: true,
                        bottomOffset: 45
                      });
                }
              
            })
            .catch(err => {
                Toast.show({
                    type: 'error',
                    position: 'bottom',
                    text1: I18n.t('attention'),
                    text2: I18n.t('error'),
                    visibilityTime: 4000,
                    autoHide: true,
                    bottomOffset: 45
                  });
                console.log(err, 'err');
            })
        
    }


    return(
        <>
        <SafeAreaView style={{flex: 0, backgroundColor: '#FEFEFE'}}/>
        <LinearGradient colors={['#FEFEFE', '#F9F9F9']} style={styles.container}>
            <SafeAreaView style={styles.contentContainer}>
            <Header leftIcon='back' title={`${props.route.params.title}`}
                    leftAction={() => props.navigation.goBack()}
                    rightAction={() => saveData()}
                    rightIcon={'check'}/>
            <View style={{flex: 1, alignItems: 'center'}}>
            <View style={styles.inputWrapper}>
                <View style={styles.inputContainer}>
                    <TextInput
                        placeholder={I18n.t('search')}
                        style={styles.input}
                        placeholderTextColor={'#7E8086'}
                        value={searchText}
                        onChangeText={(e) => search(e)}
                        autoCapitalize="none"
                        autoCorrect={false} 
                    />
                </View>
            </View>
            <ScrollView style={{flex: 1, width: '100%'}} showsVerticalScrollIndicator={false}>
            <View style={{flex: 1, paddingHorizontal: 16, paddingBottom: 15}}>
                {searchList.length === 0 ? <Text style={styles.warning}>{I18n.t('startSearch')}</Text>
                :
                searchList.map((item, index) => {
                    return(
                        <TouchableOpacity key={index} 
                                style={[styles.item]}
                                onPress={() => {
                                    setActiveId(activeId !== item.id ? item.id : null)
                                }}
                                activeOpacity={1}
                                >
                            <Text style={styles.itemLabel}>{item.name}</Text>
                            {activeId === item.id && <Image style={styles.icon} source={require('../source/img/check-symbol.png')}/>}
                        </TouchableOpacity>
                    )
                })}
            </View>
            </ScrollView>
            </View>
            </SafeAreaView>
        </LinearGradient>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        width: deviceWidth,
        flex: 1,
        flexDirection:'column', 
        alignItems:'center', 
        justifyContent:'center'
    },
    contentContainer: {
        flex: 1,
        width: '100%',
    },
    input: {
        flex: 1,
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 50,
        paddingHorizontal: 16,
        fontSize: 15
    },
    inputWrapper: {
      width: deviceWidth-32,
      marginBottom: 12,
      marginTop: 8,
    },
    inputContainer: {
        width: '100%',
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 50,
        flexDirection: 'row',
        alignItems: 'center',
        position: 'relative',
        fontWeight: '500',
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    warning: {
        fontSize: 16, 
        fontWeight: '500',
        color: '#7E8086',
        textAlign: 'center',
        paddingVertical: 16
    },
    item: {
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 16,
        height: 48, 
        justifyContent: 'space-between',
        backgroundColor: '#FFF',
        borderRadius: 12,
        marginTop: 10,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    itemLabel: {
        fontSize: 14, 
        fontWeight: '500',
        color: '#7E8086',
    },
    active: {
        borderWidth: 2,
        borderColor: '#42bec8'
    },
    icon: {
        width: 22,
        height: 22,
        resizeMode: 'contain'
    }
});

export default Search;