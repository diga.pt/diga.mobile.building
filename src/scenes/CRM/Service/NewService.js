import React, {useEffect, useState} from 'react';

import LinearGradient from 'react-native-linear-gradient';

import {View, Text, StyleSheet, SafeAreaView, ScrollView,
        Dimensions, KeyboardAvoidingView, Platform} from 'react-native';

import Header from '../../../components/Header';

let deviceWidth = Dimensions.get('window').width;

import Preloader from '../../../components/Preloader';
import I18n from '../../../i18n/i18n';
import {CrmAPI} from '../../../api/crm';
import NewServiceForm from './NewServiceForm';
import Toast from 'react-native-toast-message';

const NewService = (props) => {

    let [load, setLoad] = useState(false);

    let [serviceTypes, setServiceTypes] = useState([]);
    let [allUsers, setAllUsers] = useState([]);
    let [serviceStates, setServiceStates] = useState([]);
    let [servicePriorities, setServicePriorities] = useState([]);


      useEffect(() => {
        setLoad(true);
        CrmAPI.getServiceTypes()
            .then(res => {
                let data = res.map(it => {
                    return {
                        label: it.title,
                        value: it.id,
                        key: it.id
                    }
                });
                setServiceTypes(data);
            })
            .catch(err => {
                console.log(err, 'err getServiceTypes');
            });

        CrmAPI.getAllUsers()
            .then(res => {
                let data = res.map(it => {
                    return {
                        label: it.name,
                        value: it.id,
                        key: it.id
                    }
                });
                setAllUsers(data);
            })
            .catch(err => {
                console.log(err, 'err getAllUsers');
            });

        CrmAPI.getServiceStates()
            .then(res => {
                let data = res.map(it => {
                    return {
                        label: it.name,
                        value: it.id,
                        key: it.id
                    }
                });
                setServiceStates(data);
            })
            .catch(err => {
                console.log(err, 'err getServiceStates');
            });

        CrmAPI.getServicePriorities()
            .then(res => {
                let data = res.map(it => {
                    return {
                        label: it.name,
                        value: it.id,
                        key: it.id
                    }
                });
                setServicePriorities(data);
                setLoad(false);
            })
            .catch(err => {
                console.log(err, 'err getServicePriorities');
            });

     }, []);


     const addService = (data) => {

        let json = {
            ...data,
            autocomplete_disabled: false,
            aru_id: null,
            estimate_summ: null,
            client_contact_id: props.route.params.id
        };
        
        setLoad(true);
        CrmAPI.addServices(json)
            .then(res => {
            console.log(res, 'res');
            setLoad(false);
            Toast.show({
                type: 'success',
                position: 'bottom',
                text1: I18n.t('attention'),
                text2: I18n.t('successData'),
                visibilityTime: 4000,
                autoHide: true,
                bottomOffset: 45
              });
              props.navigation.goBack();
        })
        .catch(err => {
            console.log(err, 'err');
            setLoad(false);
            Toast.show({
                type: 'error',
                position: 'bottom',
                text1: I18n.t('attention'),
              text2: I18n.t('error'),
                visibilityTime: 4000,
                autoHide: true,
                bottomOffset: 45
              });
        })

     }

    return(
        <KeyboardAvoidingView style={{flex: 1}}
        behavior={Platform.OS == "ios" ? "padding" : null}>
         {load && <Preloader/>}
        <SafeAreaView style={{flex: 0, backgroundColor: '#FEFEFE'}}/>
        <LinearGradient colors={['#FEFEFE', '#F9F9F9']} style={styles.container}>
            <SafeAreaView style={styles.contentContainer}>
            <Header leftIcon='back' title={'Новая услуга'}
                    leftAction={() => props.navigation.goBack()}
                    sizeIcon={24}
                />
            <View style={{flex: 1, alignItems: 'center'}}>
            <ScrollView style={{flex: 1, width: '100%'}} showsVerticalScrollIndicator={false}>
            <View style={{flex: 1, paddingBottom: 20,  width: '100%', paddingHorizontal: 16}}>
            
            <NewServiceForm serviceTypes={serviceTypes}
                            allUsers={allUsers}
                            serviceStates={serviceStates}
                            servicePriorities={servicePriorities}
                            addService={addService}
                        /> 
            </View>
            </ScrollView>
            </View>
            </SafeAreaView>
        </LinearGradient>
       </KeyboardAvoidingView>
    )
}

const styles = StyleSheet.create({
    container: {
        width: deviceWidth,
        flex: 1,
        flexDirection:'column', 
        alignItems:'center', 
        justifyContent:'center'
    },
    contentContainer: {
        flex: 1,
        width: '100%',
        justifyContent: 'center'
    },
    warning: {
        fontSize: 16, 
        fontWeight: '500',
        color: '#7E8086',
        textAlign: 'center',
        paddingVertical: 16
    },
    contactContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        width: deviceWidth-32,
        flex: 1,
        paddingHorizontal: 16,
        backgroundColor: '#FFF',
        borderRadius: 12,
        paddingVertical: 12,
        marginTop: 2,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        flexWrap: 'wrap',
        marginBottom: 8,
    },
    name: {
        fontSize: 17, 
        fontWeight: '500',
        color: '#7E8086',
    },
    id: {
        fontSize: 14, 
        fontWeight: '500',
        color: '#7E8086',
    },
    infoContainer: {
        flexDirection: 'column', 
        flex: 1,
        width: '100%',
        marginLeft: 15
    },
    title: {
        fontSize: 14, 
        fontWeight: '400',
        color: '#7E8086',
    },
    active: {
        borderWidth: 2,
        borderColor: '#42bec8'
    },
    contact: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
    },
    contactIcon: {
        width: 19,
        height: 19,
        resizeMode: 'contain'
    },
    contactText: {
        fontSize: 14, 
        fontWeight: '500',
        color: '#7E8086',
        paddingLeft: 4
    },
    inputContainer: {
        width: '100%',
        height: 45,
        backgroundColor: '#dedede',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        position: 'relative',
        fontWeight: '500',
        backgroundColor: '#fff',
        borderRadius: 50,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    input: {
        flex: 1,
        height: 45,
        backgroundColor: '#fff',
        borderRadius: 50,
        paddingHorizontal: 16,
        fontSize: 14
    },
    label: {
        paddingHorizontal: 16,
        marginTop: 15,
        fontWeight: '500',
        fontSize: 14,
        color: '#4D5860',
    },
    dateTitle: {
        fontWeight: '500',
        fontSize: 14,
        color: '#4D5860',
    },
    dateHeader: {
        paddingHorizontal: 16,
        width: '100%',
        marginTop: 8
    },
    dateContainer: {
        flexDirection: 'row',
        marginTop: 2,
        //marginBottom: 8
    },
    dateItem: {
        flex: 1,
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    date: {
        fontSize: 14,
    },
    icon: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        marginRight: 8
    },
});

export default NewService;