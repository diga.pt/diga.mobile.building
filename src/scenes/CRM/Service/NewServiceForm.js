import React, {useEffect, useState} from 'react';

import LinearGradient from 'react-native-linear-gradient';

import {View, Text, StyleSheet, SafeAreaView, ScrollView,
        Dimensions, Image, TouchableOpacity, TextInput, FlatList} from 'react-native';

import Header from '../../../components/Header';
import { useDispatch } from "react-redux";
import Preloader from '../../../components/Preloader';
import I18n from '../../../i18n/i18n';
import CustomButton from '../../../components/CustomButton';
import {Autocomplete} from '../../../components/Autocomplete';
import { renderInput, renderSelect} from '../../../components/form';
import { reduxForm, Field, reset} from 'redux-form';
import {required, email} from '../../../components/form/validator';

let deviceWidth = Dimensions.get('window').width;


const NewServiceForm = (props) => {

    let [load, setLoad] = useState(false);
    const { handleSubmit, dispatch } = props;

    const addService = (data) => {
        props.addService(data);
    }
    
    return(
            <View style={styles.dataContainer}> 
                <Field  name={`name`}
                        component={renderInput}
                        label={'Название'}
                        placeholder={'Название'}
                        validate={[required]}
                        type={'default'}
                        shadow={true}
                        />
                <Field  name={`address`}
                        component={renderInput}
                        label={'Адрес'}
                        placeholder={'Адрес'}
                        validate={[required]}
                        type={'default'}
                        shadow={true}
                        />
                <Field  name={`service_type_id`}
                        component={renderSelect}
                        label={'Вид услуг'}
                        placeholder={'Вид услуг'}
                        validate={[required]}
                        type={'default'}
                        shadow={true}
                        items={props.serviceTypes}
                    />
                <Field  name={`service_priority_id`}
                        component={renderSelect}
                        label={'Приоритет'}
                        placeholder={'Приоритет'}
                        validate={[required]}
                        type={'default'}
                        shadow={true}
                        items={props.servicePriorities}
                    />
                <Field  name={`responsible_user_id`}
                        component={renderSelect}
                        label={'Ответственный'}
                        placeholder={'Ответственный'}
                        validate={[required]}
                        type={'default'}
                        shadow={true}
                        items={props.allUsers}
                    />
                <Field  name={`service_state_id`}
                        component={renderSelect}
                        label={'Статус'}
                        placeholder={'Статус'}
                        validate={[required]}
                        type={'default'}
                        shadow={true}
                        items={props.serviceStates}
                    />
                <Field  name={`note`}
                        component={renderInput}
                        label={'Заметка'}
                        placeholder={'Заметка'}
                        validate={[required]}
                        type={'default'}
                        shadow={true}
                        />
                <CustomButton action={handleSubmit(addService)} title={I18n.t('add')}/>
            </View>
    )
}

const styles = StyleSheet.create({
    dataContainer: {
        flex: 1,
        //paddingHorizontal: 16,
        paddingTop: 10,
        paddingBottom: 5
    },
});

export default reduxForm({
    form: 'NewServiceForm',
    enableReinitialize: true
  })(NewServiceForm);

