import React, {useEffect, useState} from 'react';

import {View, StyleSheet, Dimensions, TouchableOpacity, Text,
        Image} from 'react-native';

import I18n from '../../../i18n/i18n';
import CustomButton from '../../../components/CustomButton';
import {Autocomplete} from '../../../components/Autocomplete';
import { renderInput, renderSelect} from '../../../components/form';
import { reduxForm, Field, FieldArray} from 'redux-form';
import {required, email} from '../../../components/form/validator';


const AddInvoiceForm = (props) => {

    const { handleSubmit, dispatch } = props;

    const addContact = (data) => {
        props.addContact(data);
    }

    return(
            <View style={styles.dataContainer}> 
                <Field  name={`invoice_number`}
                        component={renderInput}
                        label={'Инвойс №'}
                        placeholder={'Инвойс №'}
                        validate={[required]}
                        type={'default'}
                        shadow={true}
                        />
                <Field  name={`supplier`}
                        component={renderInput}
                        label={'Поставщик'}
                        placeholder={'Поставщик'}
                        validate={[required]}
                        type={'default'}
                        shadow={true}
                        />
                <Field  name={`date`}
                        component={renderInput}
                        label={'Дата'}
                        placeholder={'Дата'}
                        validate={[required]}
                        type={'default'}
                        shadow={true}
                        />
                <Field  name={`total_without_vat`}
                        component={renderInput}
                        label={'Базовая стоимость'}
                        placeholder={'Базовая стоимость'}
                        validate={[required]}
                        type={'numeric'}
                        shadow={true}
                        />
                <Field  name={`vat_type`}
                        component={renderInput}
                        label={'Сумма НДС'}
                        placeholder={'Сумма НДС'}
                        validate={[required]}
                        type={'numeric'}
                        shadow={true}
                        />
                <Field  name={`total`}
                        component={renderInput}
                        label={'Общая сумма'}
                        placeholder={'Общая сумма'}
                        validate={[required]}
                        type={'numeric'}
                        shadow={true}
                        />

                <Field  name={`client_contact_id`}
                        component={Autocomplete}
                        label={'Контакт'}
                        placeholder={'Контакт'}
                        shadow={true}
                        data={props.contacts}
                        search={props.search}
                        setFocus={(type) => props.setFocus(type)}
                        type={'contacts'}
                        activeInput={props.activeInput}
                        index={9998}
                        statusDisabled={props.clientName ? false : true}
                        defaultValue={props.clientName ? props.clientName : ''}
                />
                
                <Field  name={`service_id`}
                        component={Autocomplete}
                        label={'Услуга'}
                        placeholder={'Услуга'}
                        shadow={true}
                        data={props.services}
                        search={props.search}
                        setFocus={(type) => props.setFocus(type)}
                        type={'services'}
                        activeInput={props.activeInput}
                        index={9997}
                        statusDisabled={true}
                        defaultValue={''}
                        />
                <Field  name={`estimate_id`}
                        component={renderInput}
                        label={'Смета'}
                        placeholder={'Смета'}
                        validate={[required]}
                        type={'default'}
                        shadow={true}
                        />
                <TouchableOpacity style={[styles.addBt, {paddingBottom: 15}]} onPress={() => props.uploadFile()}>
                        <Text style={styles.addText}>Добавить файл</Text>
                </TouchableOpacity>
                <CustomButton action={handleSubmit(addContact)} title={I18n.t('add')}/>
            </View>
    )
}

const styles = StyleSheet.create({
    dataContainer: {
        flex: 1,
        paddingHorizontal: 16,
        paddingTop: 10,
        paddingBottom: 5
    },
    label: {
        paddingHorizontal: 16,
        //marginTop: 15,
        fontWeight: '500',
        fontSize: 14,
        color: '#4D5860',
        paddingBottom: 8,
    },
    addText: {
        fontSize: 18,
        color: '#42bec8',
        paddingVertical: 8,
        fontWeight: '600',
        paddingRight: 8,
    },
    addIcon: {
        width: 16,
        height: 16
    },
    addBt: {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center'
    },
    deleteIcon: {
        height: 24,
        resizeMode: 'contain'  
    },
    delete: {
            height: 28,
            width: 28,
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: -15,
            marginLeft: 8,
    },
});

export default reduxForm({
    form: 'AddInvoiceForm',
    enableReinitialize: true
  })(AddInvoiceForm);

