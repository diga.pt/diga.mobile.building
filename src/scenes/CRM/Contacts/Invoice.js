import React, {useEffect, useState} from 'react';

import LinearGradient from 'react-native-linear-gradient';

import {View, Text, StyleSheet, SafeAreaView, ScrollView,
        Dimensions, Image, TouchableOpacity, TextInput, FlatList,
        ActivityIndicator, Linking} from 'react-native';

import Header from '../../../components/Header';

let deviceWidth = Dimensions.get('window').width;
import { useDispatch, useSelector } from "react-redux";
import Preloader from '../../../components/Preloader';
import I18n from '../../../i18n/i18n';
import {CrmAPI} from '../../../api/crm';
import { useIsFocused } from "@react-navigation/native";


const Invoice = (props) => {

    let [load, setLoad] = useState(false);
    let [loadMore, setLoadMore] = useState(false);
    let [invoices, setInvoices] = useState([]);
    let [offset, setOffsete] = useState(0);
    let [total, setTotal] = useState(0);
    let [searchText, setSearchText] = useState('');
    const settings = useSelector(state => state.general.settings);


    const isFocused = useIsFocused();
    useEffect(() => {
        console.log(isFocused, 'isFocused');
        getInvoice('');
      }, [isFocused]);

    const getInvoice = (data) => {
        setLoad(true);
        CrmAPI.getInvoice({service_id: props.route.params.service_id})
        .then(res => {
            setLoad(false);
            setInvoices(res.rows);
            setTotal(res.total);
            console.log(res, 'res');
        })
        .catch(err => {
            setLoad(false);
            console.log(err, 'err');
        });
    };

    const loadMoreContacts = () => {
        if(contacts.length < total){
            setLoadMore(true);
            CrmAPI.getInvoice({offset: offset+10, limit: 10, responsible_user_id: 0, query: searchText})
            .then(res => {
                setLoadMore(false);
                setInvoices([...contacts, ...res.rows]);
                setOffsete(offset+10);
                console.log(res, 'loadMoreContacts');
            })
            .catch(err => {
                setLoadMore(false);
                console.log(err, 'err');
            });
        };
    };

    const openLink = async (url_file) => {
        let domain = 'diga.pt';
            if(settings.subdomain === 'besterp'){
                    domain = 'rkesa.pt';
                }
            let url = `https://${settings.subdomain}.${domain}${url_file}`;
            await Linking.openURL(url);
    }
    

console.log(props, 'props');     

    return(
        <>
        {load && <Preloader/>}
        <SafeAreaView style={{flex: 0, backgroundColor: '#FEFEFE'}}/>
        <LinearGradient colors={['#FEFEFE', '#F9F9F9']} style={styles.container}>
            <SafeAreaView style={styles.contentContainer}>
            <Header leftIcon='back' title={'Фактуры'}
                    leftAction={() => props.navigation.goBack()}
                    //rightAction={() => props.navigation.navigate('AddInvoice')}
                    //rightIcon={'plus'}
                    sizeIcon={24}
                />
            <View style={{flex: 1, alignItems: 'center'}}>
            <View style={{flex: 1, width: '100%'}}>
            <View style={{flex: 1, paddingBottom: 20, alignItems: 'center'}}>
                <FlatList
                    data={invoices}
                    keyExtractor={(item, index) => String(index)}
                    /*onEndReachedThreshold={0}
                    onEndReached={data => {
                        if(!loadMore){
                            loadMoreContacts();
                        }
                    }}*/
                    renderItem={data => {
                        let item = data.item;
                        return(
                    <TouchableOpacity onPress={() => props.navigation.navigate('Client', {id: item.id})} style={styles.contactContainer}>
                    <View style={styles.infoContainer}>
                        <Text style={styles.name}>№{item.invoice_number}</Text>
                        <Text style={styles.id}>#{item.id}</Text>
                    </View>
                    <View style={styles.infoContainer}>
                        <Text style={styles.title}>Поставщик</Text>
                        <Text style={styles.contactText}>{item.supplier}</Text>
                    </View>
                    <View style={styles.infoContainer}>
                        <Text style={styles.title}>Дата</Text>
                        <Text style={styles.contactText}>{item.date}</Text>
                    </View>
                    <View style={styles.infoContainer}>
                        <Text style={styles.title}>Всего</Text>
                        <Text style={styles.contactText}>{item.total}</Text>
                    </View>
                    <View style={styles.infoContainer}>
                        <Text style={styles.title}>Инвойс</Text>
                        <Text onPress={() => openLink(item.invoice_file)} style={[styles.contactText, {color: '#42bec8'}]}>Показать</Text>
                    </View>
                </TouchableOpacity>
                        )
                    }}
                />
                {loadMore && <ActivityIndicator style={{marginTop: 15}} size="small" color="#42bec8"/>}
            </View>
            </View>
            </View>
            </SafeAreaView>
        </LinearGradient>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        width: deviceWidth,
        flex: 1,
        flexDirection:'column', 
        alignItems:'center', 
        justifyContent:'center'
    },
    contentContainer: {
        flex: 1,
        width: '100%',
        justifyContent: 'center'
    },
    warning: {
        fontSize: 16, 
        fontWeight: '500',
        color: '#7E8086',
        textAlign: 'center',
        paddingVertical: 16
    },
    contactContainer: {
        alignItems: 'center',
        flexDirection: 'column',
        width: deviceWidth-32,
        flex: 1,
        //paddingHorizontal: 16,
        backgroundColor: '#FFF',
        borderRadius: 12,
        paddingVertical: 12,
        marginTop: 2,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        flexWrap: 'wrap',
        marginBottom: 8,
    },
    name: {
        fontSize: 17, 
        fontWeight: '500',
        color: '#7E8086',
    },
    id: {
        fontSize: 14, 
        fontWeight: '500',
        color: '#7E8086',
        flexShrink: 1,
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    infoContainer: {
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        //alignItems: 'center', 
        flex: 1,
        width: '100%',
        marginBottom: 8,
        position: 'relative',
        paddingHorizontal: 16,
    },
    title: {
        fontSize: 14, 
        fontWeight: '400',
        color: '#7E8086',
    },
    active: {
        borderWidth: 2,
        borderColor: '#42bec8'
    },
    contact: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
    },
    contactIcon: {
        width: 19,
        height: 19,
        resizeMode: 'contain'
    },
    contactText: {
        fontSize: 14, 
        fontWeight: '500',
        color: '#7E8086',
        paddingLeft: 4
    },
    inputContainer: {
        width: '100%',
        height: 45,
        backgroundColor: '#dedede',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        position: 'relative',
        fontWeight: '500',
        backgroundColor: '#fff',
        borderRadius: 50,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        marginBottom: 15
    },
    input: {
        flex: 1,
        height: 45,
        backgroundColor: '#fff',
        borderRadius: 50,
        paddingHorizontal: 16,
        fontSize: 14
    },
    label: {
        paddingHorizontal: 16,
        marginTop: 15,
        fontWeight: '500',
        fontSize: 14,
        color: '#4D5860',
    },
    list: {
        flexDirection: 'row',
        paddingLeft: 20,
        flexWrap: 'wrap',
        flexShrink: 1,
        
    },
    service: {
        flexShrink: 1,
    }
});

export default Invoice;