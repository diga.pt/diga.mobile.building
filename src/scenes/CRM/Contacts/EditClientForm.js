import React, {useEffect, useState} from 'react';

import {View, StyleSheet, Dimensions, TouchableOpacity, Text,
        Image} from 'react-native';

import I18n from '../../../i18n/i18n';
import CustomButton from '../../../components/CustomButton';
import {Autocomplete} from '../../../components/Autocomplete';
import { renderInput, renderSelect} from '../../../components/form';
import { reduxForm, Field, FieldArray} from 'redux-form';
import {required, email} from '../../../components/form/validator';


const addMore = (props) => {
        return(
                <View>
                        <Text style={styles.label}>{props.label}</Text>
                        {props.fields.map((member, index) => {
                                return(
                                        <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                                        <View style={{flexShrink: 1, flex: 1}}>
                                        <Field  name={`${member}.${props.nameField}`}
                                                component={renderInput}
                                                placeholder={`${props.label} #${index+1}`}
                                                validate={[required]}
                                                type={'default'}
                                                shadow={true}
                                        />    
                                        </View>
                                        <TouchableOpacity onPress={() => {
                                                if(props.fields.length > 1){
                                                        props.fields.remove(index);
                                                }
                                                }} style={styles.delete}>
                                                <Image style={styles.deleteIcon} source={require('../../../source/img/delete.png')}/>
                                        </TouchableOpacity>
                                        </View>
                                )
                        })}
                        <TouchableOpacity onPress={() => props.fields.push({})} style={styles.addBt}>
                                <Text style={styles.addText}>Add</Text>
                                <Image style={styles.addIcon} source={require('../../../source/img/plusActive.png')}/>
                        </TouchableOpacity>
                </View>
        )
};

const EditClientForm = (props) => {

    const { handleSubmit, dispatch } = props;

    const save = (data) => {
        props.save(data);
    }

    return(
            <View style={styles.dataContainer}> 
                <Field  name={`name`}
                        component={renderInput}
                        label={'Имя'}
                        placeholder={'Имя'}
                        validate={[required]}
                        type={'default'}
                        shadow={true}
                        />
                <Field  name={`surname`}
                        component={renderInput}
                        label={'Фамиля'}
                        placeholder={'Фамиля'}
                        type={'default'}
                        shadow={true}
                        />
                <Field  name={`sex`}
                        component={renderSelect}
                        label={'Пол'}
                        placeholder={'Пол'}
                        type={'default'}
                        shadow={true}
                        items={[
                            { label: 'Мужчина', value: '0', key: '0'},
                            { label: 'Женщина', value: '1', key: '1' }
                        ]}
                        />
                <FieldArray 
                        label={'Email'} 
                        name="client_contact_emails" 
                        component={addMore}
                        nameField={'email'}
                />
                <FieldArray 
                        label={'Номер телефона'} 
                        name="client_contact_phones" 
                        component={addMore}
                        nameField={'phone_number'}
                />
               {/** <Field  name={`email`}
                        component={renderInput}
                        label={'Email'}
                        placeholder={'Email'}
                        validate={[required]}
                        type={'default'}
                        shadow={true}
                        /> *
                <Field  name={`phone_number`}
                        component={renderInput}
                        label={'Номер телефона'}
                        placeholder={'Номер телефона'}
                        validate={[required]}
                        type={'default'}
                        shadow={true}
                        />
                */}
                <Field  name={`nif`}
                        component={renderInput}
                        label={'Идентификационный номер'}
                        placeholder={'Идентификационный номер'}
                        type={'default'}
                        shadow={true}
                        />
                <Field  name={`profession`}
                        component={renderInput}
                        label={'Профессия'}
                        placeholder={'Профессия'}
                        type={'default'}
                        shadow={true}
                        />
                <Field  name={`user_id`}
                        component={Autocomplete}
                        label={'Ответственный'}
                        placeholder={'Ответственный'}
                        shadow={true}
                        data={props.users}
                        search={props.search}
                        setFocus={(type) => props.setFocus(type)}
                        type={'users'}
                        activeInput={props.activeInput}
                        index={999999}
                        statusDisabled={true}
                        defaultValue={''}
                />
                <Field  name={`referrer_note`}
                        component={renderInput}
                        label={'Заметка реферера'}
                        placeholder={'Заметка реферера'}
                        type={'default'}
                        shadow={true}
                        index={1}
                        />
                <Field  name={`note`}
                        component={renderInput}
                        label={'Заметка'}
                        placeholder={'Заметка'}
                        type={'default'}
                        shadow={true}
                        />
                <CustomButton action={handleSubmit(save)} title={I18n.t('save')}/>
            </View>
    )
}

const styles = StyleSheet.create({
    dataContainer: {
        flex: 1,
        paddingHorizontal: 16,
        paddingTop: 10,
        paddingBottom: 5
    },
    label: {
        paddingHorizontal: 16,
        //marginTop: 15,
        fontWeight: '500',
        fontSize: 14,
        color: '#4D5860',
        paddingBottom: 8,
    },
    addText: {
        fontSize: 18,
        color: '#42bec8',
        paddingVertical: 8,
        fontWeight: '600',
        paddingRight: 8,
    },
    addIcon: {
        width: 16,
        height: 16
    },
    addBt: {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center'
    },
    deleteIcon: {
        height: 24,
        resizeMode: 'contain'  
    },
    delete: {
            height: 28,
            width: 28,
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: -15,
            marginLeft: 8,
    }
});

export default reduxForm({
    form: 'EditClientForm',
    enableReinitialize: true
  })(EditClientForm);

