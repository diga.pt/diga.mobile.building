import React, {useEffect, useState} from 'react';

import LinearGradient from 'react-native-linear-gradient';

import {View, Text, StyleSheet, SafeAreaView, ScrollView,
        Dimensions, Image, TouchableOpacity, TextInput, FlatList, Linking,
        ActivityIndicator} from 'react-native';

import Header from '../../../components/Header';
import Preloader from '../../../components/Preloader';
import I18n from '../../../i18n/i18n';
import Input from '../../../components/Input'
import Modal from 'react-native-modal';
import CustomButton from '../../../components/CustomButton';
import {CrmAPI} from '../../../api/crm';
import Toast from 'react-native-toast-message';
import Clipboard from '@react-native-community/clipboard';
import HTML, {IGNORED_TAGS} from "react-native-render-html";

let deviceWidth = Dimensions.get('window').width;

const ModaldData = (props) => {
    return(
    <Modal isVisible={props.isVisible}>
            <SafeAreaView style={{flex: 1}}>
                {props.modaldInfo &&  <View style={styles.modalData}>
                            <View style={styles.modalHeader}>
                                <Text style={styles.modalTitle}>{props.modaldInfo.title}</Text>
                                <View>
                                    <TouchableOpacity onPress={() => props.close()} style={styles.close}>
                                        <Image style={styles.closeIcon} source={require('../../../source/img/close.png')}/>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{flex: 1, paddingVertical: 16}}>
                            {props.modaldInfo.type === 'events' && <FlatList
                                data={props.data.sort((a, b) => new Date(`${b.time} ${b.start}`) - new Date(`${a.time} ${a.start}`))}
                                keyExtractor={(item, index) => String(index)}
                                style={{flex: 1}}
                                renderItem={(data) => {
                                    let item = data.item;
                                    let title = props.eventTypes.find(it => it.id === item.event_type_id).title;
                                    return(
                                        
                                        <TouchableOpacity onPress={() => {
                                            props.close();
                                            props.navigation.navigate('Task', {id: item.id});
                                        }} style={styles.taskContainer}>
                                        <View style={styles.item}>
                                        <View style={styles.infoContainer}>
                                            <Text style={[styles.time]}>{title}</Text>
                                            <Text style={styles.time}>{item.time} {item.start}</Text>
                                        </View>
                                        {item.description ? <View style={{flex: 1}}>
                                        <Text style={styles.description}>{item.description}</Text>
                                        </View> : null}
                                        <View>
                                        </View>
                                        </View>
                                        </TouchableOpacity>
                                    )
                                }}
                            />}
                        {props.modaldInfo.type === 'services' && <FlatList
                                data={props.data.sort((a, b) => new Date(b.created_at) - new Date(a.created_at))}
                                keyExtractor={(item, index) => String(index)}
                                style={{flex: 1}}
                                renderItem={(data) => {
                                    let item = data.item;
                                    return(
                                        <View /*onPress={() => {props.close(); 
                                            props.navigation.navigate('Request', {item})}}*/ style={styles.taskContainer}>
                                            <View style={styles.item}>
                                            <View style={{flex: 1}}>
                                                <Text style={styles.description}>{item.estimate_number} {item.name}</Text>
                                            </View>
                     
                                        <View style={[styles.infoContainer, {marginTop: 3, margin: 0}]}>
                                                <Text style={styles.time}>{item.created_at}</Text>
                                        </View>
                                        <View style={styles.infoContainer}>
                                                <Text style={[styles.time]}>Адрес</Text>
                                                <Text style={styles.time}>{item.address ? item.address : ''}</Text>
                                        </View>
                                        <View style={styles.infoContainer}>
                                                <Text style={[styles.time]}>Сумма</Text>
                                                <Text style={styles.time}>{item.estimate_summ ? item.estimate_summ : 0}</Text>
                                        </View>
                                           {item.master_estimate_id &&  <View style={[styles.btContainer, {paddingHorizontal: 0, paddingVertical: 0}]}>
                                            <TouchableOpacity style={styles.btModal} onPress={() => props.openLink(item.master_estimate_id)}>
                                                <Text style={styles.btModalText}>Основная смета</Text>
                                            </TouchableOpacity>
                                            </View>}
                                            <View style={[styles.btContainer, {paddingVertical: 0, paddingHorizontal: 0}]}>
                                            <TouchableOpacity onPress={() => {
                                                props.close();
                                                props.navigation.navigate('Invoice', {client_contact_id: item.client_contact_id, 
                                                    estimate_id: item.master_estimate_id, service_id: item.id});
                                                }} style={styles.btModal}>
                                                <Text style={styles.btModalText}>Все фактуры</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => {
                                                props.close();
                                                props.navigation.navigate('AddInvoice', {client_contact_id: item.client_contact_id, service_id: item.id});
                                                }}   
                                                style={styles.btModal}>
                                                <Text style={styles.btModalText}>Добавить фактуру</Text>
                                            </TouchableOpacity>
                                            </View>
                                         <View>
                                        </View>
                                        </View>
                                        </View>
                                    )
                                }}
                            />}

                        {props.modaldInfo.type === 'client_history' && <FlatList
                                data={props.data.sort((a, b) => new Date(b.created_at) - new Date(a.created_at))}
                                keyExtractor={(item, index) => String(index)}
                                style={{flex: 1}}
                                renderItem={(data) => {
                                    let item = data.item;
                                    return(
                                        <View style={styles.taskContainer}>
                            <View style={styles.item}>
                            <View style={{flex: 1}}>
                            <Text style={styles.description}>{props.getText(item)}</Text>
                            </View>
                            <View style={[styles.infoContainer, {marginTop: 3, margin: 0}]}>
                                <Text style={styles.time}>{item.created_at}</Text>
                            </View>
                            <View>
                            </View>
                        </View>
                        </View> 
                                    )
                                }}
                            />}

                        {props.modaldInfo.type === 'webview' &&       <ScrollView>
                                    <HTML ignoredStyles={[ 'font-family']} source={{ html: props.data }} contentWidth={deviceWidth} />
                            </ScrollView>}
</View>

                </View>}
            </SafeAreaView>
    </Modal>
    )
}

    const ModaldHistory = (props) => {
                return(
                    <Modal isVisible={props.isVisible}>
                    <SafeAreaView style={{ flex: 1 }}>
                      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <View style={[styles.modalContainer]}>
                            <Text style={styles.modalTitle}>Добавить запись</Text>
                            <TextInput style={styles.historyInput} 
                                    placeholder={'Введите текст'}
                                    placeholderTextColor={'#7E8086'}
                                    numberOfLines={5}
                                    multiline = {true}
                                    onChangeText={e => props.setMessage(e)}
                                />
                            <View style={{paddingTop: 16}}/>
                            <CustomButton action={() => props.action()} title={'OK'}/>
                            <View style={styles.cancleContainer}>
                        <TouchableOpacity style={styles.cancleBt}
                                        onPress={() => props.close()}>
                                <Text style={styles.cancleText}>{I18n.t('cancle')}</Text>
                        </TouchableOpacity>
                  </View>
                        </View>
                      </View>
                    </SafeAreaView>
                    </Modal>
                )
            }

const Client = (props) => {

    let [load, setLoad] = useState(true);
    let [modaldHistory, setModaldHistory] = useState(false);
    let [client, setClient] = useState({});
    let [eventTypes, setEventTypes] = useState(null);
    let [message, setMessage] = useState('');
    let [modaldData, setModaldData] = useState(false);
    let [modaldInfo, setModaldInfo] = useState(null);
    let [history, setHistory] = useState([]);
    let [totalHistory, setTotalHistory] = useState([]);
    let [loadHistory, setLoadHistory] = useState(false);
    let [offset, setOffsete] = useState(0);
    let [sites, seSites] = useState([]);
    let [webviewData, setWebviewData] = useState('');

    console.log(sites);

    useEffect(() => {

        CrmAPI.getEventTypes()
        .then(res => {
            console.log(res, 'getEventTypes');
            setEventTypes(res);
        });
 
        CrmAPI.getSites()
            .then(res => {
                seSites(res);
                console.log(res, 'getSites');
            });
            
        const unsubscribe = props.navigation.addListener('focus', () => {
            setLoad(true);
            getContact();
        });
        return unsubscribe;
    }, [props.navigation]);



    const getContact = () => {
        CrmAPI.getContact({id: props.route.params.id})
        .then(res => {
            setClient(res);
            setLoad(false);
            getHistory(res.id);
            console.log(res, 'res');
        })
        .catch(err => {
            setLoad(false);
            console.log(err, 'err');
        });
    }


    const getHistory = (id) => {
        setLoadHistory(true);
        CrmAPI.getHistory({id, data: {offset: 0, limit: 10}})
        .then(res => {
            setHistory(res.rows);
            setTotalHistory(res.total);
            setLoadHistory(false);
        })
        .catch(err => {
            setLoadHistory(false);
        });
    };

    const loadMoreHistory = () => {
        if(history.length < totalHistory){
            setLoadHistory(true);
            CrmAPI.getHistory({id: client.id, data: {offset: offset+10, limit: 10}})
            .then(res => {
                setLoadHistory(false);
                setHistory([...history, ...res.rows]);
                setOffsete(offset+10);
                console.log(res, 'res');
            })
            .catch(err => {
                setLoadHistory(false);
            });
        };
    };

    const getText = (data) => {
        let type = data.type_id;
        let id = data.id;

        switch(type){
            case 23: {
                return `${data.client_contact.name} Изменил значения на странице платежей субподрядчиков 
                `
            }
            case 1: {
                return data.message
            }
            case 5: {
                return data.message
            }
            case 19: {
                return data.message
            }
            case 20: {
                return data.message
            }
            case 6: {
                return data.message
            }
            case 24: {
                return `${data.client_contact.name} Изменил значения на странице платежей клиентов`
            }
            case 3: {
                return <TouchableOpacity onPress={() => getModalData(id)}><Text style={styles.show}>Показать</Text></TouchableOpacity>
            }
            case 4: {
                return <TouchableOpacity onPress={() => getModalData(id)}><Text style={styles.show}>Показать</Text></TouchableOpacity>
            }
            case 22: {
                return 'Звонок'
            }
            case 21: {
                return 'Звонок'
            }
            case 2: {
                return `${data.client_contact.name} изменил статус услуги №${data.service.estimate_number} : ${data.service_state_id}. ${ data.message ? `Комментарий  ${data.message}` : ''}`
            }
            case 18: {
                return `${data.client_contact.name} выполнил задачу ${data.event.event_type.title} назначенную на  ${data.event.start} 
                            ${data.service_attachment ? `${data.service_attachment.name}` : ''}`
            }
        };
    }

    const get_type = (data) => {
        switch (data.type_id) {
        case 1:
            return "Комментарий";
        case 2: 
            return "Система";
        case 3:
            return "Исходящая почта";
        case 4:
            return "Входящая почта";
        case 5:
            return sites.find(it => it.id === data.site_id).domain; //сайт или форма,  с которого пришел клиент
        case 6:
            return "Интеграция";
        case 18:
            return "Система";
        case 19:
            return "Получено с Facebook";
        case 20:
            return "Отправлено в Facebook";
        case 23:
            return "Субподрядчики";
        case 24:
            return "Финансы";
        }
    }

    const copyEmail = (email) => {
        Toast.show({
            type: 'success',
            position: 'bottom',
            text1: `Email ${email} скопирован`,
            visibilityTime: 1000,
            autoHide: true,
            bottomOffset: 45
          });
          Clipboard.setString(email);
    }

    const addMessage = () => {
        if(message.length > 0){
            setModaldHistory(false);
            setLoad(true);
            CrmAPI.addHistory({id: client.id, message})
           .then(res => {
            getContact();
           Toast.show({
               type: 'success',
               position: 'bottom',
               text1: I18n.t('attention'),
               text2: I18n.t('successData'),
               visibilityTime: 4000,
               autoHide: true,
               bottomOffset: 45
             });
       })
       .catch(err => {
           console.log(err, 'err');
           setLoad(false);
           Toast.show({
               type: 'error',
               position: 'bottom',
               text1: I18n.t('attention'),
               text2: I18n.t('error'),
               visibilityTime: 4000,
               autoHide: true,
               bottomOffset: 45
             });
        })
        };
    }

   const renderNode = (node, index, siblings, parent, defaultRenderer) => {
        if (node.name === 'img') {
                const data = node.attribs;
                console.log(data, 'data');
            return (
                    <Image
                          key={index}
                          source={{uri: data.src}}
                          //resizeMode="contain"
                          style={{height: 50,
                                      width: 70, 
                                     resizeMode: 'contain',
                                    }}
                        />
                    );
    
            }
        }


        const getModalData = (id) => {
            CrmAPI.getMessage({id})
                .then(res => {
                    setModaldInfo({
                        type: 'webview',
                        title: ''
                    });
                    setModaldData(true);
                    if(res.errcode === 0){
                        setWebviewData(res.message);
                    }
                    
                })
                .catch(err => {
                    console.log(err);
                })
        }

        const openLink = async (id) => {
            setLoad(true);
            CrmAPI.getLinkToPDF({id})
            .then(async res => {
                setLoad(false);
                await Linking.openURL(res.link);
            })
            .catch(err => {
                setLoad(false);
            });
        };

        console.log(client, 'client.service');

    return(
        <>
        <ModaldData 
                isVisible={modaldData} 
                close={() => setModaldData(false)}
                modaldInfo={modaldInfo}
                data={modaldInfo ?
                        modaldInfo.type === 'events' ? client.events 
                        : modaldInfo.type === 'services' ? client.services
                        : modaldInfo.type === 'client_history' ? client.client_history
                        :  modaldInfo.type === 'webview' ? webviewData
                         : [] : []}
                eventTypes={eventTypes}
                getText={getText}
                navigation={props.navigation}
                openLink={openLink}
                clientId={client.id}
            />
        <ModaldHistory isVisible={modaldHistory}
                        action={() => addMessage()}
                        close={() => setModaldHistory(false)}
                        setMessage={setMessage}
                        />
        {load  ? <Preloader/> : <>
        <SafeAreaView style={{flex: 0, backgroundColor: '#FEFEFE'}}/>
        <LinearGradient colors={['#FEFEFE', '#F9F9F9']} style={styles.container}>
            <SafeAreaView style={styles.contentContainer}>
            <Header leftIcon='back' title={client.name}
                    leftAction={() => props.navigation.goBack()}
                    sizeIcon={24}
                     rightIcon={'edit'}
                    rightAction={() => props.navigation.navigate('EditClient', {client})}
                />
            <View style={{flex: 1, alignItems: 'center'}}>
            <ScrollView style={{flex: 1, width: '100%'}}
                    keyboardShouldPersistTaps='always' 
                    showsVerticalScrollIndicator={false}
                    nestedScrollEnabled = {true}>
            <View style={{flex: 1, paddingBottom: 20}}>
            <View style={styles.dataContainer}> 
                <Input value={client.name}
                       label={'Полное имя'}
                       editable={false}
                       />
                {client.client_contact_phones.map((item, index) => {
                    return(
                        <View style={{position: 'relative'}}>
                        <TouchableOpacity onPress={()=> {
                            Linking.openURL(`tel:${item.phone_number}`)
                        }} style={styles.inputBt}/> 
                        <Input value={item.phone_number}
                            label={'Телефон'}
                            editable={false}
                            key={index}
                            hideLable={index !== 0 ? true : false}
                        />
                        </View>
                    )
                })}
                {client.client_contact_emails.map((item, index) => {
                    return(
                        <View style={{position: 'relative'}}>
                        <TouchableOpacity onPress={()=> {
                            copyEmail(item.email)
                        }} style={styles.inputBt}/> 
                        <Input value={item.email}
                            label={'Email'}
                            editable={false}
                            key={index}
                            hideLable={index !== 0 ? true : false}
                            //action={copyEmail(item.email)}
                        />
                        </View>
                    )
                })}
                <Input value={client.client_referrer ? client.client_referrer.title : 'Источник'}
                        label={'Источник'}
                        editable={false}
                        />
            </View>
            <View style={styles.dataContainer}>
                <View style={styles.titleContainer}>
                    <TouchableOpacity onPress={() => {
                        setModaldInfo({
                            type: 'events',
                            title: 'Задачи'
                        });
                        setModaldData(true);
                        }
                    }>
                        <Text style={styles.title}>Показать задачи</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => props.navigation.navigate('AddTask', {id: client.id, name: client.name})}>
                        <Image style={styles.icon} source={require('../../../source/img/plusActive.png')}/>
                    </TouchableOpacity>
                </View>
               
            </View>
            <View style={styles.dataContainer}>
                <View style={styles.titleContainer}>
                <TouchableOpacity onPress={() => {
                        setModaldInfo({
                            type: 'services',
                            title: 'Запросы клиента'
                        });
                        setModaldData(true);
                        }
                    }>
                        <Text style={styles.title}>Показать запросы клиента</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => props.navigation.navigate('NewService', {id: client.id})}>
                        <Image style={styles.icon} source={require('../../../source/img/plusActive.png')}/>
                    </TouchableOpacity>
                </View>
                
            </View>
            <View style={styles.dataContainer}>
                <View style={styles.titleContainer}>
                        <Text style={styles.title}>История</Text>
                    <TouchableOpacity onPress={() => setModaldHistory(true)}>
                        <Image style={styles.icon} source={require('../../../source/img/plusActive.png')}/>
                    </TouchableOpacity>
                </View>
                <View style={{marginTop: 15}}>
                        <FlatList
                            nestedScrollEnabled
                                data={history ? history.sort((a, b) => new Date(b.created_at) - new Date(a.created_at)) : []}
                                keyExtractor={(item, index) => String(index)}
                                style={{flex: 1, maxHeight: 400}}
                                onEndReachedThreshold={0}
                                onEndReached={data => {
                                    if(!loadHistory){
                                        loadMoreHistory();
                                    }
                                }}
                                renderItem={(data) => {
                                    let item = data.item;
                                    return(
                                        <View style={styles.taskContainer}>
                            <View style={styles.item}>
                            <View style={{flex: 1}}>
                            {item.user && <Text style={[styles.time, {paddingBottom: 5, fontSize: 14}]}>{item.user.name}</Text>}
                            <Text style={styles.description}>{getText(item)}</Text>
                            </View>
                            <View style={[styles.infoContainer, {marginTop: 3, margin: 0}]}>
                                <Text style={styles.time}>{item.created_at}</Text>
                                <Text style={styles.time}>{get_type({type_id: item.type_id, site_id: item.site_id})}</Text>
                            </View>
                            <View>
                            </View>
                        </View>
                        </View> 
                                    )
                                }}
                            />
                        {loadHistory && <ActivityIndicator style={{marginTop: 15}} size="small" color="#42bec8"/>}
                </View>
            
            </View>
            </View>
            </ScrollView>
            </View>
            </SafeAreaView>
        </LinearGradient>
            </>
            }
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        width: deviceWidth,
        flex: 1,
        flexDirection:'column', 
        alignItems:'center', 
        justifyContent:'center'
    },
    contentContainer: {
        flex: 1,
        width: '100%',
        justifyContent: 'center'
    },
    warning: {
        fontSize: 16, 
        fontWeight: '500',
        color: '#7E8086',
        textAlign: 'center',
        paddingVertical: 16
    },
    contactContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        width: deviceWidth-32,
        flex: 1,
        paddingHorizontal: 16,
        backgroundColor: '#FFF',
        borderRadius: 12,
        paddingVertical: 12,
        marginTop: 2,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        flexWrap: 'wrap',
        marginBottom: 8,
    },
    name: {
        fontSize: 17, 
        fontWeight: '500',
        color: '#7E8086',
    },
    id: {
        fontSize: 14, 
        fontWeight: '500',
        color: '#7E8086',
    },
    infoContainer: {
        flexDirection: 'column', 
        flex: 1,
        width: '100%',
        marginLeft: 15
    },
    titleContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 10
    },
    title: {
        fontSize: 18, 
        fontWeight: '600',
        color: '#42bec8',
    },
    dataContainer: {
        flex: 1,
        paddingHorizontal: 16,
        paddingTop: 10,
        paddingBottom: 5
    },
    active: {
        borderWidth: 2,
        borderColor: '#42bec8'
    },
    contact: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
    },
    contactIcon: {
        width: 19,
        height: 19,
        resizeMode: 'contain'
    },
    contactText: {
        fontSize: 14, 
        fontWeight: '500',
        color: '#7E8086',
        paddingLeft: 4
    },
    inputContainer: {
        width: '100%',
        height: 45,
        backgroundColor: '#dedede',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        position: 'relative',
        fontWeight: '500',
        backgroundColor: '#fff',
        borderRadius: 50,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        marginBottom: 15
    },
    input: {
        flex: 1,
        height: 45,
        backgroundColor: '#fff',
        borderRadius: 50,
        paddingHorizontal: 16,
        fontSize: 14
    },
    label: {
        fontWeight: '400',
        fontSize: 14,
        color: '#4D5860',
    },
    dataText: {
        fontWeight: '400',
        fontSize: 18,
    },
    bigDataText: {
        paddingTop: 2,
        fontSize: 18
    },
    data: {
        //flexDirection: 'row',
        //alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: 10,
    },
    bigData: {
        paddingTop: 10,
        paddingHorizontal: 16
    },
    userPhoto: {
        width: 50,
        height: 50,
        borderRadius: 50,
        resizeMode: 'cover',
        borderWidth: 2
    },
    status: {
        width: 13,
        height: 13,
        borderRadius: 50,
        backgroundColor: "#42bec8"
    },
    profileContainer: {
        flex: 1,
        flexDirection: 'column',
        paddingHorizontal: 16,
        paddingVertical: 15,
        alignItems: 'center'
    },
    profileIcon: {
        width: 95,
        height: 95,
        borderRadius: 50,
    },
    profileName: {
        fontSize: 20,
        paddingTop: 10,
        paddingBottom: 5,
        fontWeight: '500'
    },
    mainInfo: {
        padding: 3,
        alignItems: 'center',
        flexDirection: 'row'
    },
    mainInfoText: {
        fontSize: 18,
    },
    itemContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 16,
        paddingVertical: 10,     
        justifyContent: 'space-between',
        backgroundColor: '#FFF',
        borderRadius: 12,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        width: deviceWidth-72,
        marginRight: 10
    },
    timeContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        flex: 1,
        paddingVertical: 5,
    },
    timeTitle: {
        fontSize: 16,
        fontWeight: '400',
        color: '#4d4d4f'
    },
    time: {
        fontSize: 16,
        fontWeight: '500',
    },
    btText: {
        fontWeight: '500',
        fontSize: 18,
        color: '#4D5860',
    },
    icon: {
        width: 19,
        height: 19,
        resizeMode: 'contain',
        opacity: .7,
        transform: [{ rotate: '180deg'}]
    },
    btContainer: {
        paddingVertical: 9,
        paddingHorizontal: 16,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    time: {
        color: '#7E8086',
    },
    description: {
        flexDirection:'row', 
        flex: 1, 
        flexWrap: 'wrap',
        paddingRight: 8
    },
    taskContainer: {
        backgroundColor: '#fff',
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        borderRadius: 12,
        paddingVertical: 12,
        paddingHorizontal: 16,
        marginBottom: 8,
        flex: 1,
        flexDirection: 'row',
    },
    itemContainer: {
        marginTop: 15,
        maxHeight: 350,
        width: '100%'
    },
    infoContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 3,
    },
    item: {
        width: '100%'
    },
    modalContainer: {
        width: deviceWidth-42,
        padding: 16,
        borderRadius: 8,
        backgroundColor: '#fff',
        maxWidth: 450
    },
    cancleContainer: {
        width: '100%',
        alignItems: "center",
        marginTop: 10
    },
    cancleBt: {
        padding: 10
    },
    cancleText: {
      fontSize: 16,
      textTransform: 'uppercase',
      fontWeight: '600',
      color: '#7E8086'
    },
    historyInput: {
      padding: 8,
      marginBottom: 5,
      borderBottomWidth: 1,
      borderBottomColor: '#7E8086',
      fontSize: 16,
      marginHorizontal: 16,
      marginTop: 20
    },
    modalTitle: {
        color: '#7E8086',
        textAlign: 'center',
        fontSize: 16,
        paddingTop: 15,
        fontWeight: '500'
    },
    inputBt: {
        position: 'absolute',
        height: 50,
        width: '100%',
        bottom: 0,
        zIndex: 1,
        borderRadius: 50
    },
    modalData: {
        backgroundColor: '#fff',
        flex: 1,
        borderRadius: 10,
        paddingVertical: 16,
        paddingHorizontal: 16
    },
    modalHeader: {
        flexDirection: 'row',
    },
    close: {
        position: 'absolute',
        top: 0,
        right: 0,
        padding: 2,
    },
    closeIcon: {
        width: 20, 
        height: 20
    },
    modalTitle: {
        color: '#7E8086',
        fontSize: 18,
        width: '100%',
        textAlign: 'center',
        fontWeight: '500'
    },
    show: {
        fontWeight: '500',
        fontSize: 16,
        color: '#42bec8'
    },
    btModal: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 8
    },
    btModalText: {
        fontSize: 14,
        color: '#42bec8'
    }
});

export default Client;