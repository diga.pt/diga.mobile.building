import React, {useEffect, useState} from 'react';

import LinearGradient from 'react-native-linear-gradient';

import {View, Text, StyleSheet, SafeAreaView, ScrollView,
        Dimensions, KeyboardAvoidingView} from 'react-native';

import Header from '../../../components/Header';
import Preloader from '../../../components/Preloader';
import I18n from '../../../i18n/i18n';
import Input from '../../../components/Input'
let deviceWidth = Dimensions.get('window').width;
import EditClientForm from './EditClientForm';
import Toast from 'react-native-toast-message';
import {CrmAPI} from  '../../../api/crm';

const EditClient = (props) => {

    let [load, setLoad] = useState(false);
    let [activeInput, setFocus] = useState(null);
    let [users, setUsers] = useState([]);

    const search = (data, type) => {
        switch(type){
            case 'users': {
                CrmAPI.getUsers({
                    fields: 'id,name',
                    offset: 0,
                    limit: 20,
                    search: data
                })
                    .then(res => {
                        let data = res.rows.map(it => {
                            return {
                                label: it.name,
                                id: it.id
                            }
                        });
                        setUsers(data);
                    })
                    .catch(err => {
                        console.log(err, 'err');
                    })
                break;
            }
        }
    }

    const save = (data) => {
        setLoad(true);
        CrmAPI.updateСontact({id: data.id, data})
            .then(res => {
                if(res.errcode === 0){
                    Toast.show({
                        type: 'success',
                        position: 'bottom',
                        text1: I18n.t('attention'),
                        text2: I18n.t('successData'),
                        visibilityTime: 4000,
                        autoHide: true,
                        bottomOffset: 45
                      });
                    props.navigation.goBack();
                } else {
                    Toast.show({
                        type: 'error',
                        position: 'bottom',
                        text1: I18n.t('attention'),
                        text2: I18n.t('error'),
                        visibilityTime: 4000,
                        autoHide: true,
                        bottomOffset: 45
                      });
                }
                setLoad(false);
            })
            .catch(err => {
                Toast.show({
                    type: 'error',
                    position: 'bottom',
                    text1: I18n.t('attention'),
                    text2: I18n.t('error'),
                    visibilityTime: 4000,
                    autoHide: true,
                    bottomOffset: 45
                  });
                setLoad(false);
            })
    }


    return(
        <>
        {load && <Preloader/>}
        <KeyboardAvoidingView style={{flex: 1}}
                        behavior={Platform.OS == "ios" ? "padding" : null}>
        <SafeAreaView style={{flex: 0, backgroundColor: '#FEFEFE'}}/>
        <LinearGradient colors={['#FEFEFE', '#F9F9F9']} style={styles.container}>
            <SafeAreaView style={styles.contentContainer}>
            <Header leftIcon='back' title={'Редактировать'}
                    leftAction={() => props.navigation.goBack()}
                    sizeIcon={24}
                />
            <View style={{flex: 1, alignItems: 'center'}}>
            <ScrollView style={{flex: 1, width: '100%'}} showsVerticalScrollIndicator={false}>
            <View style={{flex: 1, paddingBottom: 20}}>
                <EditClientForm 
                    initialValues={props.route.params.client}
                    setFocus={(type) => {
                        setFocus(type);
                    }}
                    activeInput={activeInput}
                    users={users}
                    search={(data, type) => search(data, type)}
                    save={save}
                />
            </View>
            </ScrollView>
            </View>
            </SafeAreaView>
        </LinearGradient>
        </KeyboardAvoidingView>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        width: deviceWidth,
        flex: 1,
        flexDirection:'column', 
        alignItems:'center', 
        justifyContent:'center'
    },
    contentContainer: {
        flex: 1,
        width: '100%',
        justifyContent: 'center'
    },
    warning: {
        fontSize: 16, 
        fontWeight: '500',
        color: '#7E8086',
        textAlign: 'center',
        paddingVertical: 16
    },
    contactContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        width: deviceWidth-32,
        flex: 1,
        paddingHorizontal: 16,
        backgroundColor: '#FFF',
        borderRadius: 12,
        paddingVertical: 12,
        marginTop: 2,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        flexWrap: 'wrap',
        marginBottom: 8,
    },
    name: {
        fontSize: 17, 
        fontWeight: '500',
        color: '#7E8086',
    },
    id: {
        fontSize: 14, 
        fontWeight: '500',
        color: '#7E8086',
    },
    infoContainer: {
        flexDirection: 'column', 
        flex: 1,
        width: '100%',
        marginLeft: 15
    },
    titleContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 10
    },
    title: {
        fontSize: 18, 
        fontWeight: '600',
        color: '#42bec8',
    },
    dataContainer: {
        flex: 1,
        paddingHorizontal: 16,
        paddingTop: 10,
        paddingBottom: 5
    },
    active: {
        borderWidth: 2,
        borderColor: '#42bec8'
    },
    contact: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
    },
    contactIcon: {
        width: 19,
        height: 19,
        resizeMode: 'contain'
    },
    contactText: {
        fontSize: 14, 
        fontWeight: '500',
        color: '#7E8086',
        paddingLeft: 4
    },
    inputContainer: {
        width: '100%',
        height: 45,
        backgroundColor: '#dedede',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        position: 'relative',
        fontWeight: '500',
        backgroundColor: '#fff',
        borderRadius: 50,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        marginBottom: 15
    },
    input: {
        flex: 1,
        height: 45,
        backgroundColor: '#fff',
        borderRadius: 50,
        paddingHorizontal: 16,
        fontSize: 14
    },
    label: {
        fontWeight: '400',
        fontSize: 14,
        color: '#4D5860',
    },
    dataText: {
        fontWeight: '400',
        fontSize: 18,
    },
    bigDataText: {
        paddingTop: 2,
        fontSize: 18
    },
    data: {
        //flexDirection: 'row',
        //alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: 10,
    },
    bigData: {
        paddingTop: 10,
        paddingHorizontal: 16
    },
    userPhoto: {
        width: 50,
        height: 50,
        borderRadius: 50,
        resizeMode: 'cover',
        borderWidth: 2
    },
    status: {
        width: 13,
        height: 13,
        borderRadius: 50,
        backgroundColor: "#42bec8"
    },
    profileContainer: {
        flex: 1,
        flexDirection: 'column',
        paddingHorizontal: 16,
        paddingVertical: 15,
        alignItems: 'center'
    },
    profileIcon: {
        width: 95,
        height: 95,
        borderRadius: 50,
    },
    profileName: {
        fontSize: 20,
        paddingTop: 10,
        paddingBottom: 5,
        fontWeight: '500'
    },
    mainInfo: {
        padding: 3,
        alignItems: 'center',
        flexDirection: 'row'
    },
    mainInfoText: {
        fontSize: 18,
    },
    itemContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 16,
        paddingVertical: 10,     
        justifyContent: 'space-between',
        backgroundColor: '#FFF',
        borderRadius: 12,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        width: deviceWidth-72,
        marginRight: 10
    },
    timeContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        flex: 1,
        paddingVertical: 5,
    },
    timeTitle: {
        fontSize: 16,
        fontWeight: '400',
        color: '#4d4d4f'
    },
    time: {
        fontSize: 16,
        fontWeight: '500',
    },
    btText: {
        fontWeight: '500',
        fontSize: 18,
        color: '#4D5860',
    },
    icon: {
        width: 19,
        height: 19,
        resizeMode: 'contain',
        opacity: .7,
        transform: [{ rotate: '180deg'}]
    },
    btContainer: {
        paddingVertical: 9,
        paddingHorizontal: 16,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    time: {
        color: '#7E8086',
    },
    description: {
        flexDirection:'row', 
        flex: 1, 
        flexWrap: 'wrap',
        paddingRight: 8
    },
    taskContainer: {
        backgroundColor: '#fff',
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        borderRadius: 12,
        paddingVertical: 12,
        paddingHorizontal: 16,
        marginTop: 8,
        flex: 1,
        flexDirection: 'row',
    },
    itemContainer: {
        marginTop: 15,
        maxHeight: 350,
        width: '100%'
    },
    infoContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 3,
    },
    item: {
        width: '100%'
    },
    modalContainer: {
        width: deviceWidth-42,
        padding: 16,
        borderRadius: 8,
        backgroundColor: '#fff',
        maxWidth: 450
    },
    cancleContainer: {
        width: '100%',
        alignItems: "center",
        marginTop: 10
    },
    cancleBt: {
        padding: 10
    },
    cancleText: {
      fontSize: 16,
      textTransform: 'uppercase',
      fontWeight: '600',
      color: '#7E8086'
    },
    historyInput: {
      padding: 8,
      marginBottom: 5,
      borderBottomWidth: 1,
      borderBottomColor: '#7E8086',
      fontSize: 16,
      marginHorizontal: 16  
    }
});

export default EditClient;