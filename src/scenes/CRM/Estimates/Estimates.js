import React, {useEffect, useState} from 'react';

import LinearGradient from 'react-native-linear-gradient';

import {View, Text, StyleSheet, SafeAreaView, ScrollView,
        Dimensions, Image, TouchableOpacity, TextInput, FlatList,
        ActivityIndicator, Linking} from 'react-native';
import Clipboard from '@react-native-community/clipboard';

import Header from '../../../components/Header';

let deviceWidth = Dimensions.get('window').width;

import { useDispatch, useSelector } from "react-redux";

import Preloader from '../../../components/Preloader';

import I18n from '../../../i18n/i18n';

import {SettingsAPI} from '../../../api/settings';
import {CrmAPI} from '../../../api/crm';

import { email } from '../../../components/form/validator';
import Toast from 'react-native-toast-message';
import { useIsFocused } from "@react-navigation/native";


const Estimates = (props) => {

    let [load, setLoad] = useState(false);
    let [loadMore, setLoadMore] = useState(false);
    let [estimates, setEstimates] = useState([]);
    let [offset, setOffsete] = useState(0);
    let [total, setTotal] = useState(0);
    let [searchText, setSearchText] = useState('');



    const isFocused = useIsFocused();
    useEffect(() => {
        getEstimates('');
      }, [isFocused]);

    const getEstimates = (data) => {
        setLoad(true);
        SettingsAPI.estimates({offset: 0, limit: 10, responsible_user_id: 0, query: data})
        .then(res => {
            setLoad(false);
            setEstimates(res.rows);
            setTotal(res.total);
            console.log(res, 'res');
        })
        .catch(err => {
            setLoad(false);
            console.log(err, 'err');
        });
    };

    const loadMoreContacts = () => {
        if(estimates.length < total){
            setLoadMore(true);
            SettingsAPI.estimates({offset: offset+10, limit: 10, responsible_user_id: 0, query: searchText})
            .then(res => {
                setLoadMore(false);
                setEstimates([...estimates, ...res.rows]);
                setOffsete(offset+10);
                console.log(res, 'loadMoreContacts');
            })
            .catch(err => {
                setLoadMore(false);
                console.log(err, 'err');
            });
        };
    };
        
        const search = (data) => {
            getEstimates(data);
            setSearchText(data);
        };

    const openLink = async (id) => {
        setLoad(true);
        CrmAPI.getLinkToPDF({id})
        .then(async res => {
            setLoad(false);
            await Linking.openURL(res.link);
        })
        .catch(err => {
            setLoad(false);
        });
    };

    return(
        <>
        {load && <Preloader/>}
        <SafeAreaView style={{flex: 0, backgroundColor: '#FEFEFE'}}/>
        <LinearGradient colors={['#FEFEFE', '#F9F9F9']} style={styles.container}>
            <SafeAreaView style={styles.contentContainer}>
            <Header leftIcon='menu' title={'Сметы'}
                    leftAction={() => props.navigation.openDrawer()}
                    sizeIcon={24}
                />
            <View style={{flex: 1, alignItems: 'center'}}>
            <View style={[styles.inputWrapper, {paddingHorizontal: 16}]}>
                <View style={styles.inputContainer}>
                <TextInput
                        placeholder={I18n.t('search')}
                        style={styles.input}
                        placeholderTextColor={'#7E8086'}
                        onChangeText={(e) => search(e)}
                    />
                </View>
            </View>
            <View style={{flex: 1, width: '100%'}}>
            <View style={{flex: 1, paddingBottom: 20, alignItems: 'center'}}>
                <FlatList
                    data={estimates}
                    keyExtractor={(item, index) => String(index)}
                    onEndReachedThreshold={0}
                    onEndReached={data => {
                        if(!loadMore){
                            loadMoreContacts();
                        }
                    }}
                    renderItem={data => {
                        let item = data.item;
                        return(
                    <TouchableOpacity onPress={() => openLink(item.id)}  style={styles.contactContainer}>
                    <View style={styles.infoContainer}>
                        <Text style={styles.name}>Дата:</Text>
                        <Text style={styles.id}>{item.created_at}</Text>
                    </View>
                    <View style={styles.infoContainer}>
                        <Text style={styles.name}>Название:</Text>
                        <Text style={styles.text}>{item.service.name}</Text>
                    </View>
                    <View style={styles.infoContainer}>
                        <Text style={styles.name}>Клиент:</Text>
                        <TouchableOpacity onPress={() => props.navigation.navigate('Client', {id: item.service.client_contact.id})}>
                            <Text style={styles.text}>{item.service.client_contact.name}</Text>
                        </TouchableOpacity>
                    </View>
                    </TouchableOpacity>
                        )
                    }}
                />
                {loadMore && <ActivityIndicator style={{marginTop: 15}} size="small" color="#42bec8"/>}
            </View>
            </View>
            </View>
            </SafeAreaView>
        </LinearGradient>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        width: deviceWidth,
        flex: 1,
        flexDirection:'column', 
        alignItems:'center', 
        justifyContent:'center'
    },
    contentContainer: {
        flex: 1,
        width: '100%',
        justifyContent: 'center'
    },
    warning: {
        fontSize: 16, 
        fontWeight: '500',
        color: '#7E8086',
        textAlign: 'center',
        paddingVertical: 16
    },
    contactContainer: {
        alignItems: 'center',
        flexDirection: 'column',
        width: deviceWidth-32,
        flex: 1,
        //paddingHorizontal: 16,
        backgroundColor: '#FFF',
        borderRadius: 12,
        paddingVertical: 12,
        marginTop: 2,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        flexWrap: 'wrap',
        marginBottom: 8,
    },
    name: {
        fontSize: 17, 
        fontWeight: '500',
        color: '#7E8086',
    },
    id: {
        fontSize: 14, 
        fontWeight: '500',
        color: '#7E8086',
        flexShrink: 1,
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    infoContainer: {
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        //alignItems: 'center', 
        flex: 1,
        width: '100%',
        marginBottom: 8,
        position: 'relative',
        paddingHorizontal: 16,
    },
    title: {
        fontSize: 14, 
        fontWeight: '400',
        color: '#7E8086',
    },
    active: {
        borderWidth: 2,
        borderColor: '#42bec8'
    },
    contact: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
    },
    contactIcon: {
        width: 19,
        height: 19,
        resizeMode: 'contain'
    },
    contactText: {
        fontSize: 14, 
        fontWeight: '500',
        color: '#7E8086',
        paddingLeft: 4
    },
    inputContainer: {
        width: '100%',
        height: 45,
        backgroundColor: '#dedede',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        position: 'relative',
        fontWeight: '500',
        backgroundColor: '#fff',
        borderRadius: 50,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        marginBottom: 15
    },
    input: {
        flex: 1,
        height: 45,
        backgroundColor: '#fff',
        borderRadius: 50,
        paddingHorizontal: 16,
        fontSize: 14
    },
    label: {
        paddingHorizontal: 16,
        marginTop: 15,
        fontWeight: '500',
        fontSize: 14,
        color: '#4D5860',
    },
    list: {
        flexDirection: 'row',
        paddingLeft: 20,
        flexWrap: 'wrap',
        flexShrink: 1,
        
    },
    service: {
        flexShrink: 1,
    },
    text: {
        fontSize: 16,
        fontWeight: '500',
        flexShrink: 1,
        flexWrap: 'wrap',
        textAlign: 'right',
        paddingLeft: 8,
        fontSize: 14,
        color: '#4D5860',
    }
});

export default Estimates;