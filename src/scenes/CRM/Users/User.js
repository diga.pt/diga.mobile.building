import React, {useEffect, useState} from 'react';

import LinearGradient from 'react-native-linear-gradient';

import {View, Text, StyleSheet, SafeAreaView, ScrollView,
        Dimensions, Image, TouchableOpacity, TextInput, Linking} from 'react-native';

import Header from '../../../components/Header';

let deviceWidth = Dimensions.get('window').width;

import { useSelector } from "react-redux";
import Preloader from '../../../components/Preloader';
import I18n from '../../../i18n/i18n';
import {CrmAPI} from  '../../../api/crm';



const User = (props) => {
    const settings = useSelector(state => state.general.settings);

    let [load, setLoad] = useState(false);
    let [user, setUser] = useState([]);

    useEffect(() => {
        setLoad(true);
        CrmAPI.getUser({id: props.route.params.id})
            .then(res => {
                console.log(res, 'getUser');
                setUser(res);
                setLoad(false);
            })
            .catch(err => {
                console.log(err, 'err');
                setLoad(false);
            })
    }, [props.navigation])

    console.log(props, 'props.navigation');

    let getPhoto = (url_photo) => {
        let domain = 'diga.pt';
            if(settings.subdomain === 'besterp'){
                    domain = 'rkesa.pt';
                }
            let url = `https://${settings.subdomain}.${domain}${url_photo}`;
            return url;
    }

    const checkData = (data) => {
        return data ? data : 'Нет данных';
    }

    const openLink = async (url_file) => {
        let domain = 'diga.pt';
            if(settings.subdomain === 'besterp'){
                    domain = 'rkesa.pt';
                }
            let url = `https://${settings.subdomain}.${domain}${url_file}`;
            await Linking.openURL(url);
    }

    return(
<>
        {load && !user ? <Preloader/> : <>
        <SafeAreaView style={{flex: 0, backgroundColor: '#FEFEFE'}}/>
        <LinearGradient colors={['#FEFEFE', '#F9F9F9']} style={styles.container}>
            <SafeAreaView style={styles.contentContainer}>
            <Header leftIcon='back' title={I18n.t('user')}
                    leftAction={() => props.navigation.goBack()}
                    sizeIcon={24}
                />
            <View style={{flex: 1, alignItems: 'center'}}>
            <ScrollView style={{flex: 1, width: '100%'}} showsVerticalScrollIndicator={false}>
            <View style={{flex: 1, paddingBottom: 20}}>
            <View style={styles.profileContainer}>
                <Image source={{uri: getPhoto(user.photo)}}
                        style={styles.profileIcon}/>
                <Text style={styles.profileName}>{user.name}</Text>
                <TouchableOpacity style={styles.mainInfo}>
                    <Text style={styles.mainInfoText}>Email: </Text>
                    <Text style={[styles.mainInfoText, {color: '#42bec8'}]}>{user.email}</Text>
                </TouchableOpacity>
                <View style={styles.mainInfo}>
                    <Text style={styles.mainInfoText}>Пин: </Text>
                    <Text style={styles.mainInfoText}>{user.pin}</Text>
                </View>
            </View>
            <View style={styles.dataContainer}>
                <Text style={styles.title}>Личные данные</Text>
                <View style={styles.data}>
                    <Text style={styles.label}>Полное имя</Text>
                    <Text style={styles.dataText}>{user.name}</Text>
                </View>
                <View style={styles.data}>
                    <Text style={styles.label}>Национальность</Text>
                    <Text style={styles.dataText}>{checkData(user.nation)}</Text>
                </View>
                <View style={styles.data}>
                    <Text style={styles.label}>Язык</Text>
                    <Text style={styles.dataText}>{checkData(user.languages)}</Text>
                </View>
                <View style={styles.data}>
                    <Text style={styles.label}>Дата рождения</Text>
                    <Text style={styles.dataText}>{checkData(user.birthday)}</Text>
                </View>
                <View style={styles.data}>
                    <Text style={styles.label}>Пользовательский Email</Text>
                    <Text style={styles.dataText}>{user.email}</Text>
                </View>
                <View style={styles.data}>
                    <Text style={styles.label}>Мобильный телефон</Text>
                    <Text style={styles.dataText}>{checkData(user.cell_phone)}</Text>
                </View>
                <View style={styles.data}>
                    <Text style={styles.label}>Адрес</Text>
                    <Text style={styles.dataText}>{checkData(user.address)}</Text>
                </View>
                <View style={styles.data}>
                    <Text style={styles.label}>Почтовый код</Text>
                    <Text style={styles.dataText}>{checkData(user.postal)}</Text>
                </View>
                <View style={styles.data}>
                    <Text style={styles.label}>Иждивенцы</Text>
                    <Text style={styles.dataText}>{checkData(user.dependencies)}</Text>
                </View>
            </View>
            <View style={styles.dataContainer}>
                <Text style={styles.title}>Официальная информация</Text>
                <View style={styles.bigData}>
                    <Text style={styles.label}>Идентификационный документ</Text>
                    <Text style={[styles.dataText, styles.bigDataText]}>Паспорт</Text>
                </View>
                <View style={styles.bigData}>
                    <Text style={styles.label}>Годен до</Text>
                    <Text style={[styles.dataText, styles.bigDataText]}>{checkData(user.identical_valid_to)}</Text>
                </View>
                <View style={styles.bigData}>
                    <Text style={styles.label}>Идентификационный номер</Text>
                    <Text style={[styles.dataText, styles.bigDataText]}>{checkData(user.identical_number)}</Text>
                </View>
                <View style={styles.bigData}>
                    <Text style={styles.label}>Номер налогоплатильщика</Text>
                    <Text style={[styles.dataText, styles.bigDataText]}>{checkData(user.tax_number)}</Text>
                </View>
                <View style={styles.bigData}>
                    <Text style={styles.label}>Номер банковской карты</Text>
                    <Text style={[styles.dataText, styles.bigDataText]}>{checkData(user.bank_number)}</Text>
                </View>
                <View style={styles.bigData}>
                    <Text style={styles.label}>Водительское удостоверение</Text>
                    <Text style={[styles.dataText, styles.bigDataText]}>{checkData(user.driver_number)}</Text>
                </View>
                <View style={styles.bigData}>
                    <Text style={styles.label}>Номер медицинского страхования</Text>
                    <Text style={[styles.dataText, styles.bigDataText]}>{checkData(user.insurance_number)}</Text>
                </View>
                <View style={styles.bigData}>
                    <Text style={styles.label}>Номер социального стархования</Text>
                    <Text style={[styles.dataText, styles.bigDataText]}>{checkData(user.social_security_number)}</Text>
                </View>
                <View style={styles.bigData}>
                    <Text style={styles.label}>Дата последнего медицинского обследования</Text>
                    <Text style={[styles.dataText, styles.bigDataText]}>{checkData(user.medical_date)}</Text>
                </View>
            </View>
            {user && user.work_before && <View style={styles.dataContainer}>
                <Text style={styles.title}>Рабочий стаж</Text>
                <ScrollView style={{marginTop: 10}} horizontal={true}>
                {user.work_before.map((it, index) => {
                        return(
                    <View key={index} style={styles.itemContainer}>
                    <View style={{flexDirection: 'column', flex: 1}}>
                            <View style={styles.timeContainer}>
                                <Text style={styles.timeTitle}>Начало</Text>
                                <Text style={styles.time}>{it.begin}</Text>
                            </View>
                            <View style={styles.timeContainer}>
                                <Text style={styles.timeTitle}>Окончание</Text>
                                <Text style={styles.time}>{it.end}</Text>
                            </View>
                            <View style={styles.timeContainer}>
                                <Text style={styles.timeTitle}>Место</Text>
                                <Text style={styles.time}>{it.place}</Text>
                            </View>
                            <View style={styles.timeContainer}>
                                <Text style={styles.timeTitle}>Должность</Text>
                                <Text style={styles.time}>{it.description}</Text>
                            </View>
                    </View>
                    </View>
                        )
                    })}
                </ScrollView>
            </View>}
            {user && user.educ_before && <View style={styles.dataContainer}>
                <Text style={styles.title}>Образование</Text>
                <ScrollView style={{marginTop: 10}} horizontal={true}>
                    {user.educ_before.map((it, index) => {
                        return(
                    <View key={index} style={styles.itemContainer}>
                    <View style={{flexDirection: 'column', flex: 1}}>
                            <View style={styles.timeContainer}>
                                <Text style={styles.timeTitle}>Начало</Text>
                                <Text style={styles.time}>{it.begin}</Text>
                            </View>
                            <View style={styles.timeContainer}>
                                <Text style={styles.timeTitle}>Окончание</Text>
                                <Text style={styles.time}>{it.end}</Text>
                            </View>
                            <View style={styles.timeContainer}>
                                <Text style={styles.timeTitle}>Место</Text>
                                <Text style={styles.time}>{it.place}</Text>
                            </View>
                            <View style={styles.timeContainer}>
                                <Text style={styles.timeTitle}>Специальность</Text>
                                <Text style={styles.time}>{it.description}</Text>
                            </View>
                    </View>
                    </View>
                        )
                    })}
                </ScrollView>
            </View>}
            {user && user.user_contracts && <View style={styles.dataContainer}>
                <Text style={styles.title}>Контракты</Text>
                <ScrollView style={{marginTop: 10}} horizontal={true}>
                    {user.user_contracts.map((it, index) => {
                        return(
                    <View key={index} style={styles.itemContainer}>
                    <View style={{flexDirection: 'column', flex: 1}}>
                            <View style={styles.timeContainer}>
                                <Text style={styles.timeTitle}>Начало</Text>
                                <Text style={styles.time}>{it.begin}</Text>
                            </View>
                            <View style={styles.timeContainer}>
                                <Text style={styles.timeTitle}>Окончание</Text>
                                <Text style={styles.time}>{it.end}</Text>
                            </View>
                            <View style={styles.timeContainer}>
                                <Text style={styles.timeTitle}>Номер</Text>
                                <Text style={styles.time}>{it.number}</Text>
                            </View>
                            <View style={styles.timeContainer}>
                                <Text style={styles.timeTitle}>Файл документа</Text>
                                <TouchableOpacity style={{flexShrink: 1}} onPress={() => openLink(it.contract_file)}>
                                    <Text style={[styles.time, {color: '#42bec8'}]}>{it.contract_file_name}</Text>
                                </TouchableOpacity>
                            </View>
                    </View>
                    </View>
                        )
                    })}
                </ScrollView>
            </View>}
            {user && user.user_documents && <View style={styles.dataContainer}>
                <Text style={styles.title}></Text>
                <ScrollView style={{marginTop: 10}} horizontal={true}>
                    {user.user_documents.map((it, index) => {
                        return(
                    <View key={index} style={styles.itemContainer}>
                    <View style={{flexDirection: 'column', flex: 1}}>
                            <View style={styles.timeContainer}>
                                <Text style={styles.timeTitle}>Тип документа</Text>
                                <Text style={styles.time}>{it.type}</Text>
                            </View>
                            <View style={styles.timeContainer}>
                                <Text style={styles.timeTitle}>Файл документа</Text>
                                <TouchableOpacity style={{flexShrink: 1}} onPress={() => openLink(it.file)}>
                                    <Text style={[styles.time, {color: '#42bec8'}]}>{it.file_name}</Text>
                                </TouchableOpacity>
                            </View>
                    </View>
                    </View>
                        )
                    })}
                </ScrollView>
            </View>}
            <View style={styles.dataContainer}>
            <TouchableOpacity onPress={() => props.navigation.navigate('workSchedule', {id: user.id})} style={styles.btContainer}>
                <Text style={styles.btText}>Табель</Text>
                <Image style={styles.icon} source={require('../../../source/img/back.png')}/>
            </TouchableOpacity>
            </View>
            </View>
            </ScrollView>
            </View>
            </SafeAreaView>
           
        </LinearGradient>
        </>
        }
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        width: deviceWidth,
        flex: 1,
        flexDirection:'column', 
        alignItems:'center', 
        justifyContent:'center'
    },
    contentContainer: {
        flex: 1,
        width: '100%',
        justifyContent: 'center'
    },
    warning: {
        fontSize: 16, 
        fontWeight: '500',
        color: '#7E8086',
        textAlign: 'center',
        paddingVertical: 16
    },
    contactContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        width: deviceWidth-32,
        flex: 1,
        paddingHorizontal: 16,
        backgroundColor: '#FFF',
        borderRadius: 12,
        paddingVertical: 12,
        marginTop: 2,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        flexWrap: 'wrap',
        marginBottom: 8,
    },
    name: {
        fontSize: 17, 
        fontWeight: '500',
        color: '#7E8086',
    },
    id: {
        fontSize: 14, 
        fontWeight: '500',
        color: '#7E8086',
    },
    infoContainer: {
        flexDirection: 'column', 
        flex: 1,
        width: '100%',
        marginLeft: 15
    },
    title: {
        fontSize: 18, 
        fontWeight: '600',
        color: '#42bec8',
        paddingHorizontal: 8
    },
    dataContainer: {
        flex: 1,
        paddingHorizontal: 16,
        paddingTop: 10,
        paddingBottom: 5
    },
    active: {
        borderWidth: 2,
        borderColor: '#42bec8'
    },
    contact: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
    },
    contactIcon: {
        width: 19,
        height: 19,
        resizeMode: 'contain'
    },
    contactText: {
        fontSize: 14, 
        fontWeight: '500',
        color: '#7E8086',
        paddingLeft: 4
    },
    inputContainer: {
        width: '100%',
        height: 45,
        backgroundColor: '#dedede',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        position: 'relative',
        fontWeight: '500',
        backgroundColor: '#fff',
        borderRadius: 50,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        marginBottom: 15
    },
    input: {
        flex: 1,
        height: 45,
        backgroundColor: '#fff',
        borderRadius: 50,
        paddingHorizontal: 16,
        fontSize: 14
    },
    label: {
        fontWeight: '400',
        fontSize: 14,
        color: '#4D5860',
    },
    dataText: {
        fontWeight: '400',
        fontSize: 18,
    },
    bigDataText: {
        paddingTop: 2,
        fontSize: 18
    },
    data: {
        //flexDirection: 'row',
        //alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: 10,
        paddingHorizontal: 16
    },
    bigData: {
        paddingTop: 10,
        paddingHorizontal: 16
    },
    userPhoto: {
        width: 50,
        height: 50,
        borderRadius: 50,
        resizeMode: 'cover',
        borderWidth: 2
    },
    status: {
        width: 13,
        height: 13,
        borderRadius: 50,
        backgroundColor: "#42bec8"
    },
    profileContainer: {
        flex: 1,
        flexDirection: 'column',
        paddingHorizontal: 16,
        paddingVertical: 15,
        alignItems: 'center'
    },
    profileIcon: {
        width: 95,
        height: 95,
        borderRadius: 50,
    },
    profileName: {
        fontSize: 20,
        paddingTop: 10,
        paddingBottom: 5,
        fontWeight: '500'
    },
    mainInfo: {
        padding: 3,
        alignItems: 'center',
        flexDirection: 'row'
    },
    mainInfoText: {
        fontSize: 18,
    },
    itemContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 16,
        paddingVertical: 10,     
        justifyContent: 'space-between',
        backgroundColor: '#FFF',
        borderRadius: 12,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        width: deviceWidth-72,
        marginRight: 10
    },
    timeContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        flex: 1,
        paddingVertical: 5,
    },
    timeTitle: {
        fontSize: 16,
        fontWeight: '400',
        color: '#4d4d4f'
    },
    time: {
        fontSize: 16,
        fontWeight: '500',
        flexShrink: 1,
        flexWrap: 'wrap',
        textAlign: 'right',
        paddingLeft: 8,
    },
    btText: {
        fontWeight: '500',
        fontSize: 18,
        color: '#4D5860',
    },
    icon: {
        width: 16,
        height: 16,
        resizeMode: 'contain',
        opacity: .7,
        transform: [{ rotate: '180deg'}]
    },
    btContainer: {
        paddingVertical: 9,
        paddingHorizontal: 16,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
});

export default User;