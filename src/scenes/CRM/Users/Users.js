import React, {useEffect, useState} from 'react';

import LinearGradient from 'react-native-linear-gradient';

import {View, Text, StyleSheet, SafeAreaView, ScrollView,
        Dimensions, Image, TouchableOpacity, TextInput, ActivityIndicator,
        FlatList} from 'react-native';

import Header from '../../../components/Header';
let deviceWidth = Dimensions.get('window').width;
import { useSelector } from "react-redux";
import Preloader from '../../../components/Preloader';
import I18n from '../../../i18n/i18n';
import {CrmAPI} from  '../../../api/crm';


const Users = (props) => {
    const settings = useSelector(state => state.general.settings);

    let [load, setLoad] = useState(false);
    let [loadMore, setLoadMore] = useState(false);
    let [users, setUsers] = useState([]);
    let [offset, setOffsete] = useState(0);
    let [total, setTotal] = useState(0);
    let [searchText, setSearchText] = useState('');

    useEffect(() => {
        getUsers('');
    }, []);

    const getUsers = (data) => {
        setLoad(true);
        CrmAPI.getUsers({
            fields: 'id,name,email,photo,active',
            offset: 0,
            limit: 10,
            search: data
        })
            .then(res => {
                setLoad(false);
                setUsers(res.rows);
                setTotal(res.total);
            })
            .catch(err => {
                console.log(err, 'err');
            })
    }

    const loadMoreUsers = () => {
        console.log('loadMoreUsers');
        if(users.length < total){
            setLoadMore(true);
            CrmAPI.getUsers({
                fields: 'id,name,email,photo,active',
                offset: offset+10,
                limit: 10,
                search: searchText
            })
            .then(res => {
                setLoadMore(false);
                setUsers([...users, ...res.rows]);
                setOffsete(offset+10);
            })
            .catch(err => {
                setLoadMore(false);
                console.log(err, 'err');
            });
        }
        }

        let getPhoto = (url_photo) => {
            let domain = 'diga.pt';
                if(settings.subdomain === 'besterp'){
                        domain = 'rkesa.pt';
                    }
                let url = `https://${settings.subdomain}.${domain}${url_photo}`;
                return url;
        }

        const search = (data) => {
            getUsers(data);
            setSearchText(data);
        };

    return(
        <>
        {load && <Preloader/>}
        <SafeAreaView style={{flex: 0, backgroundColor: '#FEFEFE'}}/>
        <LinearGradient colors={['#FEFEFE', '#F9F9F9']} style={styles.container}>
            <SafeAreaView style={styles.contentContainer}>
            <Header leftIcon='menu' title={I18n.t('users')}
                    leftAction={() => props.navigation.openDrawer()}
                    rightAction={() => props.navigation.navigate('AddUser')}
                    rightIcon={'addUser'}
                    sizeIcon={24}
                />
            <View style={{flex: 1, alignItems: 'center'}}>
            <View style={[styles.inputWrapper, {paddingHorizontal: 16}]}>
                <View style={styles.inputContainer}>
                <TextInput
                        placeholder={I18n.t('search')}
                        style={styles.input}
                        placeholderTextColor={'#7E8086'}
                        onChangeText={(e) => search(e)}
                    />
                </View>
            </View>
            <View style={{flex: 1, width: '100%'}}>
            <View style={{flex: 1, paddingBottom: 20, alignItems: 'center'}}>
            <FlatList
                    data={users}
                    keyExtractor={(item, index) => String(index)}
                    onEndReachedThreshold={0}
                    onEndReached={data => {
                        console.log(data, 'onEndReached');
                        if(!loadMore){
                            loadMoreUsers();
                        }
                    }}
                    renderItem={data => {
                        let item = data.item;
                        return(
                            <TouchableOpacity onPress={() => props.navigation.navigate('User', {id: item.id})} style={styles.contactContainer}>
                            <Image style={[styles.userPhoto,  {borderColor: item.active ? '#42bec8' : '#7E8086'}]}
                             source={{uri: getPhoto(item.photo)}}/>
                            <View style={styles.infoContainer}>
                                <Text style={styles.name}>{item.name}</Text>
                                <Text style={[styles.service,  {color: '#42bec8'}]}>{item.email}</Text>
                            </View>
                                <View style={[styles.status, {backgroundColor: item.active ? '#42bec8' : '#7E8086'}]}/>
                            </TouchableOpacity>
                        )
                    }}
                />
                {loadMore && <ActivityIndicator style={{marginTop: 15}} size="small" color="#42bec8"/>}
            </View>
            </View>
            </View>
            </SafeAreaView>
        </LinearGradient>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        width: deviceWidth,
        flex: 1,
        flexDirection:'column', 
        alignItems:'center', 
        justifyContent:'center'
    },
    contentContainer: {
        flex: 1,
        width: '100%',
        justifyContent: 'center'
    },
    warning: {
        fontSize: 16, 
        fontWeight: '500',
        color: '#7E8086',
        textAlign: 'center',
        paddingVertical: 16
    },
    contactContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        width: deviceWidth-32,
        flex: 1,
        paddingHorizontal: 16,
        backgroundColor: '#FFF',
        borderRadius: 12,
        paddingVertical: 12,
        marginTop: 2,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        flexWrap: 'wrap',
        marginBottom: 8,
    },
    name: {
        fontSize: 17, 
        fontWeight: '500',
        color: '#7E8086',
    },
    id: {
        fontSize: 14, 
        fontWeight: '500',
        color: '#7E8086',
    },
    infoContainer: {
        flexDirection: 'column', 
        flex: 1,
        width: '100%',
        marginLeft: 15
    },
    title: {
        fontSize: 14, 
        fontWeight: '400',
        color: '#7E8086',
    },
    active: {
        borderWidth: 2,
        borderColor: '#42bec8'
    },
    contact: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
    },
    contactIcon: {
        width: 19,
        height: 19,
        resizeMode: 'contain'
    },
    contactText: {
        fontSize: 14, 
        fontWeight: '500',
        color: '#7E8086',
        paddingLeft: 4
    },
    inputContainer: {
        width: '100%',
        height: 45,
        backgroundColor: '#dedede',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        position: 'relative',
        fontWeight: '500',
        backgroundColor: '#fff',
        borderRadius: 50,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        marginBottom: 15
    },
    input: {
        flex: 1,
        height: 45,
        backgroundColor: '#fff',
        borderRadius: 50,
        paddingHorizontal: 16,
        fontSize: 14
    },
    label: {
        paddingHorizontal: 16,
        marginTop: 15,
        fontWeight: '500',
        fontSize: 14,
        color: '#4D5860',
    },
    userPhoto: {
        width: 50,
        height: 50,
        borderRadius: 50,
        resizeMode: 'cover',
        borderWidth: 2
    },
    status: {
        width: 13,
        height: 13,
        borderRadius: 50,
        backgroundColor: "#42bec8"
    }
});

export default Users;