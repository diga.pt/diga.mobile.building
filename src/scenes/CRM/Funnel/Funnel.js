import React, {useEffect, useState} from 'react';

import LinearGradient from 'react-native-linear-gradient';

import {View, Text, StyleSheet, SafeAreaView, ScrollView,
        Dimensions, Image, TouchableOpacity, TextInput} from 'react-native';

import Header from '../../../components/Header';

let deviceWidth = Dimensions.get('window').width;
import { useDispatch } from "react-redux";
import Preloader from '../../../components/Preloader';
import I18n from '../../../i18n/i18n';
import {CrmAPI} from  '../../../api/crm';



const Contacts = (props) => {

    let [load, setLoad] = useState(false);
    let [activeCategory, setActiveCategory] = useState(0);
    let [services, setServices] = useState([]);

    useEffect(() => {
        setLoad(true);
         CrmAPI.getFunnel({from: '2018-01-01', to: '2020-12-31'})
            .then(res => {
            let servicesList = [];
            for(let item in res){
                servicesList.push({
                    name: item,
                    data: res[item].services
                })
            }
            console.log(res, 'res');
            setServices(servicesList);
            setLoad(false);
            })
            .catch(err => {
            console.log(err, 'err');
            setLoad(false);
            });
            
    }, []);

    return(
        <>
        {load && <Preloader/>}
        <SafeAreaView style={{flex: 0, backgroundColor: '#FEFEFE'}}/>
        <LinearGradient colors={['#FEFEFE', '#F9F9F9']} style={styles.container}>
            <SafeAreaView style={styles.contentContainer}>
            <Header leftIcon='menu' title={I18n.t('funnel')}
                    leftAction={() => props.navigation.openDrawer()}
                    sizeIcon={24}
                />
            <View>
                <ScrollView showsHorizontalScrollIndicator={false} style={{paddingVertical: 10}} horizontal={true}>
                    {services.length > 0 && services.map((item, index) => {
                        return(
                            <View key={index}>
                                <TouchableOpacity activeOpacity={1} onPress={() => setActiveCategory(index)} 
                                        style={[styles.category, index === 0 ? {marginLeft: 16} : 
                                                    {marginRight: 16}, activeCategory === index && styles.active]}>
                                    <Text style={[styles.categoryText, activeCategory === index && {color: '#fff'}]}>{item.name}</Text>
                                </TouchableOpacity>
                            </View>
                        )  
                    })}
                </ScrollView>
            </View>
            <View style={{flex: 1, alignItems: 'center'}}>
            <ScrollView style={{flex: 1, width: '100%'}} showsVerticalScrollIndicator={false}>
            <View style={{flex: 1, paddingBottom: 20, alignItems: 'center'}}>
            {services.length > 0 && services[activeCategory].data.map((item, index) => {
                return(
                <View key={index} style={styles.itemContainer}>
                    <View style={{flexDirection: 'column', flex: 1}}>
                            <View style={[styles.item, {paddingTop: 0}]}>
                                <Text style={styles.title}>Название</Text>
                                <Text style={styles.text}>{item.name} ({item.estimate_number})</Text>
                            </View>
                            <View style={[styles.item]}>
                                <Text style={styles.title}>Адрес</Text>
                                <Text style={styles.text}>{item.address}</Text>
                            </View>
                            <View style={[styles.item]}>
                                <Text style={styles.title}>Создан</Text>
                                <Text style={styles.text}>{item.created_at}</Text>
                            </View>
                    </View>
                </View>
                )
            })}
            </View>
            </ScrollView>
            </View>
            </SafeAreaView>
        </LinearGradient>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        width: deviceWidth,
        flex: 1,
        flexDirection:'column', 
        alignItems:'center', 
        justifyContent:'center'
    },
    contentContainer: {
        flex: 1,
        width: '100%',
        justifyContent: 'center'
    },
    category: {
        padding: 8,
        paddingHorizontal: 16
    },
    categoryText: {
        fontSize: 15,
        color: '#000'
    },
    active: {
        backgroundColor: '#42bec8',
        borderRadius: 12,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    itemContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 22,
        paddingVertical: 22,     
        justifyContent: 'space-between',
        backgroundColor: '#FFF',
        borderRadius: 12,
        marginHorizontal: 16,
        marginTop: 10,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    item: {
         flexDirection: 'row',
         justifyContent: 'space-between',
         flex: 1,
         paddingTop: 8
    },
    title: {
        fontSize: 15,
        fontWeight: '400',
        color: '#4d4d4f'
    },
    text: {
        fontSize: 16,
        fontWeight: '500',
        flexShrink: 1,
        flexWrap: 'wrap',
        textAlign: 'right',
        paddingLeft: 8,
    }
});

export default Contacts;