import React, {useEffect, useState} from 'react';

import LinearGradient from 'react-native-linear-gradient';

import {View, Text, StyleSheet, SafeAreaView, ScrollView,
        Dimensions, Image, TouchableOpacity, TextInput, FlatList,
        ActivityIndicator} from 'react-native';

import Header from '../../../components/Header';
let deviceWidth = Dimensions.get('window').width;
import { useDispatch, useSelector } from "react-redux";
import Preloader from '../../../components/Preloader';
import I18n from '../../../i18n/i18n';
import {CrmAPI} from  '../../../api/crm';
import moment from 'moment';
import Toast from 'react-native-toast-message';
import CustomSwitch from '../../../components/Switch';
import { useIsFocused } from "@react-navigation/native";

const list = [
    {
        name: 'Сегодня',
        key: 'today'
    },
    {
        name: 'Текущий месяц',
        key: 'currentMonth'
    },
    {
        name: 'Месяц',
        key: 'month'
    },
    {
        name: 'Неделя',
        key: 'week'
    },
    {
        name: 'День',
        key: 'day'
    }
];




const Calendar = (props) => {

    let [date, setDate] = useState(null);
    const isFocused = useIsFocused();

    let [load, setLoad] = useState(false);
    const user = useSelector(state => state.user.user);
    let [activeCategory, setActiveCategory] = useState({
        name: 'Сегодня',
        key: 'today'
    });
    let [eventTypes, setEventTypes] = useState([]);
    let [events, setEvents] = useState([]);
    let [activeEvent, setActiveEvent] = useState();
    let [showMyTask, setShowMyTask] = useState(true);


    useEffect(() => {
       /* if(date){
            getData(date);
        } else {
            getDate('today');
        }*/
      }, [isFocused]);

    useEffect(() => {
        setLoad(true);
        CrmAPI.getEventTypes()
        .then(res => {

            let types = res.map((it) => {
               return {
                   label: it.title, value: it.id, key: it.id
               }
            })
            setEventTypes(types);
            setLoad(false);
        })
        .catch((err) => {
                console.log(err, 'err');
                setLoad(false);
        });
        
    }, []);


    let getDate = (type) => {
        let now = moment();
        let today = now.format("YYYY-MM-DD");
        switch(type){
            case 'today': {
                let date = { start: today, end: null};
                setDate(date);
                getData(date);
                break;
            }
            case 'currentMonth': {
                const startOfMonth = moment().startOf('month').format('YYYY-MM-DD');
                const endOfMonth   = moment().endOf('month').format('YYYY-MM-DD');
                let date = { start: startOfMonth, end: endOfMonth};
                setDate(date)
                getData(date);
                break;
            }
            case 'month': {
                const startOfMonth = moment().startOf('month').format('YYYY-MM-DD');
                const endOfMonth   = moment().endOf('month').format('YYYY-MM-DD');
                let date = { start: startOfMonth, end: endOfMonth};
                setDate(date)
                getData(date);
                break;
            }
            case 'week': {
                var new_date = moment(today).add(7, 'days').format("YYYY-MM-DD");
                let date = { start: today, end: new_date};
                setDate(date)
                getData(date);
                break;
            }
            case 'day': {
                let date = { start: today, end: null};
                setDate(date)
                getData(date);
                break;
            }
        }
    }

    let updatePeriod = (action, type) => {
        let now = moment();
        let today = now.format("YYYY-MM-DD");
        switch(type){
            case 'today': {
                let new_date = moment(date.start).add(action === 'plus' ? 1 : -1, 'days').format("YYYY-MM-DD");
                let newDate = {start: new_date, end: null};
                setDate(newDate)
                getData(newDate);
                break;
            }
            case 'currentMonth': {
                let newMonth = moment(date.start).add(action === 'plus' ? 1 : -1, 'month').format("YYYY-MM-DD");
                const endOfMonth   = moment(newMonth).endOf('month').format('YYYY-MM-DD');
                const startOfMonth = moment(newMonth).startOf('month').format('YYYY-MM-DD');
                let newDate = { start: startOfMonth, end: endOfMonth};
                setDate(newDate);
                getData(newDate);
                break;
            }
            case 'month': {
                let newMonth = moment(date.start).add(action === 'plus' ? 1 : -1, 'month').format("YYYY-MM-DD");
                const endOfMonth   = moment(newMonth).endOf('month').format('YYYY-MM-DD');
                const startOfMonth = moment(newMonth).startOf('month').format('YYYY-MM-DD');
                let newDate = { start: startOfMonth, end: endOfMonth};
                setDate(newDate);
                getData(newDate);
                break;
            }
            case 'week': {
                let end = moment(date.end).add(action === 'plus' ? 7 : -7, 'days').format("YYYY-MM-DD");
                let start = moment(date.start).add(action === 'plus' ? 7 : -7, 'days').format("YYYY-MM-DD");
                let newDate = {start, end};
                setDate(newDate);
                getData(newDate);
                break;
            }
            case 'day': {
                let new_date = moment(date.start).add(action === 'plus' ? 1 : -1, 'days').format("YYYY-MM-DD");
                let newDate = {start: new_date, end: null};
                setDate(newDate)
                getData(newDate);
                break;
            }
    }
}

    const getData = (date) => {

        let params = {
            done: 0,
            event_type_id: 0,
            start: date.start,
            end: date.end ? date.end : date.start,
            user_id: user.id
        };

        /*if(date.end){
            params['end'] = date.end;
        }*/

        setLoad(true);
        
        CrmAPI.getEvents(params)
            .then(res => {
                let items = res;
                 
                setEvents(res);
                setLoad(false);
            })
            .catch(err => {
            console.log(err, 'err');
            setLoad(false);
        });
    }

    const finish = (id, key) => {
        setLoadModal(true);
        CrmAPI.finish({id})
        .then(res => {
            setLoadModal(false);
            if(res.errcode === 0){
                getData(date);
                setModalMore(false);
                setActiveEvent({...activeEvent, done: 1});
                Toast.show({
                    type: 'success',
                    position: 'bottom',
                    text1: I18n.t('attention'),
                    text2: I18n.t('successData'),
                    visibilityTime: 4000,
                    autoHide: true,
                    bottomOffset: 45
                  });
                } else {
                Toast.show({
                    type: 'error',
                    position: 'bottom',
                    text1: I18n.t('attention'),
                    text2: I18n.t('error'),
                    visibilityTime: 4000,
                    autoHide: true,
                    bottomOffset: 45
                });
            }
        })
        .catch(err => {
            setLoadModal(false);
            Toast.show({
                type: 'error',
                position: 'bottom',
                text1: I18n.t('subdomain'),
                text2: I18n.t('subdomain'),
                visibilityTime: 4000,
                autoHide: true,
                bottomOffset: 45
              });
        })
     }

     const filterData = (type) => {
        let items = events;
        let list = [];

        for(let item in items){
            let data = {
                ...items[item]
            };

            let index = list.findIndex(it => it.date === items[item].start.split(' ')[0]);

            if(type === false){
                if(items[item].user_id === user.id){
                if(index >= 0){
                    list[index] = {
                        ...list[index],
                        items: [
                            ...list[index].items,
                            data
                        ]
                    }
            } else {
                list.push({
                    date: items[item].start.split(' ')[0],
                    items: [
                        data
                    ]
                });
            }
        }
            } else {
                if(index >= 0){
                    list[index] = {
                        ...list[index],
                        items: [
                            ...list[index].items,
                            data
                        ]
                    }
            } else {
                list.push({
                    date: items[item].start.split(' ')[0],
                    items: [
                        data
                    ]
                });
            }
            }

            

        }
        return list;
     }

    return(
            <SafeAreaView style={styles.contentContainer}>
            <Header leftIcon='menu' title={I18n.t('calendar')}
                    leftAction={() => props.navigation.openDrawer()}
                    rightAction={() => props.navigation.navigate('AddTask', {id: null, name: null})}
                    rightIcon={'plus'}
                />
                        

            <View style={{flex: 1}}>
           {date && <View style={styles.dateContainer}>
                    <TouchableOpacity  onPress={() => updatePeriod('minus', activeCategory.key)} style={styles.dateControl}>
                        <Image  style={[styles.dateControlIcon, {marginLeft: -3}]} 
                                source={require('../../../source/img/back.png')}/>
                    </TouchableOpacity>
                        <Text style={styles.date}>{date.start}{date.end && ` / ${date.end}`}</Text>
                    <TouchableOpacity onPress={() => updatePeriod('plus', activeCategory.key)} style={styles.dateControl}>
                        <Image  style={[styles.dateControlIcon, {marginRight: -3, transform: [{ rotate: "180deg" }]}]} 
                                source={require('../../../source/img/back.png')}/>
                    </TouchableOpacity>
            </View>}
            <View>
            <ScrollView showsHorizontalScrollIndicator={false} style={{paddingVertical: 5}} horizontal={true}>
                    {list.length > 0 && list.map((item, index) => {
                        return(
                            <View key={index}>
                                <TouchableOpacity activeOpacity={1} onPress={() => {
                                        setActiveCategory(item);
                                        getDate(item.key);
                                    }} 
                                        style={[styles.category, index === 0 ? {marginLeft: 16} : 
                                                    {marginRight: 16}, activeCategory.key === item.key && styles.active]}>
                                    <Text style={[styles.categoryText, activeCategory.key === item.key && {color: '#fff'}]}>{item.name}</Text>
                                </TouchableOpacity>
                            </View>
                        )  
                    })}
           </ScrollView>
           <View style={styles.switch}>
            <CustomSwitch title={'Показать мои задачи'} data={showMyTask} action={() => setShowMyTask(!showMyTask)}/>
           </View>
           </View>
                {load && <ActivityIndicator style={{marginTop: 15}} size="small" color="#42bec8"/>}
                {eventTypes.length > 0 && <FlatList
                    data={load && events ? [] : filterData(showMyTask)}
                    style={styles.itemContainer}
                    renderItem={(data) => {
                                return(
                                    <View style={styles.calendarContainer}>
                                        <View style={styles.dateList}>
                                            <Text style={styles.day}>{moment(data.item.date).format('DD')}</Text>
                                            <Text style={styles.month}>{moment(data.item.date).format('MMM')}</Text>
                                        </View>
                                        <View style={styles.itemContainer}>
                                        {data.item.items.map((it, index) => {
                                            let type = eventTypes.find(i => i.value === it.event_type_id);
                                            return(
                                                <TouchableOpacity onPress={() => {
                                                                                    props.navigation.navigate('Task', {id: it.id});
                                                                                }}
                                                                    key={index} style={[styles.item,   {borderLeftColor: it.done === 0 ? '#42bec8' : '#b5b5b587'}]}>
                                                    <View style={styles.descriptionContainer}> 
                                                                                
                                                            <Text style={[styles.description]}>
                                                                {type.label}{' '}                            
                                                                    <Text onPress={() => props.navigation.navigate('Client', {id: it.client_contact.id})} 
                                                                            style={[styles.description, {color: '#42bec8', fontWeight: '500'}]}>
                                                                        {it.client_contact && it.client_contact.name}
                                                                    </Text>  
                                                                 </Text>  
                                                            
                                                    </View>
                                                </TouchableOpacity>
                                            )
                                        })}
                                        </View>
                                    </View>
                                )
                            }}
                    keyExtractor={(item, index) => String(index)}
                />}
          
            </View>
            </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        width: deviceWidth,
        flex: 1,
        flexDirection:'column', 
    },
    descriptionContainer: {
        //flexDirection: 'row'
    },
    description: {
        flexDirection:'row', 
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'center'
    },
    contentContainer: {
        flex: 1,
        width: '100%',
        backgroundColor: '#fff'
    },
    date: {
        fontSize: 16,
        color: '#7E8086',
        fontWeight: '500',
        paddingHorizontal: 22
    },
    calendarContainer: {
        paddingHorizontal: 16,
        flexDirection: 'row',
        paddingTop: 15,
        alignItems: 'flex-start'
    },
    dateContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 10,
        flexDirection: 'row'
    },
    dateControl: {
        width: 32,
        height: 32,
        borderRadius: 50,
        backgroundColor: '#e9ecef',
        alignItems: 'center',
        justifyContent: 'center'
    },
    dateControlIcon: {
        width: 16,
        resizeMode: 'contain'
    },
    day: {
        color: '#7E8086',
        fontWeight: '300',
        fontSize: 21
    },
    month:{
        color: '#7E8086',
        fontWeight: '300',
        fontSize: 16
    },
    dateList: {
        justifyContent: 'center',
        paddingRight: 15
    },
    itemContainer: {
        flex: 1,
    },
    item: {
        backgroundColor: '#fff',
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        borderRadius: 12,
        paddingVertical: 12,
        paddingHorizontal: 12,
        marginBottom: 8,
        flex: 1,
        flexDirection: 'row',
        borderLeftWidth: 5
    },
    category: {
        padding: 8,
        paddingHorizontal: 14
    },
    categoryText: {
        fontSize: 14,
        color: '#000'
    },
    active: {
        backgroundColor: '#42bec8',
        borderRadius: 12,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    modalContainer: {
        width: deviceWidth-42,
        padding: 16,
        borderRadius: 8,
        backgroundColor: '#fff',
        maxWidth: 450
    },
    cancleContainer: {
        width: '100%',
        alignItems: "center",
        marginTop: 10
    },
    cancleBt: {
        padding: 10
    },
    cancleText: {
      fontSize: 16,
      textTransform: 'uppercase',
      fontWeight: '600',
      color: '#7E8086'
    },
   
    moreContainer: {
         paddingTop: 10,
         flexDirection: 'row',
    },
    moreTitle: {
        fontSize: 18,
        fontWeight: '400',
        color: '#4d4d4f'
    },
    moreText: {
        fontSize: 18,
        fontWeight: '500',
        color: '#000000',
        paddingLeft: 5,
        flexShrink: 1,
    },
    close: {
        position: 'absolute',
        top: 0,
        right: 0,
        padding: 2,
    },
    closeIcon: {
        width: 20, 
        height: 20
    },
    modalTitle: {
        color: '#7E8086',
        fontSize: 22,
        width: '100%',
        textAlign: 'center'
    },
    modalHeader: {
        flexDirection: 'row',
    },
    bt: {
        paddingVertical: 5,
        //marginTop: 5
    },
    btText: {
        fontSize: 16,
        textTransform: 'uppercase',
        textAlign: 'center'
    },
    switch: {
        paddingHorizontal: 16,
        paddingVertical: 15
    }
});

export default Calendar;