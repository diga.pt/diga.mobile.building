import React, {useEffect, useState} from 'react';

import {View, Text, StyleSheet, SafeAreaView, ScrollView,
        Dimensions, Image, TouchableOpacity, TextInput} from 'react-native';

let deviceWidth = Dimensions.get('window').width;

import I18n from '../../../i18n/i18n';
import { renderInput, renderSelect} from '../../../components/form';
import { reduxForm, Field, reset} from 'redux-form';
import {required, email} from '../../../components/form/validator';
import CustomButton from '../../../components/CustomButton';
import {Autocomplete} from '../../../components/Autocomplete';

const AddTaskForm = (props) => {
    const { handleSubmit, dispatch } = props;

    const addTask = (data) => {
        props.addTask(data);
    }

    return(
        <>

                <Field  name={`event_type_id`}
                        component={renderSelect}
                        label={I18n.t('taskType')}
                        placeholder={I18n.t('taskType')}
                        validate={[required]}
                        type={'default'}
                        shadow={true}
                        items={props.event_type_id}
                        />
                  <Field  name={`user_id`}
                        component={Autocomplete}
                        label={'Ответственный за задачу'}
                        placeholder={'Ответственный за задачу'}
                        validate={[required]}
                        shadow={true}
                        data={props.users}
                        search={props.search}
                        setFocus={(type) => props.setFocus(type)}
                        type={'users'}
                        activeInput={props.activeInput}
                        index={9999}
                        statusDisabled={true}
                        defaultValue={''}
                        />
                <Field  name={`client_contact_id`}
                        component={Autocomplete}
                        label={'Контакт'}
                        placeholder={'Контакт'}
                        shadow={true}
                        data={props.contacts}
                        search={props.search}
                        setFocus={(type) => props.setFocus(type)}
                        type={'contacts'}
                        activeInput={props.activeInput}
                        index={9998}
                        statusDisabled={props.clientName ? false : true}
                        defaultValue={props.clientName ? props.clientName : ''}
                />
                <Field  name={`service_id`}
                        component={Autocomplete}
                        label={'Услуга'}
                        placeholder={'Услуга'}
                        shadow={true}
                        data={props.services}
                        search={props.search}
                        setFocus={(type) => props.setFocus(type)}
                        type={'services'}
                        activeInput={props.activeInput}
                        index={9997}
                        statusDisabled={true}
                        defaultValue={''}
                        />
                <Field  name={`description`}
                        component={renderInput}
                        label={'Описание'}
                        placeholder={'Описание'}
                        type={'default'}
                        shadow={true}
                        />
            <View style={{}}>
                <CustomButton action={handleSubmit(addTask)} title={I18n.t('addTask')}/>
            </View>
            </>
    )
}

const styles = StyleSheet.create({
    container: {
        width: deviceWidth,
        flex: 1,
        flexDirection:'column', 
        alignItems:'center', 
        justifyContent:'center'
    },
    contentContainer: {
        flex: 1,
        width: '100%',
        justifyContent: 'center'
    },
    warning: {
        fontSize: 16, 
        fontWeight: '500',
        color: '#7E8086',
        textAlign: 'center',
        paddingVertical: 16
    },
    contactContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        width: deviceWidth-32,
        flex: 1,
        paddingHorizontal: 16,
        backgroundColor: '#FFF',
        borderRadius: 12,
        paddingVertical: 12,
        marginTop: 2,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        flexWrap: 'wrap',
        marginBottom: 8,
    },
    name: {
        fontSize: 17, 
        fontWeight: '500',
        color: '#7E8086',
    },
    id: {
        fontSize: 14, 
        fontWeight: '500',
        color: '#7E8086',
    },
    infoContainer: {
        flexDirection: 'column', 
        flex: 1,
        width: '100%',
        marginLeft: 15
    },
    title: {
        fontSize: 14, 
        fontWeight: '400',
        color: '#7E8086',
    },
    active: {
        borderWidth: 2,
        borderColor: '#42bec8'
    },
    contact: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
    },
    contactIcon: {
        width: 19,
        height: 19,
        resizeMode: 'contain'
    },
    contactText: {
        fontSize: 14, 
        fontWeight: '500',
        color: '#7E8086',
        paddingLeft: 4
    },
    inputContainer: {
        width: '100%',
        height: 45,
        backgroundColor: '#dedede',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        position: 'relative',
        fontWeight: '500',
        backgroundColor: '#fff',
        borderRadius: 50,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    input: {
        flex: 1,
        height: 45,
        backgroundColor: '#fff',
        borderRadius: 50,
        paddingHorizontal: 16,
        fontSize: 14
    },
    label: {
        paddingHorizontal: 16,
        marginTop: 15,
        fontWeight: '500',
        fontSize: 14,
        color: '#4D5860',
    },
    dateTitle: {
        fontWeight: '500',
        fontSize: 14,
        color: '#4D5860',
    },
    dateHeader: {
        paddingHorizontal: 16,
        width: '100%',
        marginTop: 8
    },
    dateContainer: {
        flexDirection: 'row',
        marginTop: 2,
        //marginBottom: 8
    },
    dateItem: {
        flex: 1,
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    date: {
        fontSize: 14,
    },
    icon: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        marginRight: 8
    },
});

export default reduxForm({
    form: 'AddTaskForm',
    enableReinitialize: true
  })(AddTaskForm);
