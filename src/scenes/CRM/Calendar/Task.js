import React, {useEffect, useState} from 'react';

import LinearGradient from 'react-native-linear-gradient';

import {View, Text, StyleSheet, SafeAreaView, ScrollView,
    Dimensions, Image, TouchableOpacity, Linking, ActivityIndicator,
    FlatList} from 'react-native';
import Clipboard from '@react-native-community/clipboard';

import Header from '../../../components/Header';
import Preloader from '../../../components/Preloader';

let deviceWidth = Dimensions.get('window').width;

import {CrmAPI} from  '../../../api/crm';
import I18n from '../../../i18n/i18n';
import Toast from 'react-native-toast-message';
import Geocoder from 'react-native-geocoding';


const Task = (props) => {

    let [load, setLoad] = useState(false);
    let [data, setData] = useState(false);
    let [eventTypes, setEventTypes] = useState(null);
    let [allUsers, setAllUsers] = useState(null);
    Geocoder.init("AIzaSyAWAoJ2F0U1mir25P0fyUzf2P2mkQYnT6s");
    useEffect(() => {
        const unsubscribe = props.navigation.addListener('focus', () => {
            getData();
        });
        return unsubscribe;
    }, [props.navigation]);

    const getData = () => {
        setLoad(true);

        const eventTypes = CrmAPI.getEventTypes()
        .then(res => {

            let types = res.map((it) => {
               return {
                   label: it.title, value: it.id, key: it.id
               }
           })
            return types;
        });
        const allUsers = CrmAPI.getAllUsers()
        .then(res => {
            let data = res.map(it => {
                return {
                    label: it.name,
                    value: it.id,
                    key: it.id
                }
            });
           return data;
        });

        
        const event = CrmAPI.getEvent({id: props.route.params.id})
        .then(res => {
            console.log(res);
            return res.event;
        });

        Promise.all([eventTypes, allUsers, event])
        .then(res => {
            setEventTypes(res[0]);
            setAllUsers(res[1]);
            setData(res[2]);
            setLoad(false);
        })
        .catch(error => {
            setLoad(false);
            console.log(error, 'error');
        });
    }

    const finish = (id, key) => {
        setLoad(true);
        CrmAPI.finish({id})
        .then(res => {
            setLoad(false);            if(res.errcode === 0){
                setData({...data, done: 1});
                Toast.show({
                    type: 'success',
                    position: 'bottom',
                    text1: I18n.t('attention'),
                    text2: I18n.t('successData'),
                    visibilityTime: 4000,
                    autoHide: true,
                    bottomOffset: 45
                  });
                } else {
                Toast.show({
                    type: 'error',
                    position: 'bottom',
                    text1: I18n.t('attention'),
                    text2: I18n.t('error'),
                    visibilityTime: 4000,
                    autoHide: true,
                    bottomOffset: 45
                });
            }
        })
        .catch(err => {
            setLoad(false);
            Toast.show({
                type: 'error',
                position: 'bottom',
                text1: I18n.t('subdomain'),
                text2: I18n.t('subdomain'),
                visibilityTime: 4000,
                autoHide: true,
                bottomOffset: 45
              });
        })
     }

     const copyEmail = (email) => {
        Toast.show({
            type: 'success',
            position: 'bottom',
            text1: `Email ${email} скопирован`,
            visibilityTime: 1000,
            autoHide: true,
            bottomOffset: 45
          });
          Clipboard.setString(email);
    }

    const getLocation = (address) => {
        setLoad(true);
        Geocoder.from(address)
		.then(json => {
			var location = json.results[0].geometry.location;
			console.log(location);
            props.navigation.navigate('LogInfo', location);
            setLoad(false);
		})
		.catch(error => {
            setLoad(false);
        });
    }


    return(
        <>
        {load && <Preloader/>}
        <SafeAreaView style={{flex: 0, backgroundColor: '#FEFEFE'}}/>
        <LinearGradient colors={['#FEFEFE', '#F9F9F9']} style={styles.container}>
            <SafeAreaView style={styles.contentContainer}>
            <Header leftIcon='back' title={`Задача`}
                    leftAction={() => props.navigation.goBack()}
                    />
          {data &&  <View style={{flex: 1, paddingHorizontal: 16}}>
                            <View style={styles.moreContainer}>
                                <Text style={styles.moreTitle}>Тип задачи:</Text>
                                <Text style={styles.moreText}>{eventTypes && eventTypes.find(i => i.value === data.event_type_id).label}</Text>
                            </View>
                            <View style={styles.moreContainer}>
                                <Text style={styles.moreTitle}>Дата:</Text>
                                <Text style={styles.moreText}>{data.start}</Text>
                            </View>
                            <View style={styles.moreContainer}>
                                <Text style={styles.moreTitle}>Услуга:</Text>
                                <Text style={styles.moreText}>{data.service && data.service.name}</Text>
                            </View>
                            <View style={styles.moreContainer}>
                                <Text style={styles.moreTitle}>Описание:</Text>
                                <Text style={styles.moreText}>{data.description}</Text>
                            </View>
                            <View style={styles.moreContainer}>
                                <Text style={styles.moreTitle}>Ответственный:</Text>
                                <Text style={styles.moreText}>{allUsers && allUsers.find(i => i.value === data.user_id).label}</Text>
                            </View>
                            <View style={styles.moreContainer}>
                                <Text style={styles.moreTitle}>Имя:</Text>
                                {data.client_contact && <TouchableOpacity onPress={() => {
                                    props.navigation.navigate('Client', {id: data.client_contact.id});
                                }}>
                                <Text style={styles.moreText}>{data.client_contact.name}</Text>
                                </TouchableOpacity>}
                            </View>
                            <View style={styles.moreContainer}>
                                <Text style={styles.moreTitle}>Email:</Text>
                                {data.client_contact && data.client_contact.client_contact_emails.map((it, index) => {
                                let count = data.client_contact.client_contact_emails.length;
                                return(
                                    <Text onPress={() => copyEmail(it.email)} key={index} style={[styles.moreText]}>{it.email}{count - 1 !== index  && ', '}</Text>
                                )
                                })}
                            </View>
                            <View style={styles.moreContainer}>
                                <Text style={styles.moreTitle}>Телефон:</Text>
                                {data.client_contact && data.client_contact.client_contact_phones.map((it, index) => {
                                let count = data.client_contact.client_contact_phones.length;
                                return(
                                    <Text onPress={()=> {
                                        Linking.openURL(`tel:${it.phone_number}`)
                                    }} key={index} style={[styles.moreText]}>{it.phone_number}{count - 1 !== index  && ', '}</Text>
                                )
                                })}
                            </View>
                            <View style={styles.moreContainer}>
                                <Text style={styles.moreTitle}>Адрес:</Text>
                                {data.service && <TouchableOpacity onPress={() => getLocation(data.service.address)}>
                                    <Text style={styles.moreText}>{data.service.address}</Text>
                                </TouchableOpacity>}
                            </View>
                            {props.loadModal  ? <ActivityIndicator style={{marginTop: 15, marginBottom: 10}} size="small" color="#42bec8"/> :
                            data.done !== 1 && 
                            <>
                            <TouchableOpacity onPress={() => finish(data.id, data.key)} style={[styles.bt, {marginTop: 10}]}>
                                <Text style={[styles.btText, {color: '#42bec8', fontWeight: '600'}]}>Выполнено</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => {
                                    props.navigation.navigate('Responsible', {id: data.id});
                            }} style={[styles.bt]}>
                                <Text style={[styles.btText]}>Сменить ответственного</Text>
                            </TouchableOpacity>
                            </>
                            }
                            </View>} 
            </SafeAreaView>
        </LinearGradient>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        width: deviceWidth,
        flex: 1,
        flexDirection:'column', 
        alignItems:'center', 
        justifyContent:'center'
    },
    contentContainer: {
        flex: 1,
        width: '100%',
    },
    moreContainer: {
        paddingTop: 10,
        flexDirection: 'row',
   },
   moreTitle: {
       fontSize: 18,
       fontWeight: '400',
       color: '#4d4d4f'
   },
   moreText: {
       fontSize: 18,
       fontWeight: '500',
       color: '#000000',
       paddingLeft: 5,
       flexShrink: 1,
   },
   bt: {
    paddingVertical: 5,
},
btText: {
    fontSize: 16,
    textTransform: 'uppercase',
    textAlign: 'center'
},
});

export default Task;