import React, {useEffect, useState} from 'react';

import LinearGradient from 'react-native-linear-gradient';

import {View, Text, StyleSheet, SafeAreaView, ScrollView,
        Dimensions, Image, TouchableOpacity, 
        KeyboardAvoidingView, Platform} from 'react-native';

import Header from '../../../components/Header';

let deviceWidth = Dimensions.get('window').width;

import Preloader from '../../../components/Preloader';
import I18n from '../../../i18n/i18n';
import {CrmAPI} from  '../../../api/crm';
import {SettingsAPI} from '../../../api/settings';
import moment from 'moment';

import DateTimePickerModal from "react-native-modal-datetime-picker";
import AddTaskForm from './AddTaskForm';
import Toast from 'react-native-toast-message';
import { reset } from 'redux-form';


const AddTask = (props) => {

    let [load, setLoad] = useState(false);
   
    const [firstDate, setFirstDate] = useState();
    const [secondDate, setSecondDate] = useState();

    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const [activePicker, setActivePicker] = useState(null);
    let [eventTypes, setEventTypes] = useState([]);

    let [contacts, setContacts] = useState([]);
    let [users, setUsers] = useState([]);
    let [services, setServices] = useState([]);

    let [activeInput, setFocus] = useState(null);

    const showDatePicker = (picker) => {
        setActivePicker(picker);
        setDatePickerVisibility(true);
      };

      const hideDatePicker = () => {
        setDatePickerVisibility(false);
      };

      const handleConfirm = (date) => {
        console.log(date, 'date');
        if(activePicker === 0){
            setFirstDate(moment(date).format("YYYY-MM-DD HH:MM"));
        } else {
            setSecondDate(moment(date).format("YYYY-MM-DD HH:MM"));
        }    
        hideDatePicker();
      };

      useEffect(() => {
         let monday = moment().format("YYYY-MM-DD HH:MM");
         setFirstDate(monday);
         setSecondDate(monday);
         CrmAPI.getEventTypes()
         .then(res => {
             let types = res.map((it) => {
                return {
                    label: it.title, value: it.id, key: it.id
                }
            })
             setEventTypes(types);
         });
     }, []);


     const addTask = (data) => {
        let json = {
           ...data,
           start: firstDate+':00',
           end: secondDate+':00',
           done: 0,
           description: data.description ? data.description : '',
           client_contact_id: data.client_contact_id ? data.client_contact_id : ''
        };
        console.log(json, 'addTask2');

        setLoad(true);
        CrmAPI.addEvents(json)
           .then(res => {
           console.log(res, 'res');
           setLoad(false);
           if(res.errcode === 0){
            Toast.show({
                type: 'success',
                position: 'bottom',
                text1: I18n.t('attention'),
                text2: I18n.t('successData'),
                visibilityTime: 4000,
                autoHide: true,
                bottomOffset: 45
              });
           } else {
            Toast.show({
                type: 'error',
                position: 'bottom',
                text1: I18n.t('attention'),
                text2: I18n.t('permission'),
                visibilityTime: 4000,
                autoHide: true,
                bottomOffset: 45
              });
           }
             props.navigation.goBack();
       })
       .catch(err => {
           console.log(err, 'err');
           setLoad(false);
           Toast.show({
               type: 'error',
               position: 'bottom',
               text1: I18n.t('attention'),
               text2: I18n.t('error'),
               visibilityTime: 4000,
               autoHide: true,
               bottomOffset: 45
             });
       })
    }

     const search = (data, type) => {
         switch(type){
             case 'contacts': {
                SettingsAPI.contacts({query: data, offset: 0, limit: 20, responsible_user_id: 0})
                .then(res => {
                    let data = res.rows.map(it => {
                        return {
                            label: it.name,
                            id: it.id
                        }
                    });
                    setContacts(data);
                })
                .catch(err => {
                    console.log(err, 'err');
                });
                break;
             }
             case 'users': {
                CrmAPI.getUsers({
                    fields: 'id,name',
                    offset: 0,
                    limit: 20,
                    search: data
                })
                    .then(res => {
                        let data = res.rows.map(it => {
                            return {
                                label: it.name,
                                id: it.id
                            }
                        });
                        setUsers(data);
                    })
                    .catch(err => {
                        console.log(err, 'err');
                    })
                    break;
             }
             case 'services': {
                SettingsAPI.services({query: data})
                .then(res => {
                    console.log(res, 'res');
                    let data = res.rows.map(it => {
                        return {
                            label: it.name,
                            id: it.id
                        }
                    });
                    setServices(data);
                })
                .catch(err => {
                    console.log(err, 'err');
                })
                break;
             }
         }
     }

  


    return(
        <KeyboardAvoidingView style={{flex: 1}}
                        behavior={Platform.OS == "ios" ? "padding" : null}>
        {load && <Preloader/>}
        <SafeAreaView style={{flex: 0, backgroundColor: '#FEFEFE'}}/>
        <LinearGradient colors={['#FEFEFE', '#F9F9F9']} style={styles.container}>
            <DateTimePickerModal
                isVisible={isDatePickerVisible}
                mode="datetime"
                onConfirm={handleConfirm}
                onCancel={hideDatePicker}
                date={new Date(activePicker === 0 ? firstDate : secondDate)}
            />
            <SafeAreaView style={styles.contentContainer}>
         
           <View style={styles.contentContainer}>
            <Header leftIcon='back' title={I18n.t('addTask')}
                    leftAction={() => props.navigation.goBack()}
                    sizeIcon={24}
                />
            <View style={{flex: 1, alignItems: 'center'}}>
            <ScrollView 
                        style={{flex: 1, width: '100%'}} 
                            keyboardShouldPersistTaps='always' 
                            showsVerticalScrollIndicator={false}>
            <View style={{flex: 1, paddingBottom: 20,  width: '100%', paddingHorizontal: 16}}>
            <View style={styles.dateHeader}>
            <Text style={styles.dateTitle}>{I18n.t('date')}</Text>
                <View style={styles.dateContainer}>
                    <TouchableOpacity onPress={() => showDatePicker(0)} style={styles.dateItem}>
                        <Image style={styles.icon} source={require('../../../source/img/calendar_sort.png')}/>
                        <Text style={styles.date}>{firstDate}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => showDatePicker(1)} style={styles.dateItem}>
                        <Image style={styles.icon} source={require('../../../source/img/calendar_sort.png')}/>
                        <Text style={styles.date}>{secondDate}</Text>
                    </TouchableOpacity>
                </View>
            </View>

           <AddTaskForm event_type_id={eventTypes}
                        search={search}
                        setFocus={setFocus}
                        activeInput={activeInput}
                        contacts={contacts}
                        users={users}
                        services={services}
                        addTask={addTask}
                        initialValues={props.route.params.id ? 
                                            {client_contact_id: props.route.params.id} : 
                                            {}}
                        clientName={props.route.params.name}
                    />

            </View>
            </ScrollView>
            </View>
            </View>
            </SafeAreaView>
        </LinearGradient>
        </KeyboardAvoidingView>
    )
}

const styles = StyleSheet.create({
    container: {
        width: deviceWidth,
        flex: 1,
        flexDirection:'column', 
        alignItems:'center', 
        justifyContent:'center'
    },
    contentContainer: {
        flex: 1,
        width: '100%',
        justifyContent: 'center'
    },
    warning: {
        fontSize: 16, 
        fontWeight: '500',
        color: '#7E8086',
        textAlign: 'center',
        paddingVertical: 16
    },
    contactContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        width: deviceWidth-32,
        flex: 1,
        paddingHorizontal: 16,
        backgroundColor: '#FFF',
        borderRadius: 12,
        paddingVertical: 12,
        marginTop: 2,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        flexWrap: 'wrap',
        marginBottom: 8,
    },
    name: {
        fontSize: 17, 
        fontWeight: '500',
        color: '#7E8086',
    },
    id: {
        fontSize: 14, 
        fontWeight: '500',
        color: '#7E8086',
    },
    infoContainer: {
        flexDirection: 'column', 
        flex: 1,
        width: '100%',
        marginLeft: 15
    },
    title: {
        fontSize: 14, 
        fontWeight: '400',
        color: '#7E8086',
    },
    active: {
        borderWidth: 2,
        borderColor: '#42bec8'
    },
    contact: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
    },
    contactIcon: {
        width: 19,
        height: 19,
        resizeMode: 'contain'
    },
    contactText: {
        fontSize: 14, 
        fontWeight: '500',
        color: '#7E8086',
        paddingLeft: 4
    },
    inputContainer: {
        width: '100%',
        height: 45,
        backgroundColor: '#dedede',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        position: 'relative',
        fontWeight: '500',
        backgroundColor: '#fff',
        borderRadius: 50,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    input: {
        flex: 1,
        height: 45,
        backgroundColor: '#fff',
        borderRadius: 50,
        paddingHorizontal: 16,
        fontSize: 14
    },
    label: {
        paddingHorizontal: 16,
        marginTop: 15,
        fontWeight: '500',
        fontSize: 14,
        color: '#4D5860',
    },
    dateTitle: {
        fontWeight: '500',
        fontSize: 14,
        color: '#4D5860',
    },
    dateHeader: {
        paddingHorizontal: 16,
        width: '100%',
        marginTop: 8,
        marginBottom: 15
    },
    dateContainer: {
        flexDirection: 'row',
        marginTop: 2,
        //marginBottom: 8
    },
    dateItem: {
        flex: 1,
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    date: {
        fontSize: 14,
    },
    icon: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        marginRight: 8
    },
});

export default AddTask;