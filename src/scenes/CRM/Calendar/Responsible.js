import React, {useEffect, useState} from 'react';

import LinearGradient from 'react-native-linear-gradient';

import {View, Text, StyleSheet, SafeAreaView, ScrollView,
    Dimensions, Image, TouchableOpacity, TextInput, ActivityIndicator,
    FlatList} from 'react-native';

import Header from '../../../components/Header';
import Preloader from '../../../components/Preloader';

let deviceWidth = Dimensions.get('window').width;

import {CrmAPI} from  '../../../api/crm';
import I18n from '../../../i18n/i18n';
import Toast from 'react-native-toast-message';

const Responsible = (props) => {

    let [load, setLoad] = useState(false);
    let [loadMore, setLoadMore] = useState(false);
    let [users, setUsers] = useState([]);
    let [offset, setOffsete] = useState(0);
    let [total, setTotal] = useState(0);
    let [searchText, setSearchText] = useState('');
    let [activeId, setActiveId] = useState('');    

    useEffect(() => {
        getUsers('');
    }, []);

    const getUsers = (data) => {
        setLoad(true);
        CrmAPI.getUsers({
            fields: 'id,name',
            offset: 0,
            limit: 20,
            search: data
        })
            .then(res => {
                setLoad(false);
                setUsers(res.rows);
                setTotal(res.total);
            })
            .catch(err => {
                console.log(err, 'err');
            })
    }

    const loadMoreUsers = () => {
        if(users.length < total){
            setLoadMore(true);
            CrmAPI.getUsers({
                fields: 'id,name',
                offset: offset+20,
                limit: 20,
                search: searchText
            })
            .then(res => {
                setLoadMore(false);
                setUsers([...users, ...res.rows]);
                setOffsete(offset+20);
            })
            .catch(err => {
                setLoadMore(false);
                console.log(err, 'err');
            });
        }
        }

        const search = (data) => {
            getUsers(data);
            setSearchText(data);
        };

        const saveData = () => {
            setLoad(true);
            let id = props.route.params.id;
            let data = {
                id,
                data: {
                    user_id: activeId
                }
            }
            CrmAPI.updateEvent(data)
            .then(res => {
                setLoad(false);
                if(res.errcode === 0){
                    Toast.show({
                        type: 'success',
                        position: 'bottom',
                        text1: I18n.t('attention'),
                        text2: I18n.t('successData'),
                        visibilityTime: 4000,
                        autoHide: true,
                        bottomOffset: 45
                      });
                     props.navigation.goBack();
                } else {
                    Toast.show({
                        type: 'error',
                        position: 'bottom',
                        text1: I18n.t('attention'),
                        text2: I18n.t('error'),
                        visibilityTime: 4000,
                        autoHide: true,
                        bottomOffset: 45
                      });
                }
            })
            .catch(err => {
                setLoad(false);
                console.log(err, 'err');
                Toast.show({
                    type: 'error',
                    position: 'bottom',
                    text1: I18n.t('attention'),
                    text2: I18n.t('error'),
                    visibilityTime: 4000,
                    autoHide: true,
                    bottomOffset: 45
                  });
            });
            //updateEvent
        };

       

    return(
        <>
        {load && <Preloader/>}
        <SafeAreaView style={{flex: 0, backgroundColor: '#FEFEFE'}}/>
        <LinearGradient colors={['#FEFEFE', '#F9F9F9']} style={styles.container}>
            <SafeAreaView style={styles.contentContainer}>
            <Header leftIcon='back' title={`Ответственный`}
                    leftAction={() => props.navigation.goBack()}
                    rightAction={() => saveData()}
                    rightIcon={'check'}/>
            <View style={{flex: 1, alignItems: 'center'}}>
            <View style={styles.inputWrapper}>
                <View style={styles.inputContainer}>
                    <TextInput
                        placeholder={I18n.t('search')}
                        style={styles.input}
                        placeholderTextColor={'#7E8086'}
                        value={searchText}
                        onChangeText={(e) => search(e)}
                        autoCapitalize="none"
                        autoCorrect={false} 
                    />
                </View>
            </View>
            <View style={{flex: 1, paddingBottom: 20, alignItems: 'center', width: '100%'}}>
            <FlatList
                    data={users}
                    keyExtractor={(item, index) => String(index)}
                    onEndReachedThreshold={0}
                    onEndReached={data => {
                        if(!loadMore){
                            loadMoreUsers();
                        }
                    }}
                    renderItem={data => {
                        let item = data.item;
                        return(
                            <TouchableOpacity  
                                style={[styles.item]}
                                onPress={() => {
                                    setActiveId(activeId !== item.id ? item.id : null)
                                }}
                                activeOpacity={1}
                                >
                            <Text style={styles.itemLabel}>{item.name}</Text>
                            {activeId === item.id && <Image style={styles.icon} source={require('../../../source/img/check-symbol.png')}/>}
                            </TouchableOpacity>
                        )
                    }}
                />
                </View>
                {loadMore && <ActivityIndicator style={{marginTop: 15}} size="small" color="#42bec8"/>}
           {/* <ScrollView style={{flex: 1, width: '100%'}} showsVerticalScrollIndicator={false}>
            <View style={{flex: 1, paddingHorizontal: 16, paddingBottom: 15}}>
                {users.length === 0 ? <Text style={styles.warning}>{I18n.t('startSearch')}</Text>
                :
                users.map((item, index) => {
                    return(
                        <TouchableOpacity key={index} 
                                style={[styles.item]}
                                onPress={() => {
                                    setActiveId(activeId !== item.id ? item.id : null)
                                }}
                                activeOpacity={1}
                                >
                            <Text style={styles.itemLabel}>{item.name}</Text>
                            {activeId === item.id && <Image style={styles.icon} source={require('../../../source/img/check-symbol.png')}/>}
                        </TouchableOpacity>
                    )
                })}
            </View>
            </ScrollView>*/}
            </View>
            </SafeAreaView>
        </LinearGradient>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        width: deviceWidth,
        flex: 1,
        flexDirection:'column', 
        alignItems:'center', 
        justifyContent:'center'
    },
    contentContainer: {
        flex: 1,
        width: '100%',
    },
    input: {
        flex: 1,
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 50,
        paddingHorizontal: 16,
        fontSize: 15
    },
    inputWrapper: {
      width: deviceWidth-32,
      marginBottom: 12,
      marginTop: 8,
    },
    inputContainer: {
        width: '100%',
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 50,
        flexDirection: 'row',
        alignItems: 'center',
        position: 'relative',
        fontWeight: '500',
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    warning: {
        fontSize: 16, 
        fontWeight: '500',
        color: '#7E8086',
        textAlign: 'center',
        paddingVertical: 16
    },
    item: {
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 16,
        height: 48, 
        justifyContent: 'space-between',
        backgroundColor: '#FFF',
        borderRadius: 12,
        marginTop: 10,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        width: deviceWidth-32
    },
    itemLabel: {
        fontSize: 14, 
        fontWeight: '500',
        color: '#7E8086',
    },
    active: {
        borderWidth: 2,
        borderColor: '#42bec8'
    },
    icon: {
        width: 22,
        height: 22,
        resizeMode: 'contain'
    }
});

export default Responsible;