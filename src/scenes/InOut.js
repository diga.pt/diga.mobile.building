import React, {useState, useRef, useEffect} from 'react';

import LinearGradient from 'react-native-linear-gradient';
import CustomButton from '../components/CustomButton';

import {View, Text, StyleSheet, SafeAreaView, ScrollView,
        Dimensions, Image, TouchableOpacity, TextInput, PermissionsAndroid,
        Platform, AppState, ActivityIndicator, FlatList} from 'react-native';
import Geolocation from 'react-native-geolocation-service';

import Header from '../components/Header';

let deviceWidth = Dimensions.get('window').width;

import Modal from 'react-native-modal';

import { RNCamera } from 'react-native-camera';

import { openSettings, check, PERMISSIONS } from 'react-native-permissions';

import { useDispatch, useSelector, connect } from "react-redux";

import Preloader from '../components/Preloader';

import moment from 'moment';

import AsyncStorage from '@react-native-community/async-storage';

import RNFetchBlob from 'rn-fetch-blob'

import Toast from 'react-native-toast-message';

import {UserAPI} from '../api/user';
import {SettingsAPI} from '../api/settings';

import { CommonActions } from '@react-navigation/native';

import {updateAdditionalUser} from '../reducers/User';


import I18n from '../i18n/i18n';
const CELL_COUNT = 4;

import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from 'react-native-confirmation-code-field';

const ModaldSucces = (props) => {

  return(
      <Modal isVisible={props.isVisible}>
      <SafeAreaView style={{ flex: 1 }}>
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <View style={[styles.modalContainer]}>
             <View style={{flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center'}}>
                  <Image style={styles.modalIcon} source={require('../source/img/checked.png')}/>
                      <Text style={styles.modalText}>{I18n.t('successData')}</Text>
              </View>
              <View style={{paddingTop: 16}}/>
              <CustomButton action={() => props.action()} title={I18n.t('ok')}/>
          </View>
        </View>
      </SafeAreaView>
      </Modal>
  )
}


const ModalInOut = (props) => {

    const [value, setValue] = useState('');
    
    const ref = useBlurOnFulfill({value, cellCount: CELL_COUNT});
    const [propsCode, getCellOnLayoutHandler] = useClearByFocusCell({
        value,
        setValue,
    });
  
  
    const renderPin = () => {
      return(
          <View style={styles.modal}>
              <Text style={styles.titleModal}>{I18n.t('pin')}</Text>
              <View style={styles.codeContainer}>
              <CodeField
                  ref={ref}
                  {...propsCode}
                  value={value}
                  onChangeText={setValue}
                  cellCount={CELL_COUNT}
                  rootStyle={styles.codeFieldRoot}
                  keyboardType="number-pad"
                  textContentType="oneTimeCode"
                  renderCell={({index, symbol, isFocused}) => (
                      <View style={styles.item}>
                      <Text
                          key={String(index)}
                          style={[styles.cell, isFocused && styles.focusCell]}
                          onLayout={getCellOnLayoutHandler(index)}>
                          {symbol || (isFocused ? <Cursor /> : null)}
                      </Text>
                      </View>
                  )}
              />
              </View>
                  <CustomButton load={props.loadProfile} action={() =>  { 
                    if(!props.loadProfile){
                      props.auth(value); setValue('')
                    }
                  }} title={I18n.t('ok')}/>
                  {props.errorPin && <Text style={styles.error}>{I18n.t('checkThePin')}</Text>}
                  <View style={styles.cancleContainer}>
                  <TouchableOpacity style={styles.cancleBt}
                  onPress={() => {
                      setValue('');
                      props.updateStatus(false);
                      props.setErrorPin();
                  }}>
                      <Text style={styles.cancleText}>{I18n.t('cancle')}</Text>
                  </TouchableOpacity>
                  </View>
              </View>
      )
    };

    const renderEstimate = () => {
      return(
        <View>
            <View style={styles.inputContainer}>
                    <TextInput
                        placeholder={'Estimate'}
                        style={styles.input}
                        placeholderTextColor={'#7E8086'}
                        onChangeText={(e) => props.search(e)}
                        autoCapitalize="none"
                        autoCorrect={false} 
                        value={props.searchText}
                    />
                </View>
                <FlatList
                    data={props.estimates}
                    style={{marginBottom: 18, maxHeight: 200}}
                    keyExtractor={(item, index) => String(index)}
                    onEndReachedThreshold={0}
                    onEndReached={data => {
                        if(!props.loadMore){
                          props.loadMoreEstimates();
                        }
                    }}
                    renderItem={data => {
                        let item = data.item;
                        return(
                    <TouchableOpacity onPress={() => props.confirmEstimate(item)}  style={styles.estimateBt}>
                          <Text style={styles.estimate}>{item.service.estimate_number}</Text>
                    </TouchableOpacity>
                        )
                    }}
                />
                {props.loadMore && <ActivityIndicator style={{marginBottom: 15}} size="small" color="#42bec8"/>}
            <CustomButton load={props.loadProfile} action={() =>  { 
                    if(!props.loadProfile){
                      props.saveReport();
                    }
                  }} title={I18n.t('ok')}/>
            <TouchableOpacity style={[styles.cancleBt, {marginTop: 10}]}
                  onPress={() => {
                      props.setEstimateStatus(false);
                      props.saveReport();
                  }}>
                      <Text style={[styles.cancleText, {color: '#42bec8'}]}>Пропустить</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.cancleBt}
                  onPress={() => {
                      props.setEstimateStatus(false);
                  }}>
                      <Text style={styles.cancleText}>{I18n.t('cancle')}</Text>
            </TouchableOpacity>
        </View> 
      )
    }

    return (
      <View>
        <Modal isVisible={props.isVisible}>
        <SafeAreaView style={{ flex: 1 }}>
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <View style={[styles.modalContainer]}>
            {!props.estimateStatus && !props.statusPin ?  <>
                <Text style={styles.user}>{I18n.t('activeProfile')} {props.user && props.user.name ? props.user.name : ''}</Text>
                <View style={styles.changeUserContainer}>
                <TouchableOpacity onPress={() => {props.updateStatus(true)}} style={styles.changeUserBt}>
                  <Text style={styles.changeUser}>{I18n.t('changeTheUser')}</Text>
                </TouchableOpacity>
                </View>
                <View style={{flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between'}}>
                <TouchableOpacity onPress={() => {
                  props.setEstimateStatus(true);
                  props.setAction('out');
                }}
                    style={[styles.modalBt]}>
                        <Image style={styles.iconModal} source={require('../source/img/out.png')}/>
                        <Text style={styles.modalText}>{I18n.t('out')}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                  props.setEstimateStatus(true);
                  props.setAction('in');
                }} 
                  style={styles.modalBt}>
                        <Image style={styles.iconModal} source={require('../source/img/in.png')}/>
                        <Text style={[styles.modalText]}>{I18n.t('in')}</Text>
                </TouchableOpacity>
                </View>
                {props.errorText && <Text style={styles.error}>{props.errorText}</Text>}
                <View style={{paddingTop: 16}}/>
                <CustomButton action={() => props.cancle()} title={I18n.t('cancle')}/>
              </> : props.estimateStatus ? renderEstimate() : renderPin()}
            </View>
          </View>
        </SafeAreaView>
        </Modal>
      </View>
    )
  }

const InOut = (props) => {

    const settings = useSelector(state => state.general.settings);
    const user = useSelector(state => state.user.user);
    let userState = useSelector(state => state.user);
    const [load, setLoad] = useState(false);
    const [loadProfile, setLoadProfile] = useState(false);
    const [photo, setPhoto] = useState(false);
    const [modal, setModal] = useState(false);
    const [errorText, setErrorText] = useState(null);
    const [modalSucess, setModalSucess] = useState(false);
    const [flash, setFlash] = useState(RNCamera.Constants.FlashMode.off);
    const [type, setType] = useState(RNCamera.Constants.Type.front);
    const refCamera = useRef(null);
    const [error, setError] = useState(false);
    const [position, setPosition] = useState(null);
    const [mainUser, setMainUser] = useState(null);
    const [statusPin, setStatusPin] = useState(false);
    const [errorPin, setErrorPin] = useState(false);
    const appState = useRef(AppState.currentState);
    const dispatch = useDispatch();
    const [estimateStatus, setEstimateStatus] = useState(false);
    const [action, setAction] = useState('');
    let [loadMore, setLoadMore] = useState(false);
    let [estimates, setEstimates] = useState([]);
    let [estimateId, setEstimateId] = useState(null);
    let [offset, setOffsete] = useState(0);
    let [total, setTotal] = useState(0);
    let [searchText, setSearchText] = useState('');

    useEffect(() => {
      const unsubscribeInOut = props.navigation.addListener('focus', () => {
        getUser();
      });
      const unsubscribeBlur = props.navigation.addListener('blur', () => {
        console.log('blur');
        setPhoto(false);
        dispatch(updateAdditionalUser(null));
      });
      return unsubscribeInOut, unsubscribeBlur;
    }, [props.navigation]);

    const getEstimates = (data) => {
      SettingsAPI.estimates({offset: 0, limit: 8,  query: data})
      .then(res => {
          setEstimates(res.rows);
          setTotal(res.total);
          console.log(res, 'res');
      })
      .catch(err => {
          console.log(err, 'err');
      });
  };

  const loadMoreEstimates = () => {
      if(estimates.length < total){
          setLoadMore(true);
          SettingsAPI.estimates({offset: offset+8, limit: 8,  query: searchText})
          .then(res => {
              setLoadMore(false);
              setEstimates([...estimates, ...res.rows]);
              setOffsete(offset+8);
              console.log(res, 'loadMoreContacts');
          })
          .catch(err => {
              setLoadMore(false);
              console.log(err, 'err');
          });
      };
  };
      
      const search = (data) => {
          getEstimates(data);
          setSearchText(data);
      };

     const confirmEstimate = (data) => {
          console.log('confirmEstimate => ', data);
          setEstimateId(data.id);
          setSearchText(data.service.estimate_number);
          setEstimates([]);
      }
  
    
    const getUser = async () => {
      let saveUser = await AsyncStorage.getItem('@user');
     // let additionalUser = userState.additionalUser;

      let data = {
        token: JSON.parse(saveUser).accessToken,
        id: user.id,
        name: user.name,
        photo: user.photo
      };

      setMainUser(data);
    
      //let active = additionalUser ? additionalUser : mainUser;
      //setActiveUser(active);
    }
    
    const endSave = () => {
      props.navigation.setParams({item: null});
      setModalSucess(false);
    }
    

    const takePicture = async () => {
        if (refCamera.current) {
          const options = { quality: 0.3, base64: true, width: 800};
          const data = await refCamera.current.takePictureAsync(options);
          setPhoto(data);
        }
      };

      useEffect(() => {
        if (Platform.OS === 'android') {
          locateUser();
          //requestLocationPermission().then(locateUser);
        } else {
          locateUser();
        }
        
      }, []);

      useEffect(() => {
            AppState.addEventListener("change", _handleAppStateChange);
        return () => {
            AppState.removeEventListener("change", _handleAppStateChange);
        };
    }, []);

      const _handleAppStateChange = (nextAppState) => {
        if (
          appState.current.match(/inactive|background/) &&
          nextAppState === "active"
        ) {
          check(Platform.select({
            android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
            ios: PERMISSIONS.IOS.LOCATION_ALWAYS
          })).then(res => {
            if (res === PermissionsAndroid.RESULTS.GRANTED && res === 'granted'){    
              locateUser();
            } else {    
              setError(true);
            }
            })
            .catch(err => {
              setError(true);
            })
        }
        appState.current = nextAppState;
      };

      const requestLocationPermission = async () => {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
              'title': I18n.t('attention'),
              'message': I18n.t('errorGeo')
            }
          )
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            setError(false);
            locateUser();
          } else {
            setError(true);
          }
        } catch (err) {
          setError(true);
        }
      }

      const locateUser = () => {

        let params =  Platform.OS === 'android' ? 
        { enableHighAccuracy: false, timeout: 15000, maximumAge: 10000, forceRequestLocation: true,showLocationDialog: true }
        :
        {enableHighAccuracy: false, timeout: 30000,  maximumAge: 0} 
        ;

        Geolocation.getCurrentPosition(pos => {
          setPosition({
            longitude: pos.coords.longitude,
            latitude: pos.coords.latitude,
          });
          console.log(pos, 'pos');
          setLoad(false);
        },
          (error) => {console.log(error, 'error'), setError(true), setLoad(false);},
          { enableHighAccuracy: false, timeout: 15000, maximumAge: 10000, forceRequestLocation: true,showLocationDialog: true }
        )
      };

     /* const getLocateUser = () => {
        setLoad(true);
        let position = null;
        Geolocation.getCurrentPosition(pos => {
          console.log(pos, 'pos');
          setPosition({
            longitude: pos.coords.longitude,
            latitude: pos.coords.latitude,
          });
          position = {
            longitude: pos.coords.longitude,
            latitude: pos.coords.latitude,
          };
          setLoad(false);
        },
          (error) => {setError(true), setLoad(false);},
          { enableHighAccuracy: false, timeout: 15000, maximumAge: 10000, forceRequestLocation: true, showLocationDialog: true }
        )
      };*/


      const saveReport = async (type) => {

        

        let saveUser = await AsyncStorage.getItem('@user');

        let subdomain = await AsyncStorage.getItem('@subdomain');

       // setLoad(true);

       let reports = await AsyncStorage.getItem('@reports');

           reports = reports ?  JSON.parse(reports) : [];
           

        let activeUser = userState.additionalUser ? userState.additionalUser : mainUser;

        console.log(activeUser, 'activeUser');

        let pin = userState.additionalUser ? {name: 'pin', data: userState.additionalUser.pin} : null;

        let sendStatus = false;

          sendStatus = true;
          setModal(false);
          setLoad(true);

       let domain = 'diga.pt';
        if(subdomain === 'besterp'){
          domain = 'rkesa.pt';
        }

        let mainData = [
          {name: 'type', data: String(type)},
          {name: 'lat', data: settings.location === false ? '' : position ? String(position.latitude) : ''},
          {name: 'lng', data: settings.location === false ? '' : position ? String(position.longitude) : ''},
          {name: 'date_time', data: moment().utc().local().format('YYYY-MM-DD HH:mm')},
          {name: 'service_id', data: settings.service ? String(settings.service.id) : ''},
          {name: 'estimate_id', data: estimateId ? String(estimateId) : ''},
          {name: 'client_contact_id', data: settings.сontact ? String(settings.сontact.id) : ''},
          {name: 'branch_id', data: settings.branch ? String(settings.branch.id) : '',},
          {
            name: 'file',
            data: RNFetchBlob.wrap(photo.uri.replace("file://", "")),
            filename: photo.fileName || photo.uri.substr(photo.uri.lastIndexOf('/')+1),
            type: photo.type,
          }
        ];

        if(pin){
          mainData.push(pin);
        }

        console.log(mainData, 'mainData');

        if(sendStatus === true){
      RNFetchBlob.fetch(
          'POST',
          `https://${subdomain}.${domain}/api/checkin`,
          {
              'Content-Type': 'multipart/form-data',
              Authorization: `Bearer ${mainUser.token}`
          },
          mainData
          ).then((resp) => {

            console.log(resp, 'resp');
            let res = JSON.parse(resp.data);
            if(res.errcode === 0){
              setModalSucess(true);
              saveData(reports);
              dispatch(updateAdditionalUser(null));
              setLoad(false);
              setPhoto(false);
              setErrorText(null);
              setEstimateId(null);
              setSearchText('');
            } else if(res.errcode === 3 || res.errcode === 4) {
              setLoad(false);
              setModal(true);
              setEstimateId(null);
              setSearchText('');
              setErrorText(`${I18n.t('record')} ${I18n.t(res.errcode === 3 ? 'in' : 'out')}`);
            } else {
              Toast.show({
                type: 'error',
                position: 'bottom',
                text1: I18n.t('attention'),
                text2: I18n.t('error'),
                visibilityTime: 4000,
                autoHide: true,
                bottomOffset: 45
              });
              setLoad(false);
              setPhoto(false);
              setErrorText(null);
              setSearchText('');
              setEstimateId(null);
            }
           
          }).catch((err) => {

            Toast.show({
              type: 'error',
              position: 'bottom',
              text1: I18n.t('attention'),
              text2: I18n.t('error'),
              visibilityTime: 4000,
              autoHide: true,
              bottomOffset: 45
            });
            setLoad(false);
            setPhoto(false);
            setEstimateId(null);
            setSearchText('');
          });
          setErrorText(null);
        }

       // await AsyncStorage.setItem('@reports', JSON.stringify(reports));

      }

      const changeUser = () => {
        console.log('changeUser');
      }


      const saveData = async (reports) => {
        await AsyncStorage.setItem('@reports', JSON.stringify(reports));
      }

      const auth = (data) => {
        setLoadProfile(true);
        UserAPI.checkPin({pin: data, mobile: 1})
            .then(res => {
                console.log(res);
                if(res.errcode === 0){
                let access_token = res.access_token;
                dispatch(updateAdditionalUser({
                  name: res.user.name,
                  id: res.user.id,
                  photo: res.user.photo,
                  pin: data
              }));
              setStatusPin(false);
              setLoadProfile(false);
              setErrorPin(false);
              /*setTimeout(() => {
                UserAPI.getAdditionalUser({token: res.access_token})
                  .then(res => {
                    if(res.errcode === 0){
                    // updateUser({
                    //     token: access_token,
                    //     name: resp.name,
                    //     id: resp.id,
                    //     photo: resp.photo
                    // });
                    dispatch(updateAdditionalUser({
                        name: res.user.name,
                        id: res.user.id,
                        photo: res.user.photo,
                        pin: data
                    }));
                    setStatusPin(false);
                    setLoadProfile(false);
                    setErrorPin(false);
                  } 
                })
                .catch(err => {
                  setLoadProfile(false);
                })
                }, 500);*/
                } else {
                  setLoadProfile(false);
                  setErrorPin(true);
                }
            })
            .catch(err => {
                console.log(err, 'err');
                setLoad(false);
                setErrorPin(true);
            })
    }

     

    return(
        <>
        {load && <Preloader/>}
        <ModaldSucces isVisible={modalSucess} 
                    action={(data) => endSave()}
                    />
        <ModalInOut isVisible={modal} 
            errorText={errorText}
            user={userState.additionalUser ? userState.additionalUser : mainUser}
            statusPin={statusPin}
            errorPin={errorPin}
            updateStatus={(status) => setStatusPin(status)}
            changeUser={() => changeUser()}
            auth={(data) => auth(data)}
            loadProfile={loadProfile}
            setErrorPin={() => setErrorPin(false)}
            cancle={() => {setModal(false); setPhoto(null); setErrorText(null);}}
            setAction={setAction}
            setEstimateStatus={setEstimateStatus}
            estimateStatus={estimateStatus}
            action={action}
            estimates={estimates}
            loadMoreEstimates={loadMoreEstimates}
            loadMore={loadMore}
            search={search}
            searchText={searchText}
            confirmEstimate={confirmEstimate}
            saveReport={saveReport}
          />
        <SafeAreaView style={{flex: 0, backgroundColor: '#FEFEFE'}}/>
        <LinearGradient colors={['#FEFEFE', '#F9F9F9']} style={styles.container}>
            <SafeAreaView style={styles.contentContainer}>
            <Header leftIcon='menu' title={I18n.t('InOut')}
                    leftAction={() => props.navigation.openDrawer()}/>
                <View style={styles.camera}>
                    {!photo ? <RNCamera
                        ref={refCamera}
                        style={{flex: 1}}
                        flashMode={flash}
                        type={type}
                    /> : 
                    <Image style={styles.img} source={{uri: photo.uri}}/>}
                    {!photo ?<View style={styles.cameraControl}>
                            <TouchableOpacity style={styles.iconContainer}
                                        activeOpacity={1}
                                            onPress={() => setFlash(flash === RNCamera.Constants.FlashMode.off ? 
                                                RNCamera.Constants.FlashMode.on : RNCamera.Constants.FlashMode.off)}>
                                <Image style={styles.icon} 
                                    source={flash === RNCamera.Constants.FlashMode.off ? 
                                        require('../source/img/flash-off.png') : require('../source/img/flash-on.png')}/>
                            </TouchableOpacity>
                            <TouchableOpacity activeOpacity={1} style={styles.btCamera} 
                                onPress={() => takePicture()} >
                                <View style={styles.cyrcle}/>
                            </TouchableOpacity>
                            <TouchableOpacity  
                                activeOpacity={1}
                                onPress={() => setType(type === RNCamera.Constants.Type.back ? 
                                    RNCamera.Constants.Type.front : RNCamera.Constants.Type.back)} 
                                                style={styles.iconContainer}>
                                <Image style={styles.icon} source={require('../source/img/refresh.png')}/>
                            </TouchableOpacity>
                    </View> : <View style={[styles.cameraControl, {paddingHorizontal: 16, flexDirection: 'column', justifyContent: 'center'}]}>
                                <CustomButton action={() => setModal(true)} title={I18n.t('save')}/>
                                <TouchableOpacity  activeOpacity={1} style={styles.reShoot} onPress={() => setPhoto(false)}>
                                    <Text style={styles.reShootText}>{I18n.t('reShoot')}</Text>
                                </TouchableOpacity>
                                </View>}
                </View>
            </SafeAreaView>
        </LinearGradient>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        width: deviceWidth,
        flex: 1,
        flexDirection:'column', 
        alignItems:'center', 
        justifyContent:'center'
    },
    contentContainer: {
        flex: 1,
        width: '100%',
    },
    camera: {
        backgroundColor: '#fff',
        flex: 1
    },
    cameraControl: {
        backgroundColor: '#13131369',
        height: '30%',
        position: 'absolute',
        width: '100%',
        bottom: 0,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    btCamera: {
        width: 70,
        height: 70,
        borderWidth: 3,
        borderColor: '#fff',
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    cyrcle: {
        width: 55,
        height: 55,
        borderRadius: 50,
        backgroundColor: '#fff'
    },
    icon: {
        width: 28,
        height: 28,
        resizeMode: 'contain'
    },
    iconContainer: {
        padding: 5,
        marginHorizontal: 16
    },
    img: {
        flex: 1,
        resizeMode: 'cover'
    },
    reShoot: {
        marginTop: 16,
        padding: 5
    },
    reShootText: {
        fontSize: 16,
        fontWeight: '600',
        color: '#fff'
    },
    modalContainer: {
        width: deviceWidth-42,
        padding: 16,
        borderRadius: 8,
        backgroundColor: '#fff',
        maxWidth: 450
    },
    modalBt: {
        width: deviceWidth/2-46,
        height: deviceWidth/2-46,
        borderRadius: 8,
        borderWidth: 1.5,
        borderColor: '#e8e8e8',
        alignItems: 'center',
        justifyContent: 'center'
    },
    iconModal: {
        height: 26,
        resizeMode: 'contain'
    },
    modalIcon: {
        height: 75,
        resizeMode: 'contain'
    },
    modalText: {
        fontSize: 18,
        color: '#4d4d4f',
        fontWeight: '600',
        paddingTop: 16,
        width: '100%',
        textAlign: 'center'
    },
    error: {
      color: '#F44336',
      fontSize: 14,
      paddingTop: 15,
      paddingHorizontal: 16,
      textAlign: 'center'
  },
  user: {
      fontSize: 19,
      textAlign: 'center',
      paddingBottom: 5,
      color: '#4d4d4f',
  },
  changeUserContainer: {
    alignItems: 'center',
    marginBottom: 10,
    padding: 5,
  },
  changeUserBt: {
    alignItems: 'center',
    padding: 5,
  },
  changeUser: {
    fontSize: 14,
    textTransform: 'uppercase',
    fontWeight: '600',
    color: '#42bec8',
  },
  modal: {
    //width: deviceWidth-32,
    //paddingHorizontal: 16,
    //paddingVertical: 20,
    backgroundColor: '#fff',
    borderRadius: 15
},
item: {
    borderBottomWidth: 1,
    borderBottomColor: '#7e8086a6',
    alignItems: 'center',
    justifyContent: 'center'
},
cell: {
    width: (deviceWidth-124)/4,
    height: (deviceWidth-124)/4,
    lineHeight: (deviceWidth-124)/4,
    fontSize: 24,
    textAlign: 'center',
    color: '#7E8086'
  },
  focusCell: {

  },
  codeContainer: {
      paddingHorizontal: 16,
      marginBottom: 20
  },
  titleModal: {
      color: '#7E8086',
      textAlign: 'center',
      width: '100%',
      fontSize: 18,
      fontWeight: '600',
      paddingBottom: 10,
      textTransform: 'uppercase'
  },
  title: {
    color: '#7E8086',
    fontSize: 18,
    paddingHorizontal: 16,
    paddingVertical: 10,
    fontWeight: '400'
  },
  cancleContainer: {
      width: '100%',
      alignItems: "center",
      marginTop: 10
  },
  cancleBt: {
      padding: 10,
      justifyContent: 'center',
      alignItems: 'center'
  },
  cancleText: {
    fontSize: 16,
    textTransform: 'uppercase',
    fontWeight: '600',
    color: '#7E8086'
  },
  input: {
    width: '100%',
    height: 50,
    backgroundColor: '#fff',
    borderRadius: 50,
    flexDirection: 'row',
    alignItems: 'center',
    position: 'relative',
    fontWeight: '500',
    shadowColor: "#B9B9BC",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    marginTop: 10,
    paddingHorizontal: 16
  },
  estimateBt: {
    marginHorizontal: 16,
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#7e8086a6',
    paddingHorizontal: 8
  }, 
  estimate: {
    color: '#7E8086'
  }
});


export default InOut;