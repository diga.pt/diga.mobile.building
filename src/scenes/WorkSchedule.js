import React, {useEffect, useState} from 'react';

import LinearGradient from 'react-native-linear-gradient';

import {View, Text, StyleSheet, SafeAreaView, ScrollView,
        Dimensions, Image, FlatList, TextInput, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
import {
    CodeField,
    Cursor,
    useBlurOnFulfill,
    useClearByFocusCell,
  } from 'react-native-confirmation-code-field';
import moment from 'moment';
import Header from '../components/Header';
import CustomButton from '../components/CustomButton';

let deviceWidth = Dimensions.get('window').width;

const CELL_COUNT = 4;

import {UserAPI} from '../api/user';

import Preloader from '../components/Preloader';

import AsyncStorage from '@react-native-community/async-storage';

import I18n from '../i18n/i18n';

import { useDispatch, useSelector } from "react-redux";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import {updateUser} from '../../src/reducers/User';


const WorkSchedule = (props) => {


    const [load, setLoad] = useState(false);
    const [data, setData] = useState([]);

    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

    const [firstDate, setFirstDate] = useState();
    const [secondDate, setSecondDate] = useState();

    const [activePicker, setActivePicker] = useState(null);

    const [totalTime, setTotalTime] = useState(0);
    const settings = useSelector(state => state.general.settings);


    const user = useSelector(state => state.user.user);

    const showDatePicker = (picker) => {
        setActivePicker(picker);
        setDatePickerVisibility(true);
      };
     
      const hideDatePicker = () => {
        setDatePickerVisibility(false);
      };
     
      const handleConfirm = (date) => {
        if(activePicker === 0){
            setFirstDate(moment(date).format("YYYY-MM-DD"));
        } else {
            setSecondDate(moment(date).format("YYYY-MM-DD"));
        }    
        hideDatePicker();
      };

      useEffect(() => {
        getData(props.route.params ? props.route.params.id : null);
        return () => {
        
        }
      }, [props.route.params]);


      useEffect(() => {
       
        let now = moment();
        let monday = now.clone().weekday(1).format("YYYY-MM-DD");
        let today = now.format("YYYY-MM-DD");
        setFirstDate(monday > today ? today : monday);
        setSecondDate(today);
        getData(null);

      }, [])

     
   /* useEffect(() => {
       // getSubdomain();
        //getSaveUsers();
        let now = moment();
        let monday = now.clone().weekday(1).format("YYYY-MM-DD");
        let today = now.format("YYYY-MM-DD");
        setFirstDate(monday > today ? today : monday);
        setSecondDate(today);
        //var friday = now.clone().weekday(5);
       // setData(user.estimate_group_workers);
    }, []);*/



      const getData = (id) => {
        setLoad(true);
        UserAPI.getUser()
        .then(res => {

            let json = res.estimate_group_workers;
            
           if(id){
                json = json.filter(it => it.user_id === id);
            }

            setData(json);

          setLoad(false);
        })
        .catch(err => {
            setLoad(false);
        })
      }



     let total =  data.filter(it => moment(it.date) <= moment(secondDate) && moment(it.date) >= moment(firstDate))
                    .filter(it => it.date_start_before_lunch && it.date_end_before_lunch && 
                            it.date_start_after_lunch && it.date_end_after_lunch)
                    .sort((a, b) => moment(b.date) - moment(a.date))
                    .reduce((a, b) => {
                        let b_time_start_before_lunch = moment(b.date_start_before_lunch);
                        let b_time_end_before_lunch = moment(b.date_end_before_lunch);
                        let b_time_start_after_lunch = moment(b.date_start_after_lunch);
                        let b_time_end_after_lunch = moment(b.date_end_after_lunch);
    
                        let b_firstTime = moment.duration(b_time_end_before_lunch.diff(b_time_start_before_lunch));
                        let b_secondTime = moment.duration(b_time_end_after_lunch.diff(b_time_start_after_lunch));
            
                        let total_time = (b_firstTime.asHours()+b_secondTime.asHours());
                        total_time = total_time > 0 ? total_time : 0;
                        return a+total_time;
                    }, 0);
                

    const renderItem = (data) => {
        let status = data.item.date_start_before_lunch && 
                    data.item.date_end_before_lunch && 
                    data.item.date_start_after_lunch &&
                    data.item.date_end_after_lunch ? true : false;

            let total_time = 0;
            let salary = 0;

            if(status){
                let time_start_before_lunch = moment(data.item.date_start_before_lunch);
                let time_end_before_lunch = moment(data.item.date_end_before_lunch);
    
                let time_start_after_lunch = moment(data.item.date_start_after_lunch);
                let time_end_after_lunch = moment(data.item.date_end_after_lunch);
    
               let firstTime = moment.duration(time_end_before_lunch.diff(time_start_before_lunch));
               let secondTime = moment.duration(time_end_after_lunch.diff(time_start_after_lunch));
            
               total_time = firstTime.asHours()+secondTime.asHours();
               let hours = firstTime.asHours()+secondTime.asHours();
               if(user.salary_type && hours > 0){
                salary = user.salary*hours;
               } else if(hours < 0){
                salary = 0;
                total_time = 0;
               };

            }
            

        return(
                <View  style={styles.itemContainer}>
                    <View style={{flexDirection: 'column', flex: 1}}>
                            <Text style={styles.datetime}>{data.item.date}</Text>
                            <View style={styles.timeContainer}>
                            <Text style={styles.timeTitle}>{I18n.t('startBeforenoon')}</Text>
                                <Text style={styles.time}>{data.item.date_start_before_lunch ? 
                                        data.item.date_start_before_lunch.split(' ')[1] : I18n.t('noData')}</Text>
                            </View>
                            <View style={styles.timeContainer}>
                                <Text style={styles.timeTitle}>{I18n.t('endBeforeNoon')}</Text>
                                <Text style={styles.time}>{data.item.date_end_before_lunch ? 
                                        data.item.date_end_before_lunch.split(' ')[1] : I18n.t('noData')}</Text>
                            </View>
                            <View style={styles.timeContainer}>
                                <Text style={styles.timeTitle}>{I18n.t('startInTheAfternoon')}</Text>
                                <Text style={styles.time}>{data.item.date_start_after_lunch ? 
                                        data.item.date_start_after_lunch.split(' ')[1] : I18n.t('noData')}</Text>
                            </View>
                            <View style={styles.timeContainer}>
                                <Text style={styles.timeTitle}>{I18n.t('endInTheAfternoon')}	</Text>
                                <Text style={styles.time}>{data.item.date_end_after_lunch ? 
                                        data.item.date_end_after_lunch.split(' ')[1] : I18n.t('noData')}</Text>
                            </View>
                            <View style={styles.bottomContainer}>
                                <View style={styles.bottomRow}>
                                    <Text style={styles.bottomTitle}>{I18n.t('spentHours')}</Text>
                                <Text style={styles.bottomText}>{getTimeFromMins(total_time*60)}</Text>
                                </View>
                                {user.salary_type && <View style={styles.bottomRow}>
                                    <Text style={styles.bottomTitle}>{I18n.t('earnedMoney')}</Text>
                                    <Text style={styles.bottomText}>{settings.roundOutTheAmount ? salary.toFixed(2) : Math.round(salary)}€</Text>
                                </View>}
                            </View>
                    </View>
                </View>
        )
    }


    const getTimeFromMins = (mins) => {
        let hours = Math.trunc(mins/60);
        let minutes = mins % 60;
        return hours.toString().padStart(2, '0') + ':' + Math.floor(minutes).toString().padStart(2, '0');
    };
    

    return(
        <>
        {load && <Preloader/>}
        <SafeAreaView style={{flex: 0, backgroundColor: '#FEFEFE'}}/>
        <LinearGradient colors={['#FEFEFE', '#F9F9F9']} style={styles.container}>
            <SafeAreaView style={styles.contentContainer}>
            <Header leftIcon='menu' title={I18n.t('workSchedule')}
                    leftAction={() => props.navigation.openDrawer()}
                    sizeIcon={26}
                />
            <View style={styles.dateHeader}>
            <Text style={styles.dateTitle}>{I18n.t('sortByDate')}</Text>
                <View style={styles.dateContainer}>
                    <TouchableOpacity onPress={() => showDatePicker(0)} style={styles.dateItem}>
                        <Image style={styles.icon} source={require('../source/img/calendar_sort.png')}/>
                        <Text style={styles.date}>{firstDate}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => showDatePicker(1)} style={styles.dateItem}>
                        <Image style={styles.icon} source={require('../source/img/calendar_sort.png')}/>
                        <Text style={styles.date}>{secondDate}</Text>
                    </TouchableOpacity>
                </View>
            </View>

            <DateTimePickerModal
                isVisible={isDatePickerVisible}
                mode="date"
                onConfirm={handleConfirm}
                onCancel={hideDatePicker}
                date={new Date(activePicker === 0 ? firstDate : secondDate)}
            />
            
            <View style={{flex: 1, paddingBottom: 15}}>
                {data  && data.length > 0 && <FlatList
                  data={data.filter(it => moment(it.date) <= moment(secondDate) && moment(it.date) >= moment(firstDate))
                            .sort((a, b) => moment(b.date) - moment(a.date))}
                  renderItem={renderItem}
                  keyExtractor={(item, index) => String(index)}
                />}
                <View style={styles.totalContainer}>
                                <View style={[styles.bottomRow, {paddingRight: 32}]}>
                                    <Text style={styles.bottomTitle}>{I18n.t('totalAmountOfHours')}</Text>
                                <Text style={styles.bottomText}>{getTimeFromMins(total*60)}</Text>
                                </View>
                                <View style={styles.bottomRow}>
                                    <Text style={styles.bottomTitle}>{I18n.t('totalAmountOfMoney')}</Text>
                                    <Text style={styles.bottomText}>{user.salary_type  ? settings.roundOutTheAmount ? (total*user.salary).toFixed(2) : Math.round(total*user.salary) : user.salary} €</Text>
                                </View>
                </View>
            </View>
            </SafeAreaView>
        </LinearGradient>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        width: deviceWidth,
        flex: 1,
        flexDirection:'column', 
        alignItems:'center', 
        justifyContent:'center'
    },
    contentContainer: {
        flex: 1,
        width: '100%',
    },
    itemContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 22,
        paddingVertical: 22,     
        justifyContent: 'space-between',
        backgroundColor: '#FFF',
        borderRadius: 12,
        marginHorizontal: 16,
        marginTop: 10,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    datetime: {
        fontSize: 18,
        fontWeight: '500'
    },
    icon: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        marginRight: 8
    },
    timeContainer: {
        paddingTop: 18,
       // paddingBottom: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        flex: 1,
    },
    timeTitle: {
        fontSize: 15,
        fontWeight: '400',
        color: '#4d4d4f'
    },
    time: {
        fontSize: 16,
        fontWeight: '500',
    },
    bottomContainer:{
         marginTop: 18,
         flexDirection: 'row',
         justifyContent: 'space-between',
         flex: 1,
    },
    bottomTitle: {
        fontSize: 16,
        fontWeight: '400',
        color: '#4d4d4f'
    },
    bottomText: {
        fontSize: 16,
        fontWeight: '600',
        paddingTop: 5,
        color: '#000'
    },
    dateTitle: {
        fontSize: 14,
        fontWeight: '300',
        color: '#4d4d4f'
    },
    dateHeader: {
        paddingHorizontal: 22
    },
    dateContainer: {
        flexDirection: 'row',
        marginTop: 2,
        marginBottom: 8
    },
    dateItem: {
        flex: 1,
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    date: {
        fontSize: 16,
    },
    totalContainer: {
        paddingHorizontal: 32,
        paddingVertical: 10,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    total: {
        fontSize: 16,
        fontWeight: '300',
        color: '#4d4d4f'
    },
    bottomRow: {
        flex: 1
    }
});

export default WorkSchedule;