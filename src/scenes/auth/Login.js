import React, {useEffect, useState} from 'react';

import LinearGradient from 'react-native-linear-gradient';

import {View, Text, StyleSheet, SafeAreaView, ScrollView,
        Dimensions, Image, TouchableOpacity, TextInput, KeyboardAvoidingView, Platform} from 'react-native';

import Preloader from '../../components/Preloader';

import LoginForm from './form/LoginForm';

import {UserAPI} from '../../api/user';

import { useDispatch } from "react-redux";

import {updateUser} from '../../reducers/User';

import AsyncStorage from '@react-native-community/async-storage';

import I18n from '../../i18n/i18n';

import Toast from 'react-native-toast-message';



let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;

const Login = (props) => {
    const dispatch = useDispatch();


    const [load, setLoad] = useState(false);
    const [rememberMe, setRememberMe] = useState(false);

    const [savedData, setSavedData] = useState({});

    useEffect(() => {
        getSavedData();
    }, [])

    const getSavedData = async() => {
        let user = await AsyncStorage.getItem('@rememberUser');
            user = user ? JSON.parse(user) : {};
            setSavedData(user);
    }

    const auth = async (data) => {
        console.log(data);
        setLoad(true);
        await AsyncStorage.setItem('@subdomain', data.subdomain);
        let subdomain = data.subdomain;
        UserAPI.auth({password: data.password, username: data.username})
            .then(res => {
                if(res.status !== 'error'){
                AsyncStorage.setItem('@user', JSON.stringify(res));
                setTimeout(() => {
            UserAPI.getUser()
                .then(rs => {
                    if(rs.status !== 'error'){
                            UserAPI.getUser()
                              .then(res => {
                                saveLogsUser(res, subdomain);
                                saveUser(data);
                                dispatch(updateUser(res));
                              })
                              .catch(err => {
                                Toast.show({
                                    type: 'error',
                                    position: 'bottom',
                                    text1: I18n.t('attention'),
                                    text2: I18n.t('error'),
                                    visibilityTime: 4000,
                                    autoHide: true,
                                    bottomOffset: 45
                                  });
                              })
                          //  dispatch(updateStatusAuth(true));
                         
                    } else {
                        setLoad(false);
                        Toast.show({
                            type: 'error',
                            position: 'bottom',
                            text1: I18n.t('attention'),
                            text2: I18n.t('error'),
                            visibilityTime: 4000,
                            autoHide: true,
                            bottomOffset: 45
                          });
                    }
                  
                })
                .catch(err => {
                    setLoad(false);
                    Toast.show({
                        type: 'error',
                        position: 'bottom',
                        text1: I18n.t('attention'),
                        text2: I18n.t('error'),
                        visibilityTime: 4000,
                        autoHide: true,
                        bottomOffset: 45
                      });
                })
                }, 500);
                props.navigation.navigate('MainNaw');
                setLoad(false);
            } else {
                setLoad(false);
                Toast.show({
                    type: 'error',
                    position: 'bottom',
                    text1: I18n.t('attention'),
                    text2: I18n.t('error'),
                    visibilityTime: 4000,
                    autoHide: true,
                    bottomOffset: 45
                  });
            }})
            .catch(err => {
                setLoad(false);
                console.log(err, 'err');
            })
    }

    const saveUser = async (data) => {
        if(rememberMe){
            await AsyncStorage.setItem('@rememberUser', JSON.stringify(data));
        } else {
            await AsyncStorage.setItem('@rememberUser', JSON.stringify({}));
         }
    }

    const saveLogsUser = async (data, subdomain) => {

        let users = await AsyncStorage.getItem('@users');

            users = users ?  JSON.parse(users) : [];
        
            let index = users.findIndex(it => it.id === data.id);

            if(index === -1){
                users.push({ 
                    id: data.id,
                    name: data.name,
                    photo: data.photo, 
                    subdomain
                });
            } else {
                users[index] = {
                    id: data.id,
                    name: data.name,
                    photo: data.photo,
                    subdomain
                }
            }

            await AsyncStorage.setItem('@users', JSON.stringify(users));
    }

    const status = () => {
        setRememberMe(rememberMe ? false : true);
    }

    return(
        <KeyboardAvoidingView
            style={{flex: 1, backgroundColor: '#FEFEFE'}}
            behavior={Platform.OS === "ios" ? "padding" : null}
            >
        <LinearGradient colors={['#ebecec', '#d8e9ed']} style={styles.container}>
            {load && <Preloader/>}
            <SafeAreaView style={styles.contentContainer}>
            <ScrollView style={{flex: 1, width: '100%'}} showsVerticalScrollIndicator={false}>
            <View style={{flex: 1}}>
            <View style={styles.logoContainer}>
                <Image source={require('../../source/img/logo.png')} style={styles.logo}/>
            </View>
            <LoginForm navigation={props.navigation} 
                        auth={auth}
                        rememberMe={rememberMe} 
                        action={status}
                        initialValues={savedData}
                        />
            <View style={styles.signUpContainer}>
            <Text style={[styles.text]}>
                    {I18n.t('haveNoAcoount')}
            </Text>
            <TouchableOpacity onPress={() => props.navigation.navigate('SignUp')} style={{padding: 6}}>
                    <Text style={[styles.text, {color: '#42bec8'}]}>{I18n.t('signUp')}</Text>
            </TouchableOpacity>
            </View>
            </View>
            </ScrollView>
            </SafeAreaView>
        </LinearGradient>
        </KeyboardAvoidingView>
    )
}

const styles = StyleSheet.create({
    container: {
        width: deviceWidth,
        flex: 1,
        flexDirection:'column', 
        alignItems:'center', 
        justifyContent:'center'
    },
    logo: {
        width: 180,
        height: 90,
        resizeMode: 'contain'
    },
    contentContainer: {
        flex: 1,
        width: deviceWidth-32,
        alignItems:'center', 
    },
    logoContainer: {
        paddingVertical: '20%',
        alignItems:'center', 
        justifyContent:'center'
    },
    
    signUpContainer: {
        alignItems: 'center', 
        justifyContent: 'center', 
        flexDirection: 'row',
        paddingVertical: 16,
    },
    text: {
        fontSize: 14, 
        fontWeight: '500',
        color: '#7E8086'
    }
});

export default Login;