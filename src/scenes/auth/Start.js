import React, {useEffect, useState} from 'react';

import LinearGradient from 'react-native-linear-gradient';

import {View, Text, StyleSheet, SafeAreaView,
        Dimensions, Image, TouchableOpacity} from 'react-native';

let deviceWidth = Dimensions.get('window').width;
import Preloader from '../../components/Preloader';
import I18n from '../../i18n/i18n';
import AsyncStorage from '@react-native-community/async-storage';
import Auth0 from 'react-native-auth0';
import {UserAPI} from '../../api/user';
import Toast from 'react-native-toast-message';
import { useDispatch } from "react-redux";
import {updateUser, updateStatusAuth} from '../../reducers/User';

const auth0 = new Auth0({
    domain: 'diga.eu.auth0.com',
    clientId: 'In9799EXJYQi4UqZSVS6T4r49d2LDdYg'
  });

const Start = ({navigation}) => {

    const [language, setLanguage] = useState('');
    const [authToken, setAuthToken] = useState(null);
    const [load, setLoad] = useState(false);
    const dispatch = useDispatch();

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            getStartData();
        });
        return unsubscribe;
      }, [navigation]);

    const  getStartData = async () => {
        let language = await AsyncStorage.getItem('@language');
            language = language ? JSON.parse(language) : {name: 'English', id: 'en'};
         setLanguage(language);
     }

    const handleLoginPress = async () => {
        setLoad(true);
        
    try {
      let credentials = await auth0
        .webAuth
        .authorize({ scope: 'openid profile email', audience:'https://diga.pt'});
        AsyncStorage.setItem('@user', JSON.stringify(credentials));
        setTimeout(() => {
            getUserProfile(credentials.accessToken);
        });
    } catch (error) {
      console.log(error);
      setLoad(false);
      Toast.show({
        type: 'error',
        position: 'bottom',
        text1: I18n.t('attention'),
        text2: I18n.t('error'),
        visibilityTime: 4000,
        autoHide: true,
        bottomOffset: 45
      });
    }
  }


  const saveLogsUser = async (data, subdomain) => {

    let users = await AsyncStorage.getItem('@users');

        users = users ?  JSON.parse(users) : [];
    
        let index = users.findIndex(it => it.id === data.id);

        if(index === -1){
            users.push({ 
                id: data.id,
                name: data.name,
                photo: data.photo, 
                subdomain
            });
        } else {
            users[index] = {
                id: data.id,
                name: data.name,
                photo: data.photo,
                subdomain
            }
        }

        await AsyncStorage.setItem('@users', JSON.stringify(users));
}

  const getUserProfile = async (authToken) => {
    auth0.auth
      .userInfo({ token: authToken })
      .then(result => {
          console.log('user profile', result);
          UserAPI.getSubdomain({data: result.sub})
            .then(async res => {
                console.log(res);
                let subdomain = res[0].name.split('.')[0];
                await AsyncStorage.setItem('@subdomain', subdomain);
                getUser(authToken);
            })
            .catch(err => {
                console.log(err, 'err');
                setLoad(false);
      Toast.show({
        type: 'error',
        position: 'bottom',
        text1: I18n.t('attention'),
        text2: I18n.t('error'),
        visibilityTime: 4000,
        autoHide: true,
        bottomOffset: 45
      });
            })
      })
      .catch(console.error);
  }

  const getUser = (subdomain) => {
   
    UserAPI.getUser()
      .then(res => {
          console.log(res, 'res');
        saveLogsUser(res, subdomain);
        dispatch(updateUser(res));
        dispatch(updateStatusAuth(true));
        setLoad(false);

      })
      .catch(err => {
        console.log(err, 'err');
        setLoad(false);
      Toast.show({
        type: 'error',
        position: 'bottom',
        text1: I18n.t('attention'),
        text2: I18n.t('error'),
        visibilityTime: 4000,
        autoHide: true,
        bottomOffset: 45
      });
      })
}

    return(
        <LinearGradient colors={['#ebecec', '#d8e9ed']} style={styles.container}>
            {load && <Preloader/>}
             <TouchableOpacity onPress={() => navigation.navigate('Languages', {id: language.id})} style={styles.languageBt}>
                    <Image style={styles.languageIcon} source={require('../../source/img/language.png')}/>
                    <Text style={styles.language}>{language.name}</Text>
            </TouchableOpacity>
            <View style={styles.row}>
                <Image source={require('../../source/img/logo.png')} style={styles.logo}/>
                <Text style={styles.title}>ERP/CRM{`\n`}Diga.pt</Text>
                <TouchableOpacity onPress={() => handleLoginPress()/*navigation.navigate('Login')*/} style={styles.bt}>
                    <Text style={styles.btText}>{I18n.t('login')}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigation.navigate('SignUp')} style={[styles.bt, {backgroundColor: '#fff'}]}>
                    <Text style={[styles.btText, {color: '#4d4d4f'}]}>{I18n.t('signUp')}</Text>
                </TouchableOpacity>
            </View>
        </LinearGradient>
    )
}

const styles = StyleSheet.create({
    container: {
        width: deviceWidth,
        flex: 1,
        flexDirection:'row', 
        alignItems:'center', 
        justifyContent:'center'
    },
    logo: {
        width: 250,
        height: 115,
        resizeMode: 'contain'
    },
    row: {
        paddingHorizontal: 32,
        width: '100%',
        justifyContent:'center',
        alignItems: 'center'
    },
    title: {
        textAlign: 'center',
        color: '#4d4d4f',
        fontWeight: '700',
        paddingTop: 10,
        paddingBottom: 110,
        fontSize: 22,
    },
    bt: {
        width: '100%',
        height: 50,
        backgroundColor: '#42bec8',
        marginBottom: 15,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    btText: {
        color: '#fff',
        fontSize: 18,
        fontWeight: '500'
    },
    languageBt: {
        position: 'absolute',
        right: 20,
        top: 45,
        alignItems: 'center'
    },
    languageIcon: {
        width: 28,
        height: 28
    },
    language: {
        fontSize: 14,
        paddingTop: 2,
        fontWeight: '500'
    }
});

export default Start;