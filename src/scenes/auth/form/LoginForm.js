
import React, {useState, useEffect} from 'react';

import {View, Text, SafeAreaView, Keyboard, Image, StyleSheet, TouchableOpacity,
    TextInput} from 'react-native';

import { renderInput, renderPasswordInput} from '../../../components/form';
import { reduxForm, Field, } from 'redux-form';
import {required, number, email} from '../../../components/form/validator';

import CustomButton from '../../../components/CustomButton';

import I18n from '../../../i18n/i18n';
import CheckBox from 'react-native-check-box'


const renderCheckBox = (props) => {
    console.log(props.rememberMe);
    return (
        <CheckBox
                style={{paddingBottom: 20, paddingHorizontal: 16}}
                onClick={props.action}
                isChecked={props.rememberMe}
                rightText={I18n.t('rememberMe')}
                rightTextStyle={{
                    fontSize: 15,
                    color: '#7E8086',
                    fontWeight: '500'
                }}
                checkBoxColor='#7E8086'
                checkedCheckBoxColor='#42bec8'
            />
    )
}


const LoginForm = (props) => {
    const { handleSubmit } = props;


    

    return(
        <>
        <View style={styles.formContainer}>
              
                <Field  name={`subdomain`}
                        component={renderInput}
                        placeholder={I18n.t('subdomain')}
                        validate={[required]}
                        subdomain={true}
                        type={'default'}
                        />
                <Field  name={`username`}
                        component={renderInput}
                        placeholder={I18n.t('login')}
                        validate={[required, email]}
                        type={'email-address'}
                        />
                <Field  name={`password`}
                        component={renderPasswordInput}
                        placeholder={I18n.t('password')}
                        validate={[required]}
                        type={'default'}
                        secureTextEntry={true}
                        />
                <TouchableOpacity onPress={() => props.navigation.navigate('ForgotPassword')} style={styles.forgot}>
                    <Text style={styles.text}>{I18n.t('forgot')}</Text>
                </TouchableOpacity>
                <View style={{width: '100%'}}>
                    {renderCheckBox(props)}
                </View>
            </View>
            <CustomButton action={handleSubmit((data) => props.auth(data))} title={I18n.t('signIn')}/>
            </>
    )
}

const styles = StyleSheet.create({
    formContainer: {
        height: 'auto',
        width: '100%',
        marginTop: 15,
        alignItems: 'flex-end'
    },
    input: {
        flex: 1,
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 50,
        paddingHorizontal: 16,
        fontSize: 15
    },
    inputContainer: {
        width: '100%',
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 50,
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 15,
        position: 'relative',
        fontWeight: '500'
    },
    subdomain: {
        paddingRight: 16,
        paddingLeft: 8,
        fontSize: 15,
        fontWeight: '500',
        color: '#42bec8'
    },
    text: {
        fontSize: 14, 
        fontWeight: '500',
        color: '#7E8086'
    },
    forgot: {
        paddingVertical: 5,
        marginBottom: 10,
        paddingHorizontal: 5
    }
})


export default reduxForm({
    form: 'LoginForm',
    enableReinitialize: true
  })(LoginForm);