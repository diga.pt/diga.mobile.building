
import React, {useState, useEffect} from 'react';

import {View, Text, SafeAreaView, Keyboard, Image, StyleSheet, TouchableOpacity,
    TextInput} from 'react-native';

import { renderInput, renderSelect, renderPasswordInput} from '../../../components/form';
import { reduxForm, Field, } from 'redux-form';
import { required, email, numericality } from 'redux-form-validators';

import CustomButton from '../../../components/CustomButton';


import I18n from '../../../i18n/i18n';


const SignUpForm = (props) => {
    const { handleSubmit, signUp } = props;


    return(
        <>
        <View style={styles.formContainer}>
                <Field  name={`name`}
                        component={renderInput}
                        placeholder={I18n.t('name')}
                        validate={[required()]}
                        type={'default'}
                        />
                <Field  name={`email`}
                        component={renderInput}
                        placeholder={I18n.t('email')}
                        validate={[required(), email()]}
                        type={'email-address'}
                        />
                <Field  name={`telephone`}
                        component={renderInput}
                        placeholder={I18n.t('phone')}
                        validate={[required()]}
                        type={'phone-pad'}
                        />
                <Field  name={`password`}
                        component={renderPasswordInput}
                        placeholder={I18n.t('password')}
                        validate={[required()]}
                        type={'default'}
                        secureTextEntry={true}
                        />
                <Field  name={`domain`}
                        component={renderInput}
                        placeholder={I18n.t('subdomain')}
                        validate={[required()]}
                        subdomain={true}
                        type={'default'}
                        
                        />
                <Field  name={`language`}
                        component={renderSelect}
                        placeholder={I18n.t('language')}
                        items={[
                            { label: 'Português', value: 'pt', key: 'pt'},
                            { label: 'Русский', value: 'ru', key: 'ru' },
                            { label: 'English', value: 'en', key: 'en' },
                            { label: 'Español', value: 'es', key: 'es' },
                        ]}
                        validate={[required()]}
                        type={'default'}
                        />
            </View>
            <CustomButton action={handleSubmit(signUp)} title={I18n.t('signUp')}/>
            </>
    )
}

const styles = StyleSheet.create({
    formContainer: {
        height: 'auto',
        width: '100%',
        marginTop: 15,
        alignItems: 'flex-end'
    },
    input: {
        flex: 1,
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 50,
        paddingHorizontal: 16,
        fontSize: 15
    },
    inputContainer: {
        width: '100%',
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 50,
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 15,
        position: 'relative',
        fontWeight: '500'
    },
    subdomain: {
        paddingRight: 16,
        paddingLeft: 8,
        fontSize: 15,
        fontWeight: '500',
        color: '#42bec8'
    },
    text: {
        fontSize: 14, 
        fontWeight: '500',
        color: '#7E8086'
    },
    forgot: {
        paddingVertical: 5,
        marginBottom: 20,
        paddingHorizontal: 5
    }
})


export default reduxForm({
    form: 'SignUpForm',
    enableReinitialize: true
  })(SignUpForm);