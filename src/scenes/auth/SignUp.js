import React, {useState} from 'react';

import LinearGradient from 'react-native-linear-gradient';

import {View, Text, StyleSheet, SafeAreaView, ScrollView,
        Dimensions, Image, TouchableOpacity, TextInput, KeyboardAvoidingView, Platform} from 'react-native';

import Preloader from '../../components/Preloader';
import {UserAPI} from '../../api/user';
import SignUpForm from './form/SignUpForm';

let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;
import CustomButton from '../../components/CustomButton';

import Modal from 'react-native-modal';
import Toast from 'react-native-toast-message';

import I18n from '../../i18n/i18n';


const ModaldSucces = (props) => {
    return(
        <Modal isVisible={props.isVisible}>
        <SafeAreaView style={{ flex: 1 }}>
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <View style={[styles.modalContainer]}>
                <View style={{flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center'}}>
                    <Image style={styles.modalIcon} source={require('../../source/img/checked.png')}/>
                    <Text style={styles.modalInfo}>{I18n.t('accountOnCheck')}</Text>
                </View>
                <View style={{paddingTop: 16}}/>
                <CustomButton action={() => props.action()} title={'OK'}/>
            </View>
          </View>
        </SafeAreaView>
        </Modal>
    )
}




const SignUp = (props) => {

    const [load, setLoad] = useState(false);
    const [modal, setModal] = useState(false);

    const signUp = (data) => {
        console.log(data);
        setLoad(true);
        let params = data;
        UserAPI.checkSubdomain({per_page: 100, type: 'A', name: `${data.domain}.diga.pt`})
            .then(res => {
                if(res.result.length === 0){
                    console.log(params, 'true');
                    UserAPI.signUp({...params, packetToken: '925c1226-f51e-40c8-bcc6-dba99ce81f79'})
                    .then(res => {
                        if(res.status === 'error'){
                            Toast.show({
                                type: 'error',
                                position: 'top',
                                text1: I18n.t('attention'),
                                text2: I18n.t('error'),
                                visibilityTime: 4000,
                                autoHide: true,
                                topOffset: 45
                              });
                        } else {
                            setModal(true);
                        }
                        setLoad(false);
                        
                        console.log(res, 'signUp');
                    })
                    .catch(err => {
                        setLoad(false);
                        Toast.show({
                            type: 'error',
                            position: 'top',
                            text1: I18n.t('attention'),
                            text2: I18n.t('error'),
                            visibilityTime: 4000,
                            autoHide: true,
                            topOffset: 45
                          });
                        console.log(err, 'err');
                    })
                } else {
                    Toast.show({
                        type: 'error',
                        position: 'top',
                        text1: I18n.t('attention'),
                        text2: I18n.t('errorSubdomain'),
                        visibilityTime: 4000,
                        autoHide: true,
                        topOffset: 45
                      });
                      setLoad(false);
                }
                console.log(res);
            })
            .catch(err => {
                setLoad(false);
                Toast.show({
                    type: 'error',
                    position: 'top',
                    text1: I18n.t('attention'),
                    text2: I18n.t('errorSubdomain'),
                    visibilityTime: 4000,
                    autoHide: true,
                    topOffset: 45
                  });
                console.log(err, 'err');
            })
    }

    return(
        <KeyboardAvoidingView
            style={{flex: 1, backgroundColor: '#FEFEFE'}}
            behavior={Platform.OS === "ios" ? "padding" : null}
            >
        <LinearGradient colors={['#ebecec', '#d8e9ed']} style={styles.container}>
            {load && <Preloader/>}
            <ModaldSucces isVisible={modal} 
                action={() => {
                    setModal(false);
                    props.navigation.navigate('AuthStart');
                }}
            />
            <SafeAreaView style={styles.contentContainer}>
            <ScrollView style={{flex: 1, width: '100%'}} showsVerticalScrollIndicator={false}>
            <View style={{flex: 1}}>
            <View style={styles.logoContainer}>
                <Image source={require('../../source/img/logo.png')} style={styles.logo}/>
            </View>
            <View style={styles.formContainer}>
                <SignUpForm signUp={signUp}/>
            </View>

            <View style={styles.signUpContainer}>
            <Text style={[styles.text]}>
                {I18n.t('alreadyHaveAnAccount')}
            </Text>
            <TouchableOpacity onPress={() => props.navigation.navigate('Login')} style={{padding: 6}}>
                    <Text style={[styles.text, {color: '#42bec8'}]}>{I18n.t('signIn')}</Text>
            </TouchableOpacity>
            </View>
            </View>
            </ScrollView>
            </SafeAreaView>
        </LinearGradient>
        </KeyboardAvoidingView>
    )
}


const styles = StyleSheet.create({
    container: {
        width: deviceWidth,
        flex: 1,
        flexDirection:'column', 
        alignItems:'center', 
        justifyContent:'center'
    },
    logo: {
        width: 180,
        height: 90,
        resizeMode: 'contain'
    },
    formContainer: {
        height: 'auto',
        width: '100%',
        marginTop: 15,
        alignItems: 'flex-end'
    },
    contentContainer: {
        flex: 1,
        width: deviceWidth-32,
        alignItems:'center', 
    },
    input: {
        flex: 1,
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 50,
        paddingHorizontal: 16,
        fontSize: 15
    },
    inputContainer: {
        width: '100%',
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 50,
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 15,
        position: 'relative',
        fontWeight: '500'
    },
    logoContainer: {
        paddingVertical: '10%',
        alignItems:'center', 
        justifyContent:'center'
    },
    subdomain: {
        paddingRight: 16,
        paddingLeft: 8,
        fontSize: 15,
        fontWeight: '500',
        color: '#42bec8'
    },
    text: {
        fontSize: 14, 
        fontWeight: '500',
        color: '#7E8086'
    },
    signUpContainer: {
        alignItems: 'center', 
        justifyContent: 'center', 
        flexDirection: 'row',
        paddingVertical: 16,
    },
    forgot: {
        paddingVertical: 5,
        marginBottom: 20,
        paddingHorizontal: 5
    },
    modalContainer: {
        width: deviceWidth-42,
        padding: 16,
        borderRadius: 8,
        backgroundColor: '#fff',
        maxWidth: 450
    },
    modalIcon: { 
        width: 86, 
        height: 86,
        marginVertical: 25
    },
    modalInfo: {
        fontSize: 17,
        textAlign: 'center',
        paddingBottom: 15,
        fontWeight: '500',
        color: '#7E8086',
        paddingHorizontal: 16,
        width: '100%'
    }
});


export default SignUp;