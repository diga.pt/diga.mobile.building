import React, {useEffect, useState} from 'react';

import LinearGradient from 'react-native-linear-gradient';

import {View, Text, StyleSheet, SafeAreaView, ScrollView,
        Dimensions, Image, TouchableOpacity, TextInput, Linking} from 'react-native';

import Header from '../components/Header';

import RNPickerSelect from 'react-native-picker-select';

import { Switch } from 'react-native-switch';

let deviceWidth = Dimensions.get('window').width;

import AsyncStorage from '@react-native-community/async-storage';

import { useDispatch, useSelector } from "react-redux";
import {updateStatusAuth, updateUser} from '../reducers/User';
import {updateSettings} from '../reducers/General';
import Auth0 from 'react-native-auth0';

const auth0 = new Auth0({
    domain: 'diga.eu.auth0.com',
    clientId: 'In9799EXJYQi4UqZSVS6T4r49d2LDdYg'
  });

import I18n from '../i18n/i18n';


const Settings = (props) => {

    const dispatch = useDispatch();

    const settings = useSelector(state => state.general.settings);
    const user = useSelector(state => state.user.user);

    const [subdomain, setSubdomai] = useState('');
    const [language, setLanguage] = useState('');

    /*useEffect(() => {
        getStartData();
    }, []);*/

    useEffect(() => {
        const unsubscribe = props.navigation.addListener('focus', () => {
            getStartData();
        });
        return unsubscribe;
      }, [props.navigation]);


    const logOut =  async () => {
      auth0.webAuth
       .clearSession({ scope: 'openid profile email', audience:'https://diga.pt'})
       .then(success => {
            dispatch(updateStatusAuth('auth'));
            AsyncStorage.removeItem('@user');
            AsyncStorage.removeItem('@users');
            AsyncStorage.removeItem('@reports');
       })
       .catch(error => {
           console.log('Log out cancelled');
       });
        
    }


    const settingLocation = async (data) => {
        console.log(data);
        let settings = await AsyncStorage.getItem('@settings');
            settings = settings === null ? {} : JSON.parse(settings);
       
                if(settings === null){ 
                    settings['location'] = data;
                } else {
                    settings = {
                        ...settings,
                        location: data
                    }
                
                await AsyncStorage.setItem(`@settings`, JSON.stringify(settings));
                dispatch(updateSettings(settings));
            }
        
    }

    const settingSwitch = async (data, type) => {
        let settings = await AsyncStorage.getItem('@settings');
            settings = settings === null ? {} : JSON.parse(settings);
       
                if(settings === null){ 
                    settings[type] = data;
                } else {
                    settings = {
                        ...settings,
                        [type]: data
                    }
                
                await AsyncStorage.setItem(`@settings`, JSON.stringify(settings));
                dispatch(updateSettings(settings));
            }
        
    }

    const  getStartData = async () => {
       let subdomain = await AsyncStorage.getItem('@subdomain');
       let language = await AsyncStorage.getItem('@language');
           language = language ? JSON.parse(language) : {name: 'English', id: 'en'};
        setSubdomai(subdomain);
        setLanguage(language);
    }

    console.log(user, 'user');

    
    return(
        <>
        <SafeAreaView style={{flex: 0, backgroundColor: '#FEFEFE'}}/>
        <LinearGradient colors={['#FEFEFE', '#F9F9F9']} style={styles.container}>
            <SafeAreaView style={styles.contentContainer}>
            <Header leftIcon='menu' title={I18n.t('settings')}
                    leftAction={() => props.navigation.openDrawer()}/>
            <ScrollView style={{flex: 1, width: '100%'}} showsVerticalScrollIndicator={false}>
            <View style={{flex: 1, paddingHorizontal: 16}}>
   
            <View style={{}}>
            <Text style={styles.label}>{I18n.t('subdomain')}</Text>
            <View style={styles.inputWrapper}>
                <View style={styles.inputContainer}>
                    <TextInput
                        placeholder={I18n.t('subdomain')}
                        style={styles.input}
                        placeholderTextColor={'#7E8086'}
                        editable={false}
                        value={subdomain}
                    />
                </View>
            </View>
            </View>
            <View style={{}}>
            <Text style={styles.label}>{I18n.t('language')}</Text>
            <View style={styles.inputWrapper}>
            <TouchableOpacity style={styles.inputContainer}
                    onPress={() => props.navigation.navigate('Languages', {id: language.id})}>
                    <TextInput
                        placeholder={I18n.t('language')}
                        style={styles.input}
                        placeholderTextColor={'#7E8086'}
                        editable={false}
                        onTouchStart={() => props.navigation.navigate('Languages', {id: language.id})}
                        value={language.name}
                    />
                </TouchableOpacity>
            </View>
            </View>
           {/** <View style={{}}>
            <Text style={styles.label}>{I18n.t('Service')}</Text>
            <View style={styles.inputWrapper}>
                <TouchableOpacity style={styles.inputContainer}
                    onPress={() => props.navigation.navigate('Search', {name: 'Service', title: I18n.t('Service')})}>
                    <TextInput
                        placeholder={I18n.t('Service')}
                        style={styles.input}
                        placeholderTextColor={'#7E8086'}
                        onTouchStart={() => props.navigation.navigate('Search', {name: 'Service', title: I18n.t('Service')})}
                        editable={false}
                        navigation={props.navigation}
                        value={settings.service ? settings.service.name : ''}
                    />
                </TouchableOpacity>
            </View>
            </View>
            <View style={{}}>
            <Text style={styles.label}>{I18n.t('Estimate')}</Text>
            <View style={styles.inputWrapper}>
                <TouchableOpacity style={styles.inputContainer}
                    onPress={() => props.navigation.navigate('Search', {name: 'Estimate', title: I18n.t('Estimate')})}>
                    <TextInput
                        placeholder={I18n.t('Estimate')}
                        style={styles.input}
                        placeholderTextColor={'#7E8086'}
                        onTouchStart={() => props.navigation.navigate('Search', {name: 'Estimate', title: I18n.t('Estimate')})}
                        editable={false}
                        navigation={props.navigation}
                        value={settings.estimate ? settings.estimate.name : ''}
                    />
                </TouchableOpacity>
            </View>
            </View>
            <View style={{}}>
            <Text style={styles.label}>{I18n.t('Contact')}</Text>
            <View style={styles.inputWrapper}>
                <TouchableOpacity style={styles.inputContainer}
                    onPress={() => props.navigation.navigate('Search', {name: 'Contact', title: I18n.t('Contact')})}>
                    <TextInput
                        placeholder={I18n.t('Contact')}
                        style={styles.input}
                        placeholderTextColor={'#7E8086'}
                        onTouchStart={() => props.navigation.navigate('Search', {name: 'Сontact', title: I18n.t('Contact')})}
                        editable={false}
                        navigation={props.navigation}
                        value={settings.сontact ? settings.сontact.name : ''}
                    />
                </TouchableOpacity>
            </View>
            </View>
            <View style={{}}>
            <Text style={styles.label}>{I18n.t('branch')}</Text>
            <View style={styles.inputWrapper}>
                <TouchableOpacity style={styles.inputContainer}
                    onPress={() => props.navigation.navigate('Branch')}>
                <TextInput
                        placeholder={I18n.t('branch')}
                        style={styles.input}
                        placeholderTextColor={'#7E8086'}
                        onTouchStart={() => props.navigation.navigate('Branch')}
                        editable={false}
                        navigation={props.navigation}
                        value={settings.branch ? settings.branch.name : ''}
                    />
                </TouchableOpacity>
            </View>
            </View> */}
            
            <View style={{paddingHorizontal: 16, marginTop: 15}}>
            <TouchableOpacity onPress={() => props.navigation.navigate('AddUser')} style={styles.btContainer}>
                <Text style={styles.btText}>{I18n.t('addUser')}</Text>
                <Image style={styles.icon} source={require('../source/img/plus.png')}/>
            </TouchableOpacity> 
            <TouchableOpacity onPress={() => Linking.openURL('mailto:geral@diga.pt')} style={styles.btContainer}>
                <Text style={styles.btText}>{I18n.t('contactSupport')}</Text>
                <Image style={styles.icon} source={require('../source/img/email.png')}/>
            </TouchableOpacity> 
            <View style={styles.btContainer}>
                <Text style={styles.btText}>{I18n.t('accessToLocation')}</Text>
                <Switch
                    value={settings.location ? settings.location : false}
                    onValueChange={(val) => {
                        settingSwitch(val, 'location');
                    }}
                    disabled={false}
                    activeText={'On'}
                    inActiveText={'Off'}
                    circleSize={16}
                    barHeight={24}
                    circleBorderWidth={0}
                    backgroundActive={'#b5b5b557'}
                    backgroundInactive={'#b5b5b557'}
                    circleActiveColor={'#42bec8'}
                    circleInActiveColor={'#42bec8'}
                    changeValueImmediately={true}
                    changeValueImmediately={true} // if rendering inside circle, change state immediately or wait for animation to complete
                    innerCircleStyle={{ alignItems: "center", justifyContent: "center" }} // style for inner animated circle for what you (may) be rendering inside the circle
                    outerCircleStyle={{}} // style for outer animated circle
                    renderActiveText={false}
                    renderInActiveText={false}
                    switchLeftPx={-2} // denominator for logic when sliding to TRUE position. Higher number = more space from RIGHT of the circle to END of the slider
                    switchRightPx={-2} // denominator for logic when sliding to FALSE position. Higher number = more space from LEFT of the circle to BEGINNING of the slider
                    switchWidthMultiplier={3} // multipled by the `circleSize` prop to calculate total width of the Switch
                    switchBorderRadius={30} // Sets the border Radius of the switch slider. If unset, it remains the circleSize.
                />
            </View> 
            <View style={styles.btContainer}>
                <Text style={styles.btText}>{I18n.t('roundOutTheAmount')}</Text>
                <Switch
                    value={settings.roundOutTheAmount ? settings.roundOutTheAmount : false}
                    onValueChange={(val) => {
                        settingSwitch(val, 'roundOutTheAmount');
                    }}
                    disabled={false}
                    activeText={'On'}
                    inActiveText={'Off'}
                    circleSize={16}
                    barHeight={24}
                    circleBorderWidth={0}
                    backgroundActive={'#b5b5b557'}
                    backgroundInactive={'#b5b5b557'}
                    circleActiveColor={'#42bec8'}
                    circleInActiveColor={'#42bec8'}
                    changeValueImmediately={true}
                    changeValueImmediately={true} // if rendering inside circle, change state immediately or wait for animation to complete
                    innerCircleStyle={{ alignItems: "center", justifyContent: "center" }} // style for inner animated circle for what you (may) be rendering inside the circle
                    outerCircleStyle={{}} // style for outer animated circle
                    renderActiveText={false}
                    renderInActiveText={false}
                    switchLeftPx={-2} // denominator for logic when sliding to TRUE position. Higher number = more space from RIGHT of the circle to END of the slider
                    switchRightPx={-2} // denominator for logic when sliding to FALSE position. Higher number = more space from LEFT of the circle to BEGINNING of the slider
                    switchWidthMultiplier={3} // multipled by the `circleSize` prop to calculate total width of the Switch
                    switchBorderRadius={30} // Sets the border Radius of the switch slider. If unset, it remains the circleSize.
                />
            </View> 
           {user.is_admin && <View style={styles.btContainer}>
                <Text style={styles.btText}>Доступ к камере</Text>
                <Switch
                    value={settings.cameraPermission ? settings.cameraPermission : false}
                    onValueChange={(val) => {
                        settingSwitch(val, 'cameraPermission');
                    }}
                    disabled={false}
                    activeText={'On'}
                    inActiveText={'Off'}
                    circleSize={16}
                    barHeight={24}
                    circleBorderWidth={0}
                    backgroundActive={'#b5b5b557'}
                    backgroundInactive={'#b5b5b557'}
                    circleActiveColor={'#42bec8'}
                    circleInActiveColor={'#42bec8'}
                    changeValueImmediately={true}
                    changeValueImmediately={true} // if rendering inside circle, change state immediately or wait for animation to complete
                    innerCircleStyle={{ alignItems: "center", justifyContent: "center" }} // style for inner animated circle for what you (may) be rendering inside the circle
                    outerCircleStyle={{}} // style for outer animated circle
                    renderActiveText={false}
                    renderInActiveText={false}
                    switchLeftPx={-2} // denominator for logic when sliding to TRUE position. Higher number = more space from RIGHT of the circle to END of the slider
                    switchRightPx={-2} // denominator for logic when sliding to FALSE position. Higher number = more space from LEFT of the circle to BEGINNING of the slider
                    switchWidthMultiplier={3} // multipled by the `circleSize` prop to calculate total width of the Switch
                    switchBorderRadius={30} // Sets the border Radius of the switch slider. If unset, it remains the circleSize.
                />
            </View>}
            <View style={styles.bottomContainer}>
                    <TouchableOpacity onPress={() => logOut()} style={styles.logOutContainer}>
                        <Image style={styles.icon} source={require('../source/img/logout.png')}/>
                        <Text style={styles.logOut}>{I18n.t('logOut')}</Text>
                    </TouchableOpacity>
                    <Text style={styles.version}>V.1.0</Text>
            </View>
            </View>
            </View>
            </ScrollView>
                
            </SafeAreaView>
        </LinearGradient>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        width: deviceWidth,
        flex: 1,
        flexDirection:'column', 
        alignItems:'center', 
        justifyContent:'center'
    },
    contentContainer: {
        flex: 1,
        width: '100%',
    },
    inputContainer: {
        width: '100%',
        height: 50,
        backgroundColor: '#dedede',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        position: 'relative',
        fontWeight: '500',
        backgroundColor: '#fff',
        borderRadius: 50,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    input: {
        flex: 1,
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 50,
        paddingHorizontal: 16,
        fontSize: 15
    },
    label: {
        paddingHorizontal: 16,
        marginTop: 15,
        fontWeight: '500',
        fontSize: 14,
        color: '#4D5860',
    },
    btContainer: {
        paddingVertical: 13,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    btText: {
        fontWeight: '500',
        fontSize: 16,
        color: '#4D5860',
    },
    icon: {
        width: 16,
        height: 16,
        resizeMode: 'contain',
        opacity: .7
    },
    bottomContainer: {
        width: '100%',
        alignItems: 'center',
        paddingVertical: 15
    },
    version: {
        paddingTop: 18,
        color: '#7E8086'
    },
    logOut: {
        fontWeight: '500',
        fontSize: 16,
        color: '#4D5860',
        paddingLeft: 8
    },
    logOutContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    profileContainer: {
        flex: 1,
        flexDirection: 'column',
        paddingHorizontal: 16,
        paddingVertical: 15,
        alignItems: 'center'
    },
    profileIcon: {
        width: 75,
        height: 75,
        borderRadius: 50,
    },
    profileName: {
        fontSize: 16,
        paddingTop: 10,
        paddingBottom: 5,
        fontWeight: '500'
    },
    openProfile: {
        padding: 3,
    },
    btProfile: {
        textTransform: 'uppercase',
        fontSize: 14,
        color: '#42bec8'
    }
});

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        backgroundColor: '#fff',
        borderRadius: 50,
        paddingHorizontal: 16,
        fontSize: 15,
        height: 50,
        width: deviceWidth-32,
    },
    inputAndroid: {
        backgroundColor: '#fff',
        borderRadius: 50,
        paddingHorizontal: 16,
        fontSize: 15,
        height: 50,
        width: deviceWidth-32
    },
    placeholder: {
        color: '#7E8086',
        fontSize: 15,
    }
  });

export default Settings;