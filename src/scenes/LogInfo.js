import React, {useState, useEffect} from 'react';

import LinearGradient from 'react-native-linear-gradient';

import {View, Text, StyleSheet, SafeAreaView, ScrollView,
        Dimensions, Image, TouchableOpacity, TextInput} from 'react-native';

import Header from '../components/Header';

import RNPickerSelect from 'react-native-picker-select';

import { Switch } from 'react-native-switch';

import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';


let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;

const LogInfo = (props) => {

    const [location, setLocation] = useState(true);

    console.log(props, 'LogInfo');

    return(
        <>
        <LinearGradient colors={['#FEFEFE', '#F9F9F9']} style={styles.container}>
            
            <MapView
            initialRegion={{
                longitude: Number(props.route.params.lng),
                latitude: Number(props.route.params.lat),
              latitudeDelta: 0.122,
              longitudeDelta: 0.621,
            }}
            provider={PROVIDER_GOOGLE}
            style={{
              width: deviceWidth,
              height: deviceHeight,
            }}
          >
            <MapView.Marker
                coordinate={{
                  longitude: Number(props.route.params.lng),
                  latitude: Number(props.route.params.lat)
                }}
              ></MapView.Marker>
          </MapView>
          <TouchableOpacity onPress={() => props.navigation.goBack()} style={styles.close}>
                    <Image style={styles.closeIcon} source={require('../source/img/close.png')}/>
          </TouchableOpacity>  
        </LinearGradient>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        width: deviceWidth,
        flex: 1,
        flexDirection:'column', 
        alignItems:'center', 
        justifyContent:'center'
    },
    contentContainer: {
        flex: 1,
        width: '100%',
        position: 'relative'
    },
    close: {
        width: 45,
        height: 45,
        borderRadius: 50,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        top: 15,
        left: 15,
        position: 'absolute',
        left: 15,
        top: 45
    },
    closeIcon: {
        width: 18,
        height: 18,
        resizeMode: 'contain'
    }
});


export default LogInfo;