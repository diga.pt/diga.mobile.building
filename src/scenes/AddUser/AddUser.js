import React, {useState} from 'react';

import LinearGradient from 'react-native-linear-gradient';

import {View, Text, StyleSheet, SafeAreaView, ScrollView,
        Dimensions, Image, TouchableOpacity, TextInput, KeyboardAvoidingView, Platform} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

import ImagePicker from 'react-native-image-picker';

import Header from '../../components/Header';
import AddUserForm from './AddUserForm';
import AsyncStorage from '@react-native-community/async-storage';
import RNFetchBlob from 'rn-fetch-blob'
import Preloader from '../../components/Preloader';
import Toast from 'react-native-toast-message';

import RNPickerSelect from 'react-native-picker-select';

import {SettingsAPI} from '../../api/settings';

let deviceWidth = Dimensions.get('window').width;

import CustomButton from '../../components/CustomButton';
import I18n from '../../i18n/i18n';

import Modal from 'react-native-modal';


const options = {
    title: 'Фото сотрудника',
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
    quality: 0.5,
    maxHeight: 1200,
  };


  const ModaldSucces = (props) => {
    return(
        <Modal isVisible={props.isVisible}>
        <SafeAreaView style={{ flex: 1 }}>
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <View style={[styles.modalContainer]}>
                <View style={{flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center'}}>
                    <Image style={styles.modalIcon} source={require('../../source/img/checked.png')}/>
                        <Text style={styles.modalText}>{I18n.t('successUser')}</Text>
                      <Text style={styles.pinTitle}>{I18n.t('youPin')}</Text>
                      <Text style={styles.pin}>{props.pin}</Text>
                </View>
                <View style={{paddingTop: 16}}/>
                <CustomButton action={() => props.action()} title={I18n.t('ok')}/>
            </View>
          </View>
        </SafeAreaView>
        </Modal>
    )
  }

const AddUser = (props) => {

    const [location, setLocation] = useState(true);
    const [load, setLoad] = useState(false);

    const [photo, setPhoto] = useState(null);

    const [pin, setPin] = useState(null);


    const addPhoto = () => {
        ImagePicker.showImagePicker(options, (response) => {          
            if (response.didCancel) {
              console.log('User cancelled image picker');
            } else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
              console.log('User tapped custom button: ', response.customButton);
            } else {          
              setPhoto(response);
            }
          });
    }

    const uploadImage = async (data) => {

        setLoad(true);

        let user = await AsyncStorage.getItem('@user');
        let subdomain = await AsyncStorage.getItem('@subdomain');

        if(photo){
        
        const fileData = {
            name: 'files',
            data: RNFetchBlob.wrap(photo.uri.replace("file://", "")),
            filename: photo.fileName || photo.uri.substr(photo.uri.lastIndexOf('/')+1),
            type: photo.type,
        };

        let domain = 'diga.pt';
        if(subdomain === 'besterp'){
          domain = 'rkesa.pt';
        }
        

        RNFetchBlob.fetch(
            'POST',
            `https://${subdomain}.${domain}/api/photo_upload`,
            {
                'Content-Type': 'multipart/form-data',
                Authorization: `Bearer ${JSON.parse(user).accessToken}`
            },
            [fileData]).then((resp) => {
                let photo = JSON.parse(resp.data);
                console.log(photo, 'respphoto');
                setPhoto(null);
                if(photo.errcode === 0){
                    let user = {
                        ...data,
                        photo: photo.url,
                        salary: Number(data.salary),
                        "active": true,
                        "salary_type": false,
                        "educ_before": [],
                        "work_before": [],
                        "user_contracts": [],
                        "user_documents": [],
                        "email_password": "",
                    }
            SettingsAPI.addUser(user)
            .then(res => {
                if(res.errcode === 0){
                    console.log(res, 'res');
                    setPin(res.pin);
                    Toast.show({
                        type: 'success',
                        position: 'bottom',
                        text1: I18n.t('attention'),
                        text2: 'Пользователь успешно добавлен!',
                        visibilityTime: 4000,
                        autoHide: true,
                        bottomOffset: 45
                      });
                } else {
                    Toast.show({
                        type: 'error',
                        position: 'bottom',
                        text1: I18n.t('attention'),
                        text2: I18n.t('error'),
                        visibilityTime: 4000,
                        autoHide: true,
                        bottomOffset: 45
                      });
                }
                setLoad(false);
                setPhoto(null);
            })
            .catch(err => {
                setLoad(false);
                setPhoto(null);
                console.log(err, 'err');
            })
                } else {
                    Toast.show({
                        type: 'error',
                        position: 'bottom',
                        text1: I18n.t('attention'),
                        text2: I18n.t('permission'),
                        visibilityTime: 4000,
                        autoHide: true,
                        bottomOffset: 45
                      });
                    setLoad(false);
                }
            }).catch((err) => {
                Toast.show({
                    type: 'error',
                    position: 'bottom',
                    text1: I18n.t('attention'),
                    text2: I18n.t('errorPhoto'),
                    visibilityTime: 4000,
                    autoHide: true,
                    bottomOffset: 45
                  });
                console.log(err, 'err');
                setLoad(false);
            });
        } else {
            let user = {
                ...data,
                photo: '/img/no_profile_picture.png',
                salary: Number(data.salary),
                "active": true,
                "salary_type": false,
                "educ_before": [],
                "work_before": [],
                "user_contracts": [],
                "user_documents": [],
                "email_password": "",
            }
    SettingsAPI.addUser(user)
    .then(res => {
        console.log(res, 'res');
        if(res.errcode === 0){
            setPin(res.pin);
            Toast.show({
                type: 'success',
                position: 'bottom',
                text1: I18n.t('attention'),
                text2: 'Пользователь успешно добавлен!',
                visibilityTime: 4000,
                autoHide: true,
                bottomOffset: 45
              });
        } else {
            Toast.show({
                type: 'error',
                position: 'bottom',
                text1: I18n.t('attention'),
                text2: I18n.t('permission'),
                visibilityTime: 4000,
                autoHide: true,
                bottomOffset: 45
              });
        }
        setLoad(false);
        setPhoto(null);
    })
    .catch(err => {
        setLoad(false);
        setPhoto(null);
        Toast.show({
            type: 'error',
            position: 'bottom',
            text1: I18n.t('attention'),
            text2: I18n.t('error'),
            visibilityTime: 4000,
            autoHide: true,
            bottomOffset: 45
          });
    })
        }
    }

    const saveUser = (data) => {
        uploadImage(data);
    }

    return(
        <KeyboardAvoidingView
            style={{flex: 1, backgroundColor: '#FEFEFE'}}
            behavior={Platform.OS === "ios" ? "padding" : null}
            >
                <ModaldSucces isVisible={pin ? true : false} action={() => setPin(null)} pin={pin}/>
        <SafeAreaView style={{flex: 0, backgroundColor: '#FEFEFE'}}/>
        {load && <Preloader/>}
        <LinearGradient colors={['#FEFEFE', '#F9F9F9']} style={styles.container}>
            <SafeAreaView style={styles.contentContainer}>
            
            <Header leftIcon='back' title={I18n.t('addUser')}
                    leftAction={() => props.navigation.goBack()}/>
            <ScrollView style={{flex: 1, width: '100%'}} showsVerticalScrollIndicator={false}>
            <View style={{flex: 1, paddingHorizontal: 16}}>
            <View style={styles.photoContainer}>
                <TouchableOpacity onPress={() => addPhoto()} style={styles.addPhoto}>
                   {photo && <View style={styles.editContainer}>
                        <Image style={styles.editIcon} source={require('../../source/img/edit.png')}/>
                    </View>}
                    <Image style={photo ? styles.userPhoto : styles.userIcon} source={photo ? {uri: photo.uri} : require('../../source/img/user.png')}/>
                </TouchableOpacity>
            </View>
        
            <AddUserForm saveUser={saveUser}/>
            
            </View>
            </ScrollView>
            </SafeAreaView>
            
        </LinearGradient>
        </KeyboardAvoidingView>
    )
}

const styles = StyleSheet.create({
    container: {
        width: deviceWidth,
        flex: 1,
        flexDirection:'column', 
        alignItems:'center', 
        justifyContent:'center'
    },
    contentContainer: {
        flex: 1,
        width: '100%',
    },
    inputContainer: {
        width: '100%',
        height: 50,
        backgroundColor: '#dedede',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        position: 'relative',
        fontWeight: '500',
        backgroundColor: '#fff',
        borderRadius: 50,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    input: {
        flex: 1,
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 50,
        paddingHorizontal: 16,
        fontSize: 15
    },
    label: {
        fontWeight: '500',
        fontSize: 14,
        color: '#4D5860',
    },
    btContainer: {
        paddingHorizontal: 16,
        marginTop: 15,
        marginBottom: 5,
        paddingVertical: 5,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    btText: {
        fontWeight: '500',
        fontSize: 16,
        color: '#4D5860',
    },
    icon: {
        width: 16,
        height: 16,
        resizeMode: 'contain',
        opacity: .7
    },
    bottomContainer: {
        width: '100%',
        alignItems: 'center',
        paddingVertical: 15
    },
    version: {
        paddingTop: 18,
        color: '#7E8086'
    },
    logOut: {
        fontWeight: '500',
        fontSize: 16,
        color: '#4D5860',
        paddingLeft: 8
    },
    logOutContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    icon: {
        width: 16,
        height: 16,
        resizeMode: 'contain',
        opacity: .7
    },
    photoContainer: {
        paddingVertical: 15,
        alignItems: 'center',
        justifyContent: 'center',
    },
    addPhoto: {
        width: 95,
        height: 95,
        backgroundColor: '#fff',
        borderRadius: 50,
        borderColor: '#e8e8e8',
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative'
    },
    editContainer: {
        position: 'absolute',
        top: 10,
        right: -8,
        width: 30,
        height: 30,
        borderRadius: 100,
        backgroundColor: '#fff',
        zIndex: 999,
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    editIcon: {
        width: 15,
        resizeMode: 'contain',
    },
    userPhoto: {
        width: 94,
        height: 94,
        borderRadius: 50,
        resizeMode: 'cover'
    },
    userIcon: {
        width: 50,
        height: 50,
        resizeMode: 'contain',
        opacity: .5,
    },
    modalContainer: {
        width: deviceWidth-42,
        padding: 16,
        borderRadius: 8,
        backgroundColor: '#fff',
        maxWidth: 450
    },
    modalBt: {
        width: deviceWidth/2-46,
        height: deviceWidth/2-46,
        borderRadius: 8,
        borderWidth: 1.5,
        borderColor: '#e8e8e8',
        alignItems: 'center',
        justifyContent: 'center'
    },
    iconModal: {
        height: 26,
        resizeMode: 'contain'
    },
    modalIcon: {
        height: 75,
        resizeMode: 'contain'
    },
    modalText: {
        fontSize: 18,
        color: '#4d4d4f',
        fontWeight: '600',
        paddingTop: 16
    },
    pinTitle: {
        fontSize: 14,
        paddingTop: 15,
        color: '#333'
    },
    pin: {
        fontSize: 42,
        fontWeight: '700',
        color: '#42bec8',
        width: '100%',
        textAlign: 'center'
    }
});

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        paddingHorizontal: 16,
        fontSize: 15,
        height: 50
    },
    inputAndroid: {
        paddingHorizontal: 16,
        fontSize: 15,
        height: 50
    },
    placeholder: {
        color: '#7E8086',
        fontSize: 15,
    },
  });

export default AddUser;