import React, {useEffect, useState} from 'react';

import LinearGradient from 'react-native-linear-gradient';

import {View, Text, StyleSheet, SafeAreaView, ScrollView,
        Dimensions, Image, TouchableOpacity, FlatList, ActivityIndicator} from 'react-native';

import Header from '../components/Header';

let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;

import { useSelector } from "react-redux";
import Preloader from '../components/Preloader';

import moment from 'moment';

import I18n from '../i18n/i18n';
import {SettingsAPI} from '../api/settings';


const Logs = (props) => {

    let [list, setList] = useState([]);
    let [load, setLoad] = useState(false);
    let [loadMore, setLoadMore] = useState(false);
    let [logs, setLogs] = useState([]);
    let [offset, setOffsete] = useState(0);
    let [total, setTotal] = useState(0);
    const settings = useSelector(state => state.general.settings);

    useEffect(() => {
        const unsubscribe = props.navigation.addListener('focus', () => {
            getLogs('');
        });
        return unsubscribe;
      }, [props.navigation]);



    const getLogs = (data) => {
        setLoad(true);
        SettingsAPI.getLogs({offset: 0, limit: 15})
        .then(res => {
            setLoad(false);
            setLogs(res.rows);
            setTotal(res.total);
            console.log(res, 'res');
        })
        .catch(err => {
            setLoad(false);
            console.log(err, 'err');
        });
    };

    const loadMoreLogs = () => {
        if(logs.length < total){
            setLoadMore(true);
            SettingsAPI.getLogs({offset: offset+15, limit: 15})
            .then(res => {
                setLoadMore(false);
                setLogs([...logs, ...res.rows]);
                setOffsete(offset+15);
                console.log(res, 'res');
            })
            .catch(err => {
                setLoadMore(false);
                console.log(err, 'err');
            });
        };
    };


    
    const showOnMap = (data) => {
        if(data.lat > 0){
            props.navigation.navigate('LogInfo', data);
        } else {

        }
    }
    
    let getPhoto = (url_photo) => {
        let domain = 'diga.pt';
            if(settings.subdomain === 'besterp'){
                    domain = 'rkesa.pt';
                }
            let url = `https://${settings.subdomain}.${domain}${url_photo}`;
            return url;
    }
  

    return(
        <>
                {load && <Preloader/>}
        <SafeAreaView style={{flex: 0, backgroundColor: '#FEFEFE'}}/>
        <LinearGradient colors={['#FEFEFE', '#F9F9F9']} style={styles.container}>
            <SafeAreaView style={styles.contentContainer}>
            <Header leftIcon='menu' title={I18n.t('logs')}
                    leftAction={() => props.navigation.openDrawer()}/>
            <ScrollView style={{flex: 1, width: '100%'}} showsVerticalScrollIndicator={false}>
            <View style={{flex: 1, paddingBottom: 15}}>
            <FlatList
                    data={logs}
                    keyExtractor={(item, index) => String(index)}
                    onEndReachedThreshold={0}
                    onEndReached={data => {
                        if(!loadMore){
                            loadMoreLogs();
                        }
                    }}
                    renderItem={data => {
                                let item = data.item;
                                    return(
                                        <TouchableOpacity  onPress={() => showOnMap({lat: item.photo.lat,lng: item.photo.lng})} style={styles.userContainer}>
                                                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                                    <Image style={[styles.userPhoto, {borderColor: item.type === 'IN' ? '#42bec8' : '#F44336'}]} source={{uri: getPhoto(item.photo.url)}}/>
                                                        <View>
                                                            <Text style={styles.name}>{item.user_name}</Text>
                                                            <Text style={styles.time}>{item.date_time}</Text>
                                                        </View>
                                                    </View>
                                                    <Image style={styles.icon} source={item.type === 'IN' ? require('../source/img/in.png') : require('../source/img/out.png')}/>
                                        </TouchableOpacity>
                                    )
                                }}
                    />
                       {loadMore && <ActivityIndicator style={{marginTop: 15}} size="small" color="#42bec8"/>}
                {/*list && list.length > 0 ?
                    list.map((item, index) => {
                        let data = item.data.sort((a, b) => new Date(b.date_time) - new Date(a.date_time));                        ;
                        return(
                            <View key={index}>
                                        <Text style={styles.date}>{item.key}</Text>
                                        {data.map((it, key) => {
                                            return(
                                                <TouchableOpacity key={`${item}_${key}`} onPress={() => showOnMap(it)} style={styles.userContainer}>
                                                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                                    <Image style={[styles.userPhoto, {borderColor: it.type === 'in' ? '#42bec8' : '#F44336'}]} source={{uri: it.uri}}/>
                                                        <View>
                                                            <Text style={styles.name}>{it.user ? it.user.name : 'User'}</Text>
                                                            <Text style={styles.time}>{it.date_time.split(' ')[1]}</Text>
                                                        </View>
                                                    </View>
                                                    <Image style={styles.icon} source={it.type === 'in' ? require('../source/img/in.png') : require('../source/img/out.png')}/>
                                                </TouchableOpacity>
                                            )
                                        })}
                            </View>
                        )
                    }) : <Text style={styles.warning}>{I18n.t('noRecords')}</Text>
                */}
            </View>
            </ScrollView>
            </SafeAreaView>
        </LinearGradient>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        width: deviceWidth,
        flex: 1,
        flexDirection:'column', 
        alignItems:'center', 
        justifyContent:'center'
    },
    contentContainer: {
        flex: 1,
        width: '100%',
    },
    userContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 16,
        paddingVertical: 8,     
        justifyContent: 'space-between',
        backgroundColor: '#FFF',
        borderRadius: 12,
        marginHorizontal: 16,
        marginTop: 10,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    userPhoto: {
        width: 50,
        height: 50,
        borderRadius: 50,
        resizeMode: 'cover',
        backgroundColor: '#dedede',
        borderWidth: 3,
    },
    name: {
        paddingLeft: 16,
        fontSize: 16,
        color: '#4D5860',
        fontWeight: '500'
    },
    time: {
        paddingLeft: 16,
        fontSize: 16,
        color: '#4D5860',
    },
    date: {
        paddingHorizontal: 16,
        fontSize: 18,
        color: '#4D5860',
        paddingTop: 16,
        paddingBottom: 6,
        fontWeight: '600'
    },
    icon: {
        width: 26,
        height: 26,
        resizeMode: 'contain'
    },
    icon: {
        width: 20,
        height: 25,
        resizeMode: 'contain'
    },
    modalContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        maxWidth: 450
    },
    modal: {
        width: deviceWidth-32,
        height: deviceHeight*0.7,
        paddingHorizontal: 16,
        paddingVertical: 20,
        backgroundColor: '#fff',
        borderRadius: 15,
        position: 'relative'
    },
    close: {
        width: 45,
        height: 45,
        borderRadius: 50,
        backgroundColor: '#fff',
        position: 'absolute',
        right: 15,
        top: 15,
        alignItems: 'center',
        justifyContent: 'center'
    },
    closeIcon: {
        width: 18,
        height: 18,
        resizeMode: 'contain'
    },
    warning: {
        fontSize: 16, 
        fontWeight: '500',
        color: '#7E8086',
        textAlign: 'center',
        paddingVertical: 16
    }
});

export default Logs;