import React, {useEffect, useState} from 'react';

import LinearGradient from 'react-native-linear-gradient';

import {View, Text, StyleSheet, SafeAreaView, ScrollView,
        Dimensions, Image, TouchableOpacity, TextInput} from 'react-native';
import Modal from 'react-native-modal';
import {
    CodeField,
    Cursor,
    useBlurOnFulfill,
    useClearByFocusCell,
  } from 'react-native-confirmation-code-field';

import Header from '../components/Header';
import CustomButton from '../components/CustomButton';

let deviceWidth = Dimensions.get('window').width;

const CELL_COUNT = 4;

import {UserAPI} from '../api/user';

import Preloader from '../components/Preloader';

import AsyncStorage from '@react-native-community/async-storage';

import I18n from '../i18n/i18n';

import { useDispatch, useSelector } from "react-redux";


import {updateAdditionalUser} from '../reducers/User';


const  ModalPassword = (props) => {
    const [value, setValue] = useState('');
    const ref = useBlurOnFulfill({value, cellCount: CELL_COUNT});
    const [propsCode, getCellOnLayoutHandler] = useClearByFocusCell({
        value,
        setValue,
    });

    return (
        <Modal isVisible={props.status}>
          <View style={styles.modalContainer}>
            <View style={styles.modal}>
            <Text style={styles.titleModal}>{I18n.t('pin')}</Text>
            <View style={styles.codeContainer}>
            <CodeField
                ref={ref}
                {...propsCode}
                value={value}
                onChangeText={setValue}
                cellCount={CELL_COUNT}
                rootStyle={styles.codeFieldRoot}
                keyboardType="number-pad"
                textContentType="oneTimeCode"
                renderCell={({index, symbol, isFocused}) => (
                    <View style={styles.item}>
                    <Text
                        key={index}
                        style={[styles.cell, isFocused && styles.focusCell]}
                        onLayout={getCellOnLayoutHandler(index)}>
                        {symbol || (isFocused ? <Cursor /> : null)}
                    </Text>
                    </View>
                )}
            />
            </View>
                <CustomButton action={() => {props.action(value); setValue('')}} title={I18n.t('ok')}/>
                <View style={styles.cancleContainer}>
                <TouchableOpacity style={styles.cancleBt}
                onPress={() => {
                    setValue('');
                    props.cancle();
                }}>
                    <Text style={styles.cancleText}>{I18n.t('cancle')}</Text>
                </TouchableOpacity>
                </View>
            </View>
            
          </View>
        </Modal>
    )
  }

const Users = (props) => {

    const [modalStatus, setModal] = useState(false);
    const [load, setLoad] = useState(false);
    const [users, setUsers] = useState([]);
    const [subdomain, setSubdomain] = useState(null);
    const user = useSelector(state => state.user.user);

    const dispatch = useDispatch();

    useEffect(() => {
        getSubdomain();
        getSaveUsers();
    }, []);

    const getSaveUsers = async () => {
        let users = await AsyncStorage.getItem('@users');
            users = users ?  JSON.parse(users) : [];
            setUsers(users);
    }


    const auth = (data) => {
        setModal(false);
        setLoad(true);
        UserAPI.checkPin({pin: data, mobile: 1})
            .then(res => {
                console.log(res);
                if(res.errcode === 0){
                    let item = {
                        name: res.user.name,
                        id: res.user.id,
                        photo: res.user.photo,
                        pin: data
                    };
                    saveUser(item);
                    setLoad(false);
                   /* let access_token = res.access_token;
                    let expires_in = res.expires_in;
                setTimeout(() => {
             UserAPI.getAdditionalUser({token: res.access_token})
                .then(resp => {
                    let item = {
                        token: access_token,
                        expires_in: expires_in,
                        //date: new Date(),
                        name: resp.name,
                        id: resp.id,
                        photo: resp.photo
                    };
                  saveUser(item);
                  setLoad(false);
                 
                })
                .catch(err => {
                    setLoad(false);

                  console.log(err, 'errgetAdditionalUser');
                })
                }, 500);*/
                } else {
                    setModal(true);
                    setLoad(false);

                }
            })
            .catch(err => {
                console.log(err, 'err');
                setModal(true);
            })
    }

    const saveUser = async (data) => {
        let users = await AsyncStorage.getItem('@users');

            users = users ?  JSON.parse(users) : [];
        
            let index = users.findIndex(it => it.id === data.id);
            console.log(index, 'index');

            if(index === -1){
                users.push({...data, subdomain});
            } else {
                users[index] = {
                    ...data,
                    subdomain
                }
            }

            setUsers(users);
            await AsyncStorage.setItem('@users', JSON.stringify(users));
            console.log(data, 'data');
            dispatch(updateAdditionalUser(data));
            setTimeout(() => {
                props.navigation.navigate('In/Out');
            }, 500);
    }

    const getSubdomain = async () => {
        let subdomain = await AsyncStorage.getItem('@subdomain');
        setSubdomain(subdomain);
    }
    

    return(
        <>
        <ModalPassword status={modalStatus} action={(data) => auth(data)} cancle={() => setModal(false)}/>
        {load && <Preloader/>}
        <SafeAreaView style={{flex: 0, backgroundColor: '#FEFEFE'}}/>
        <LinearGradient colors={['#FEFEFE', '#F9F9F9']} style={styles.container}>
            <SafeAreaView style={styles.contentContainer}>
            <Header leftIcon='menu' title={I18n.t('home')}
                    leftAction={() => props.navigation.openDrawer()}
                    rightIcon={'addUser'}
                    rightAction={() => setModal(true)}
                    sizeIcon={26}
                    />
            <Text style={styles.title}>{I18n.t('selectUser')}</Text>
            <ScrollView style={{flex: 1, width: '100%'}} showsVerticalScrollIndicator={false}>
            <View style={{flex: 1, paddingBottom: 15}}>
                {users.map((item, index) => {
                    return(
                    <View key={index}>
                    <TouchableOpacity  onPress={() => setModal(true)} style={styles.userContainer}>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Image style={styles.userPhoto} source={{uri: `https://${subdomain}.diga.pt/${item.photo}`}}/>
                            <Text style={styles.name}>{item.name}</Text>
                        </View>
                        {user.id === item.id && <View style={styles.online}/>}
                    </TouchableOpacity>
                    </View>
                    )
                })}
                
            </View>
            </ScrollView>
            </SafeAreaView>
        </LinearGradient>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        width: deviceWidth,
        flex: 1,
        flexDirection:'column', 
        alignItems:'center', 
        justifyContent:'center'
    },
    contentContainer: {
        flex: 1,
        width: '100%',
    },
    userContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 16,
        paddingVertical: 8,     
        justifyContent: 'space-between',
        backgroundColor: '#FFF',
        borderRadius: 12,
        marginHorizontal: 16,
        marginTop: 10,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    userPhoto: {
        width: 50,
        height: 50,
        borderRadius: 50,
        resizeMode: 'contain'
    },
    name: {
        paddingLeft: 16,
        fontSize: 16,
        color: '#4D5860',
        fontWeight: '500'
    },
    icon: {
        width: 26,
        height: 26,
        resizeMode: 'contain'
    },
    modalContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        maxWidth: 450
    },
    modal: {
        width: deviceWidth-32,
        paddingHorizontal: 16,
        paddingVertical: 20,
        backgroundColor: '#fff',
        borderRadius: 15
    },
    item: {
        borderBottomWidth: 1,
        borderBottomColor: '#7e8086a6',
        alignItems: 'center',
        justifyContent: 'center'
    },
    cell: {
        width: (deviceWidth-124)/4,
        height: (deviceWidth-124)/4,
        lineHeight: (deviceWidth-124)/4,
        fontSize: 24,
        textAlign: 'center',
        color: '#7E8086'
      },
      focusCell: {

      },
      codeContainer: {
          paddingHorizontal: 16,
          marginBottom: 20
      },
      titleModal: {
          color: '#7E8086',
          textAlign: 'center',
          width: '100%',
          fontSize: 18,
          fontWeight: '600',
          paddingBottom: 10,
          textTransform: 'uppercase'
      },
      title: {
        color: '#7E8086',
        fontSize: 18,
        paddingHorizontal: 16,
        paddingVertical: 10,
        fontWeight: '400'
      },
      cancleContainer: {
          width: '100%',
          alignItems: "center",
          marginTop: 10
      },
      cancleBt: {
          padding: 10
      },
      cancleText: {
        fontSize: 16,
        textTransform: 'uppercase',
        fontWeight: '600',
        color: '#7E8086'
      },
      online: {
          width: 16,
          height: 16,
          borderRadius: 50,
          backgroundColor: '#42bec8'
      }
});

export default Users;