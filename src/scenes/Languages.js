import React, {useEffect, useState} from 'react';

import LinearGradient from 'react-native-linear-gradient';

import {View, Text, StyleSheet, SafeAreaView, ScrollView,
        Dimensions, Image, TouchableOpacity, TextInput} from 'react-native';

import Header from '../components/Header';

let deviceWidth = Dimensions.get('window').width;

import AsyncStorage from '@react-native-community/async-storage';

import {updateSettings} from '../reducers/General';

import { useDispatch } from "react-redux";

import Preloader from '../components/Preloader';

import I18n from '../i18n/i18n';
import RNRestart from 'react-native-restart';


const Languages = (props) => {

    const dispatch = useDispatch();

    const [list, setList] = useState([
        {
            id: 'pt',
            name: 'Português'
        },
        {
            id: 'ru',
            name: 'Русский'
        },
        {
            id: 'en',
            name: 'English'
        },
        {
            id: 'es',
            name: 'Español'
        }
    ]);

    const [load, setLoad] = useState(false);


    const [language, setLanguage] = useState(null);

    useEffect(() => {
        setLanguage(props.route.params.id);
    }, []);


    const saveData = async () => {
        I18n.locale = language;
        let data = list.find(it => it.id === language);
        AsyncStorage.setItem('@language', JSON.stringify(data))
        props.navigation.navigate('Settings');
        RNRestart.Restart();
    }

    return(
        <>
        {load && <Preloader/>}
        <SafeAreaView style={{flex: 0, backgroundColor: '#FEFEFE'}}/>
        <LinearGradient colors={['#FEFEFE', '#F9F9F9']} style={styles.container}>
            <SafeAreaView style={styles.contentContainer}>
            <Header leftIcon='back' title={I18n.t('language')}
                    leftAction={() => props.navigation.goBack()}
                    rightAction={() => saveData()}
                    rightIcon={'check'}/>
            <View style={{flex: 1, alignItems: 'center'}}>
            <ScrollView style={{flex: 1, width: '100%'}} showsVerticalScrollIndicator={false}>
            <View style={{flex: 1, paddingHorizontal: 16, paddingBottom: 20}}>
                {list.map((item, index) => {
                    return(
                        <TouchableOpacity key={index} 
                                style={[styles.item]}
                                onPress={() => {
                                    setLanguage(item.id);
                                }}
                                activeOpacity={1}
                                >
                            <Text style={styles.itemLabel}>{item.name}</Text>
                            {language === item.id && <Image style={styles.icon} source={require('../source/img/check-symbol.png')}/>}
                        </TouchableOpacity>
                    )
                })}
            </View>
            </ScrollView>
            </View>
            </SafeAreaView>
        </LinearGradient>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        width: deviceWidth,
        flex: 1,
        flexDirection:'column', 
        alignItems:'center', 
        justifyContent:'center'
    },
    contentContainer: {
        flex: 1,
        width: '100%',
    },
    input: {
        flex: 1,
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 50,
        paddingHorizontal: 16,
        fontSize: 15
    },
    inputWrapper: {
      width: deviceWidth-32,
      marginBottom: 12,
      marginTop: 8,
    },
    inputContainer: {
        width: '100%',
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 50,
        flexDirection: 'row',
        alignItems: 'center',
        position: 'relative',
        fontWeight: '500',
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    warning: {
        fontSize: 16, 
        fontWeight: '500',
        color: '#7E8086',
        textAlign: 'center',
        paddingVertical: 16
    },
    item: {
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 16,
        height: 48, 
        justifyContent: 'space-between',
        backgroundColor: '#FFF',
        borderRadius: 12,
        marginTop: 10,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    itemLabel: {
        fontSize: 14, 
        fontWeight: '500',
        color: '#7E8086',
    },
    active: {
        borderWidth: 2,
        borderColor: '#42bec8'
    },
    icon: {
        width: 22,
        height: 22,
        resizeMode: 'contain'
    }
});

export default Languages;