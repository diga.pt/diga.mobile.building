
import React, {useState, useEffect} from 'react';

import {View, Text, SafeAreaView, Keyboard, Image, StyleSheet, TouchableOpacity,
    TextInput} from 'react-native';

import { renderInput} from '../../components/form';
import { reduxForm, Field, reset} from 'redux-form';
import {required, email} from '../../components/form/validator';

import CustomButton from '../../components/CustomButton';

import I18n from '../../i18n/i18n';



const ForgotPasswordForm = (props) => {
    const { handleSubmit, dispatch } = props;

    const forgotPassword = (data) => {
        dispatch(reset('ForgotPasswordForm'));
        props.forgotPassword(data);
    }


    return(
        <>
        <View style={styles.formContainer}>
              
                <Field  name={`subdomain`}
                        component={renderInput}
                        placeholder={I18n.t('subdomain')}
                        validate={[required]}
                        subdomain={true}
                        type={'default'}
                        />
                <Field  name={`email`}
                        component={renderInput}
                        placeholder={I18n.t('email')}
                        validate={[required, email]}
                        type={'email-address'}
                        />
            </View>
            <CustomButton action={handleSubmit(forgotPassword)} title={I18n.t('ok')}/>
            </>
    )
}

const styles = StyleSheet.create({
    formContainer: {
        height: 'auto',
        width: '100%',
        marginTop: 15,
        alignItems: 'flex-end'
    },
    input: {
        flex: 1,
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 50,
        paddingHorizontal: 16,
        fontSize: 15
    },
    inputContainer: {
        width: '100%',
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 50,
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 15,
        position: 'relative',
        fontWeight: '500'
    },
    subdomain: {
        paddingRight: 16,
        paddingLeft: 8,
        fontSize: 15,
        fontWeight: '500',
        color: '#42bec8'
    },
    text: {
        fontSize: 14, 
        fontWeight: '500',
        color: '#7E8086'
    },
    forgot: {
        paddingVertical: 5,
        marginBottom: 20,
        paddingHorizontal: 5
    }
})


export default reduxForm({
    form: 'ForgotPasswordForm',
    enableReinitialize: true
  })(ForgotPasswordForm);