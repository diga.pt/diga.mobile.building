import React, {useState} from 'react';

import LinearGradient from 'react-native-linear-gradient';

import {View, Text, StyleSheet, SafeAreaView, ScrollView,
        Dimensions, Image, TouchableOpacity, TextInput} from 'react-native';


import Header from '../../components/Header';

import RNPickerSelect from 'react-native-picker-select';

import { Switch } from 'react-native-switch';

let deviceWidth = Dimensions.get('window').width;

import CustomButton from '../../components/CustomButton';

import {UserAPI} from '../../api/user';

import ForgotPasswordForm from './ForgotPasswordForm'
import Toast from 'react-native-toast-message';

import I18n from '../../i18n/i18n';


const ForgotPassword = (props) => {

    const forgot = (data) => {
        console.log(data);
        UserAPI.forgotPassword(data)
            .then(res => {
                console.log(res, 'res')
                Toast.show({
                    type: 'success',
                    position: 'bottom',
                    text1: I18n.t('attention'),
                    text2: I18n.t('succesForgot'),
                    visibilityTime: 4000,
                    autoHide: true,
                    bottomOffset: 45
                  });
            })
            .catch(err => {
                console.log(err, 'err')
                Toast.show({
                    type: 'error',
                    position: 'bottom',
                    text1: I18n.t('subdomain'),
                    text2: I18n.t('subdomain'),
                    visibilityTime: 4000,
                    autoHide: true,
                    bottomOffset: 45
                  });
            })
    }

    return(
        <>
        <SafeAreaView style={{flex: 0, backgroundColor: '#ebecec'}}/>
        <LinearGradient colors={['#ebecec', '#d8e9ed']} style={styles.container}>
            <SafeAreaView style={styles.contentContainer}>
            <Header leftIcon='back' title={I18n.t('forgot')}
                    leftAction={() => props.navigation.goBack()}/>
            <ScrollView style={{flex: 1, width: '100%'}} showsVerticalScrollIndicator={false}>
            <View style={{flex: 1, paddingHorizontal: 16}}>
                <ForgotPasswordForm forgotPassword={forgot}/>
                <View style={{marginTop: 25}}>
                <TouchableOpacity style={styles.bt}>
                    <Image style={{width: 23, height: 23}} source={require('../../source/img/message.png')}/>
                    <Text style={styles.textBt}>{I18n.t('informAbout')}</Text>
                </TouchableOpacity>
                </View>
            </View>
            </ScrollView>
                
            </SafeAreaView>
        </LinearGradient>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        width: deviceWidth,
        flex: 1,
        flexDirection:'column', 
        alignItems:'center', 
        justifyContent:'center'
    },
    contentContainer: {
        flex: 1,
        width: '100%',
    },
    inputContainer: {
        width: '100%',
        height: 50,
        backgroundColor: '#dedede',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        position: 'relative',
        fontWeight: '500',
        backgroundColor: '#fff',
        borderRadius: 50,
        shadowColor: "#B9B9BC",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    input: {
        flex: 1,
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 50,
        paddingHorizontal: 16,
        fontSize: 15
    },
    label: {
        fontWeight: '500',
        fontSize: 14,
        color: '#4D5860',
    },
    btContainer: {
        paddingHorizontal: 16,
        marginTop: 15,
        marginBottom: 5,
        paddingVertical: 5,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    btText: {
        fontWeight: '500',
        fontSize: 16,
        color: '#4D5860',
    },
    icon: {
        width: 16,
        height: 16,
        resizeMode: 'contain',
        opacity: .7
    },
    bottomContainer: {
        width: '100%',
        alignItems: 'center',
        paddingVertical: 15
    },
    version: {
        paddingTop: 18,
        color: '#7E8086'
    },
    logOut: {
        fontWeight: '500',
        fontSize: 16,
        color: '#4D5860',
        paddingLeft: 8
    },
    logOutContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    icon: {
        width: 16,
        height: 16,
        resizeMode: 'contain',
        opacity: .7
    },
    photoContainer: {
        paddingVertical: 15,
        alignItems: 'center',
        justifyContent: 'center',
    },
    addPhoto: {
        width: 95,
        height: 95,
        backgroundColor: '#fff',
        borderRadius: 50,
        borderColor: '#e8e8e8',
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    userIcon: {
        width: 50,
        height: 50,
        resizeMode: 'contain',
        opacity: .5,
    },
    bt: {
        padding: 5,
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center'
    },
    textBt: {
        fontSize: 16,
        color: '#3A3A3C',
        paddingLeft: 15
    }
});



export default ForgotPassword;