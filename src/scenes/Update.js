import React, { useEffect, useState } from 'react';

import {View, Text, StyleSheet, 
        Dimensions, Image, Linking, TouchableOpacity} from 'react-native';

        import LinearGradient from 'react-native-linear-gradient';

import AsyncStorage from '@react-native-community/async-storage';

let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;

import Preloader from '../components/Preloader';
import CustomButton from '../components/CustomButton';
import I18n from '../i18n/i18n';

const UpdateScreen = (props) => {

    return(
        <LinearGradient colors={['#ebecec', '#d8e9ed']} style={styles.container}>
            <View style={styles.row}>
                <Image source={require('../source/img/logo.png')} style={styles.logo}/>
                <Text style={styles.title}>{props.version.title[props.lang.id]}</Text>
                <Text style={styles.text}>{props.version.text[props.lang.id]}</Text>
                <CustomButton action={() => Linking.openURL(props.version.url)} title={I18n.t('update')}/>
                <Text style={styles.info}>{I18n.t('infoUpdate')}</Text>
                {props.version.required === 'false' && <View>
                    <TouchableOpacity onPress={() => props.skip()} style={styles.bt}>
                    <Text style={styles.btText}>{I18n.t('skip')}</Text>
                </TouchableOpacity>
                </View>}
            </View>
              
                {/*<Image source={require('../source/img/logo.png')} style={styles.logo}/>
                <Text style={styles.title}>{props.version.title[lang.id]}</Text>
                <Text style={styles.text}>{props.version.text[lang.id]}</Text>
                <CustomButton action={() => Linking.openURL(props.version.url)} title={I18n.t('update')}/>
                <View>
                    <Text style={styles.info}>{I18n.t('infoUpdate')}</Text>
                </View>
                
                {props.version.required && <View>
                    <TouchableOpacity onPress={() => props.skip()} style={styles.bt}>
                    <Text style={styles.btText}>{I18n.t('skip')}</Text>
                </TouchableOpacity>
                </View>}*/}
                
        </LinearGradient>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },
    row: {
        width: deviceWidth-52,
        flexDirection:'row', 
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo: {
        width: 250,
        height: 115,
        resizeMode: 'contain',
        marginBottom: 30
    },
    title: {
        textAlign: 'center',
        fontSize: 22,
        fontWeight: '500',
        color: '#42bec8',
        width: '100%'
    },
    text: {
        textAlign: 'center',
        fontSize: 18,
        color: '#7E8086',
        paddingBottom: 25,
        paddingTop: 5,
        width: '100%'
    },
    btText: {
        fontWeight: '500',
        color: '#7E8086',
        textTransform: 'uppercase',
        fontSize: 16,
        textAlign: 'center',
        width: '100%'
    },
    bt: {
        padding: 5,
        marginTop: 15
    },
    info: {
        paddingTop: 10,
        textAlign: 'center',
        color: '#7E8086',
        width: deviceWidth-62,
    }
});

export default UpdateScreen;